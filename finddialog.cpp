#include "finddialog.h"


FindDialog::FindDialog(QWidget *parent) :
    QDialog(parent)
{
    if(parent) this->setParent(parent);

    QLabel *findLabel = new QLabel(tr("Введите текст для поиска:"));
    lineEdit = new QLineEdit;

    findButton = new QPushButton(tr("&Найти"));
    findText = "";

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(findLabel);
    layout->addWidget(lineEdit);
    layout->addWidget(findButton);

    setLayout(layout);
    setWindowTitle(tr("Найти"));
    connect(findButton, SIGNAL(clicked()), this, SLOT(findClicked()));

    checkbox_Backward = new QCheckBox("В обратном направлении");
    checkbox_CaseSensitively = new QCheckBox("С учетом регистра");
    checkbox_FindWholeWords = new QCheckBox("Искать целое выражение");

    layout->addWidget(checkbox_Backward);
    layout->addWidget(checkbox_CaseSensitively);
    layout->addWidget(checkbox_FindWholeWords);
}


void FindDialog::findClicked()
 {
   findText = lineEdit->text();
   if(checkbox_Backward->isChecked()) flags|=QTextDocument::FindBackward;
                                 else flags&=~QTextDocument::FindBackward;
   if(checkbox_CaseSensitively->isChecked()) flags|=QTextDocument::FindCaseSensitively;
                                        else flags&=~QTextDocument::FindCaseSensitively;
   if(checkbox_FindWholeWords->isChecked()) flags|=QTextDocument::FindWholeWords;
                                        else flags&=~QTextDocument::FindWholeWords;

   if(findText.length()) emit find_pressed();
   else{
       QMessageBox *msgBox = new QMessageBox(this);
       msgBox->setIcon(QMessageBox::Warning);
       msgBox->setText("Пустая строка поиска!");
       msgBox->exec();
   }
 }


 QString FindDialog::getFindText()
 {
     return findText;
 }


 QTextDocument::FindFlags FindDialog::GetSearchParams(){
     return flags;
 }


void FindDialog::hideEvent(__attribute__((unused)) QHideEvent * event){
    emit find_hide();
}


void FindDialog::fault_msg(){
    QMessageBox *msgBox = new QMessageBox(this);
    msgBox->setIcon(QMessageBox::Warning);
    msgBox->setText("Не найдено!");
    msgBox->exec();
}
