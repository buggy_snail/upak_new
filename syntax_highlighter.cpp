#include "syntax_highlighter.h"


Highlighter::Highlighter(QTextDocument *parent=0, int(*func)(QStringRef)) :
    QSyntaxHighlighter(parent)
{
    SetOrdinaryRules();
    SetDebugModeRules();

    debug_mode_flag=false;
    get_reg_func=func;
}


void Highlighter::SetOrdinaryRules(){
    HighlightingRule rule;

    highlightingRules.clear();

    //номера регистров
    reg_num_format.setFontWeight(QFont::Bold);
    reg_num_format.setForeground(Qt::darkMagenta);
    rule.pattern = QRegExp("\\b[0-9]{1,4}\\b");
    rule.format = reg_num_format;
    highlightingRules.append(rule);

    //имена регистров
    reg_name_format.setFontWeight(QFont::Bold);
    reg_name_format.setForeground(Qt::magenta);
    rule.pattern = QRegExp("\\b[A-Za-zА-Яа-яЁё][A-Za-zА-Яа-яЁё0-9_]*\\b");
    rule.format = reg_name_format;
    highlightingRules.append(rule);

    //описания переменных
    define_format.setFontWeight(QFont::Bold);
    define_format.setForeground(Qt::darkCyan);
    rule.pattern = QRegExp("#\\?[^\n]*");
    rule.format = define_format;
    highlightingRules.append(rule);

    //условные переходы
    condition_format.setFontWeight(QFont::Bold);
    condition_format.setForeground(Qt::blue);
    rule.pattern = QRegExp("#\\*[^\n]*");
    rule.format = condition_format;
    highlightingRules.append(rule);

    //комменты
    comment_format.setFontItalic(true);
    comment_format.setForeground(Qt::lightGray);
    rule.pattern = QRegExp("#[^\\?\\*][^\n]*");
    rule.format = comment_format;
    highlightingRules.append(rule);
}


void Highlighter::SetDebugModeRules(){
    HighlightingRule rule;

    highlightingDebugRules.clear();

    //имена и номера регистров
    reg_num_name_debug_format.setFontWeight(QFont::Bold);
    rule.pattern = QRegExp("\\b[A-Za-zА-Яа-яЁё0-9_]+\\b");
    rule.format = reg_num_name_debug_format;
    DebugRegsRule=rule;

    //описания переменных
    define_debug_format.setFontWeight(QFont::Bold);
    define_debug_format.setForeground(Qt::darkCyan);
    rule.pattern = QRegExp("#\\?");
    rule.format = define_debug_format;
    highlightingDebugRules.append(rule);

    //условные переходы
    condition_debug_format.setFontWeight(QFont::Bold);
    condition_debug_format.setForeground(Qt::blue);
    rule.pattern = QRegExp("#\\*");
    rule.format = condition_debug_format;
    highlightingDebugRules.append(rule);

    //комменты
    comment_debug_format.setFontItalic(true);
    comment_debug_format.setForeground(Qt::lightGray);
    rule.pattern = QRegExp("#[^\\?\\*][^\n]*");
    rule.format = comment_debug_format;
    highlightingDebugRules.append(rule);
}


void Highlighter::highlightBlock(const QString &text){
    int index, res;

    if(debug_mode_flag){
        foreach (const HighlightingRule &rule, highlightingDebugRules) {
                 QRegExp expression(rule.pattern);
                 index = expression.indexIn(text);
                 while (index >= 0) {
                     int length = expression.matchedLength();
                     setFormat(index, length, rule.format);
                     index = expression.indexIn(text, index + length);
                 }
             }


        QRegExp expression(DebugRegsRule.pattern);
        index = expression.indexIn(text);
        while (index >= 0) {
            int length = expression.matchedLength();
            if(get_reg_func!=nullptr){
                res = get_reg_func(text.midRef(index, length));
                if(res<0) DebugRegsRule.format.setForeground(Qt::black);
                if(res==0) DebugRegsRule.format.setForeground(Qt::darkGreen);
                if(res==1) DebugRegsRule.format.setForeground(Qt::darkRed);
            }else DebugRegsRule.format.setForeground(Qt::black);
            setFormat(index, length, DebugRegsRule.format);
            index = expression.indexIn(text, index + length);
        }

    }else{
        foreach (const HighlightingRule &rule, highlightingRules) {
                 QRegExp expression(rule.pattern);
                 index = expression.indexIn(text);
                 while (index >= 0) {
                     int length = expression.matchedLength();
                     setFormat(index, length, rule.format);
                     index = expression.indexIn(text, index + length);
                 }
             }
    }

}
