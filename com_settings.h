#ifndef COM_SETTINGS_H
#define COM_SETTINGS_H


#include <functional>

#include <QDialog>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>

#include "upak_settings.h"

class DialogComSettings : public QDialog
{
    Q_OBJECT
public:
    explicit DialogComSettings(QWidget *parent, upak_settings_t& upak_settings);
    bool IsOK(){return ok_status;}

private:
    bool ok_status;
    std::reference_wrapper<upak_settings_t> settings;
    QPushButton* button_ok;
    QPushButton* button_cancel;
    QSpinBox* spin_com;
    QSpinBox* spin_adr;
    QComboBox* combo_baudrate;
};


#endif // COM_SETTINGS_H
