#ifndef UPAK_COMPILE_H
#define UPAK_COMPILE_H

#include <QString>
#include <QStringList>
#include <QSet>


#include "compile_stack.h"


#define N_OF_REGS       4096
#define UPAK_MAX_SIZE   5120


enum ConvertDirection{
    ToChars,
    ToDigits
};


//типы дозаторов
enum DozatorType{
 VES=0,
 VES_PV,
 SUB,
 IMP
};

typedef struct{
  float weight;
  float gist;
  QString reg;
} upak_wp_data_t;


//структура редактируемых данных дозаторов
typedef struct{
  unsigned char dozator_type;
  unsigned char NOfComp;
  unsigned char et04_adr;
  unsigned char et04_channel;
  struct{
     QString speed_12[16][2];
     QString error_load;
     QString error_zero;
     QString wait_weighter;
     QString substr_flag;
     QString DozAllow;
     QString DozOpen;
     QString DozEnd;
     QString DozEmpty;
  } flags;
  unsigned char wp_num;
  unsigned char reserv;
  upak_wp_data_t wp[4];
} upak_dozator_data_t;




#pragma pack(push,1)
typedef struct{
  float weight;
  float gist;
  uint16_t reg;
} wp_data_t;
#pragma pack(pop)

//структура передаваемых в прибор данных о дозаторе
#pragma pack(push,1)
typedef struct{
  uint8_t dozator_type;
  uint8_t NOfComp;
  uint8_t et04_adr;
  uint8_t et04_channel;
  struct{
     uint16_t speed_12[16][2];
     uint16_t error_load;
     uint16_t error_zero;
     uint16_t wait_weighter;
     uint16_t substr_flag;
     uint16_t DozAllow;
     uint16_t DozOpen;
     uint16_t DozEnd;
     uint16_t DozEmpty;
  } flags;
  uint8_t wp_num;
  uint8_t reserv;
  wp_data_t wp[4];
} dozator_data_t;
#pragma pack(pop)



class upak_compile
{
 private:
    static QString ErrData;
    static int LastErrCode;

    static QString ErrString;

    static QStringList rules;
    static QHash<QString, uint16_t> hash_rules;

    static int jump_stack[50];
    static int jump_stack_ptr;


    static bool AddRules(QString& str);
    static QString GetRegName(QString num);

    static bool is_special_character(QChar c);

    static bool is_operation(QChar c);
    static int get_priority(QChar op);

    static bool check_string(const QString& str);
    static bool make_poland(QString& str, QChar ch);
    static bool check_poland_string(const QString& str);
    static bool add_bytecode(const QString& str);


public:

    static QString GetLastErr(){return ErrString;}

    static void ClearRules();
    static bool UpdateRules(const QStringList &list);
    static void ConvertNumAndChars(QStringList& list, ConvertDirection dir);
    static QString& ConvertNumAndChars(QString& str, ConvertDirection dir);

    static void ReplaceText(QStringList& list, const QString before, const QString after);

    static bool Compile(const QStringList &list);

    static int compile_dosers(uint8_t *ptr);
    static void ResetDozators();

    static ushort mas[5*UPAK_MAX_SIZE/2];  //компилированный код упака ; с 5кратным запасом
    static ushort mas_words;                   //счетчик компилированных слов

    static upak_dozator_data_t dozator_data[10];
    static int NOfDoz;

    static QSet<uint16_t> USED_REGS;

    static QString GetRegNum(QString name);
    static int GetRegNum(QStringRef word);
};

#endif // UPAK_COMPILE_H
