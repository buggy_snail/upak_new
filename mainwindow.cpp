#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QComboBox>
#include <QFileDialog>
#include <QFile>
#include <QIODevice>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QMetaType>
#include <QAction>
#include <QTextCodec>


#include "common.h"
#include "comboboxitem.h"
#include "lineedititem.h"
#include "spinboxitem.h"
#include "doublespinboxitem.h"


uint16_t MainWindow::REGS[256];


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //восстановление колбеков меню
    connect(ui->action, &QAction::triggered, this, &MainWindow::action_activated);
    connect(ui->action_2, &QAction::triggered, this, &MainWindow::action_2_activated);
    connect(ui->action_3, &QAction::triggered, this, &MainWindow::action_3_activated);
    connect(ui->action_4, &QAction::triggered, this, &MainWindow::action_4_activated);
    connect(ui->action_6, &QAction::triggered, this, &MainWindow::action_6_activated);
    connect(ui->action_7, &QAction::triggered, this, &MainWindow::action_7_activated);
    connect(ui->action_11, &QAction::triggered, this, &MainWindow::action_11_activated);
    connect(ui->action_LP_ELL, &QAction::triggered, this, &MainWindow::import_ell_callback);
    //---------------------

    connect(ui->btn_EraseProgram, &QPushButton::clicked, this, &MainWindow::EraseUPAK);

    find_dialog.setParent(this);

    Qt::WindowFlags	wFlags;
    wFlags = find_dialog.windowFlags();
    wFlags = (  Qt::Tool | Qt::MSWindowsFixedSizeDialogHint);
    wFlags &= ~Qt::WindowContextHelpButtonHint;
    wFlags &= ~Qt::WindowSystemMenuHint;
    find_dialog.setWindowFlags(wFlags);


    QAction *ctrl_f = new QAction(this);
    ctrl_f->setShortcuts(QKeySequence::Find);
    connect(ctrl_f, SIGNAL(triggered()), this, SLOT(action_4_activated()));
    ui->plainTextEdit_ProgramText->addAction(ctrl_f);


    //диалог поиска.  событие нажатия кнопки 'искать'
    connect(&find_dialog, SIGNAL(find_pressed()), this, SLOT(find_text()));
    //событие скрытия диалога поиска
    connect(&find_dialog, SIGNAL(find_hide()), this, SLOT(find_text_hide()));

    ui->plainTextEdit_ProgramText->setLineWrapMode(QPlainTextEdit::NoWrap);


    //подсветка синтаксиса
    highlighter = new Highlighter(ui->plainTextEdit_ProgramText->document(), MainWindow::GetReg);

    //создаем с статусбаре виджеты для статуса ком-порта и т.п.
    line_number_label = new QLabel();
    ui->statusBar->addPermanentWidget(line_number_label);

    QFrame *h_spacer = new QFrame(ui->statusBar);
    h_spacer->setObjectName(QString::fromUtf8("spacer_1"));
    h_spacer->setFixedWidth(15);
    ui->statusBar->addPermanentWidget(h_spacer);

    COM_status_label = new QLabel();
    ui->statusBar->addPermanentWidget(COM_status_label);

    h_spacer = new QFrame(ui->statusBar);
    h_spacer->setObjectName(QString::fromUtf8("spacer_2"));
    h_spacer->setFixedWidth(15);
    ui->statusBar->addPermanentWidget(h_spacer);

    ProgressBar = new QProgressBar(this);
    ProgressBar->setMaximum(100);
    ProgressBar->setMinimum(0);
    ProgressBar->setTextVisible(false);
    ProgressBar->setVisible(false);
    ui->statusBar->addPermanentWidget(ProgressBar);
    ProgressBar->setValue(0);


    //при запуске всегда показываем главную вкладку с упаковской программой
    ui->tabWidget->setCurrentIndex(0);

    //инициализация всякой графики в главном окне
    CreateDozatorTree();
    CreateModulesTree();

    //настройки для COM-порта
    GetComSettings(settings());

    //соединяем сигнал потока обмена с ф-ией приема результатов
    qRegisterMetaType<message_data_t>("message_data_t");
    qRegisterMetaType<MSG_STATUS>("MSG_STATUS");
    connect(&port, SIGNAL(task_done(message_data_t, MSG_STATUS)), this, SLOT(modbus_reply(message_data_t, MSG_STATUS)), Qt::QueuedConnection);

    //соединяем сигнал хода процесса от потока обмена с ф-ией обновления прогресс-бара
    connect(&port, SIGNAL(progress(int)), this, SLOT(UpdateProgressBar(int)), Qt::QueuedConnection);


    //пытаемся сразу при запуске открыть порт со значениями по-умолчанию
    this->OpenCOM();

    //привязываем все сигналы об изменении с виджетов к обновлению флага необходимости перекомпиляции
    connect(ui->plainTextEdit_ProgramText, SIGNAL(textChanged()), this, SLOT(content_changed()));
    connect(ui->spinBox_et01num, SIGNAL(valueChanged(int)), this, SLOT(content_changed()));
    connect(ui->spinBox_et05num, SIGNAL(valueChanged(int)), this, SLOT(content_changed()));
    connect(ui->spinBox_et07num, SIGNAL(valueChanged(int)), this, SLOT(content_changed()));
    connect(ui->tableTimers, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(content_changed()));
    connect(ui->table100msecTimers, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(content_changed()));
    connect(ui->tableCounters, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(content_changed()));

    //привязываем сигналы об изменении флажков к ф-ии, посылающей изменения в прибор
    deny_flags_update_flag=true;
    connect(ui->listWidget_Regs, SIGNAL(itemChanged ( QListWidgetItem*)), this, SLOT(flags_changed(QListWidgetItem*)));

    //таймер периодического опроса регистров
    poll_init();


}


MainWindow::~MainWindow()
{
    if(port.IsOpened()) port.Close();
    delete ui;
}


void MainWindow::WriteStatus(QString str){
  ui->statusBar->showMessage(str, 4000);
}


template<typename T>
uint8_t* SetBuf(uint8_t *ptr,  T par){
 *reinterpret_cast<T*>(ptr)=par;
 return ptr+sizeof(T);
}

#define OPEN_SECTION(s)  section_ptr=upak_ptr;               \
                         SetBuf(section_ptr, static_cast<uint16_t>(s)); \
                         upak_ptr=section_ptr+4

#define CLOSE_SECTION() *reinterpret_cast<uint16_t*>(section_ptr+2)=static_cast<uint16_t>(upak_ptr-(section_ptr+4))



void MainWindow::on_btn_Compile_released()
{
    QMessageBox msgBox;
    bool res;
    //--------------------
    uint8_t  *upak_ptr, *section_ptr;
    upak_structure_t *upak_header;
    //--------------------
    int count, bytes;

    ui->btn_Compile->setEnabled(false);

    upak_compile::USED_REGS.clear();

    // преобразую в список строк, с учетом пустых строк для корректного отображения номеров строк в сообщениях об ошибках
    QStringList list = ui->plainTextEdit_ProgramText->toPlainText().split("\n", QString::KeepEmptyParts);

    res=upak_compile::UpdateRules(list);

    if(res){
       upak_compile::ConvertNumAndChars(list, ToDigits);

       res = upak_compile::Compile(list);
    }

    if(!res){
        msgBox.setText(upak_compile::GetLastErr());
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
        goto exit;
     }

     //указатель на начало записи данных в упаке
    upak_ptr = &upak_mas[6];   //начинаем с 6-го байта, сразу после заголовка, который заполним в конце
    OPEN_SECTION(LABEL_MAIN_PROGRAM);
     memcpy(upak_ptr, &upak_compile::mas, upak_compile::mas_words*2);
     upak_ptr+=upak_compile::mas_words*2;
    CLOSE_SECTION();

     //записываю таймеры, 100мсек_таймеры, счетчики
    OPEN_SECTION(LABEL_TIMERS);
     count=compile_timers(upak_ptr);
     if(count<0) goto exit;
     else upak_ptr+=count*6;
    CLOSE_SECTION();

    OPEN_SECTION(LABEL_MSEC_TIMERS);
     count=compile_100msec_timers(upak_ptr);
     if(count<0) goto exit;
     else upak_ptr+=count*6;
    CLOSE_SECTION();

    OPEN_SECTION(LABEL_COUNTERS);
     count=compile_counters(upak_ptr);
     if(count<0) goto exit;
     else upak_ptr+=count*8;
    CLOSE_SECTION();

    //компилирую дозаторы
    OPEN_SECTION(LABEL_DOSERS);
     bytes=upak_compile::compile_dosers(upak_ptr);
     if(bytes<0){
         msgBox.setText(upak_compile::GetLastErr());
         msgBox.setIcon(QMessageBox::Critical);
         msgBox.exec();
         goto exit;
     }
     upak_ptr+=bytes;
    CLOSE_SECTION();

    //компилирую модули
    OPEN_SECTION(LABEL_MODULES);
     bytes=compile_modules(upak_ptr);
     if(bytes<0) goto exit;
     else upak_ptr+=bytes;
    CLOSE_SECTION();


    //записываю шапку упака
    upak_header=reinterpret_cast<upak_structure_t*>(&upak_mas);
    upak_header->size=static_cast<unsigned short>(upak_ptr-&upak_mas[6]);
    port.fast_crc16(reinterpret_cast<uchar*>(&upak_header->crc16) , reinterpret_cast<uchar*>(&upak_mas[6]), upak_header->size);
    upak_header->signature=0x5A5A;

    this->upak_bytes=upak_header->size+6;

    if(this->upak_bytes>UPAK_MAX_SIZE){
        msgBox.setText("Ошибка! Текущая длина скомпилированной программы ("+QString::number(this->upak_bytes)+"байт) превышает максимально допустимую ("+QString::number(UPAK_MAX_SIZE)+"байт) для прибора!");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
        goto exit;
     }


    this->WriteStatus("Успешно!");

    this->RebuildRegList();


    content_changed_flag=false;

  exit:
    ui->btn_Compile->setEnabled(true);
}


int MainWindow::compile_timers(uint8_t *ptr){
  QMessageBox msgBox;
  QStringList columns[3];
  QTableWidgetItem *table_item;
  int items_in_row;
  QString str;
  uint16_t in, out, value;
  uint tmp32;
  int count;
  bool ok;
  uint16_t *local_ptr = reinterpret_cast<uint16_t*>(ptr);

  msgBox.setIcon(QMessageBox::Critical);

    for(int i=0; i<3; i++) columns[i].clear();

    //собираю в список все строчки таймеров, где заполнены все поля , либо не заполнены вообще
    for(int i=0; i<40; i++){
        items_in_row=0;
        for(int j=0; j<3; j++){
          table_item = ui->tableTimers->item(i,j);
          if(table_item){
              columns[j].append(table_item->text());
              if(table_item->text().length()) items_in_row++;
          }
          else columns[j].append("");
        }
        if(items_in_row!=0 && items_in_row!=3){
            msgBox.setText("Ошибка при компиляции таймеров! Заданы не все поля таймера. Таймер №" + QString::number(i+1));
            msgBox.exec();
            return -1;
        }
    }

    for(int i=0; i<2; i++){
        upak_compile::ConvertNumAndChars(columns[i], ToDigits);
    }

    count=0;

    for(int i=0; i<40; i++){
        str=columns[0][i];
        if(str.length()) count++; else continue;

        tmp32=str.toUInt(&ok);
        if(!ok){
            msgBox.setText("Ошибка при компиляции таймеров! Вход таймера имеет неверное значение! Таймер №" + QString::number(i+1));
            msgBox.exec();
            return -1;
        }
        if(tmp32>4095){
            msgBox.setText("Ошибка при компиляции таймеров! Вход таймера >4095! Таймер №" + QString::number(i+1));
            msgBox.exec();
            return -1;
        }
        in=static_cast<uint16_t>(tmp32);


        str=columns[1][i];
        tmp32=str.toUInt(&ok);
        if(!ok){
            msgBox.setText("Ошибка при компиляции таймеров! Выход таймера имеет неверное значение! Таймер №" + QString::number(i+1));
            msgBox.exec();
            return -1;
        }
        if(tmp32>4095){
            msgBox.setText("Ошибка при компиляции таймеров! Выход таймера >4095! Таймер №" + QString::number(i+1));
            msgBox.exec();
            return -1;
        }
        out=static_cast<uint16_t>(tmp32);

        str=columns[2][i];
        tmp32=str.toUInt(&ok);
        if(!ok){
            msgBox.setText("Ошибка при компиляции таймеров! Уставка таймера имеет неверное значение! Таймер №" + QString::number(i+1));
            msgBox.exec();
            return -1;
        }
        if(tmp32>0xffff){
            msgBox.setText("Ошибка при компиляции таймеров! Уставка таймера >65535! Таймер №" + QString::number(i+1));
            msgBox.exec();
            return -1;
        }
        value=static_cast<uint16_t>(tmp32);

        //записываю очередной таймер
        *local_ptr=in; local_ptr++;
        *local_ptr=out; local_ptr++;
        *local_ptr=value; local_ptr++;

        upak_compile::USED_REGS+=in;
        upak_compile::USED_REGS+=out;
    }

  return count;
}


int MainWindow::compile_100msec_timers(uint8_t *ptr){
    QMessageBox msgBox;
    QStringList columns[3];
    QTableWidgetItem *table_item;
    int items_in_row;
    QString str;
    uint16_t in, out, value;
    uint tmp32;
    int count;
    bool ok;
    uint16_t *local_ptr = reinterpret_cast<uint16_t*>(ptr);

    msgBox.setIcon(QMessageBox::Critical);

      for(int i=0; i<3; i++) columns[i].clear();

      //собираю в список все строчки 100мсек таймеров, где заполнены все поля , либо не заполнены вообще
      for(int i=0; i<10; i++){
          items_in_row=0;
          for(int j=0; j<3; j++){
            table_item = ui->table100msecTimers->item(i,j);
            if(table_item){
                columns[j].append(table_item->text());
                if(table_item->text().length()) items_in_row++;
            }
            else columns[j].append("");
          }
          if(items_in_row!=0 && items_in_row!=3){
              msgBox.setText("Ошибка при компиляции 100мсек таймеров! Заданы не все поля таймера. Таймер №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
      }

      for(int i=0; i<2; i++){
          upak_compile::ConvertNumAndChars(columns[i], ToDigits);
      }

      count=0;

      for(int i=0; i<10; i++){
          str=columns[0][i];
          if(str.length()) count++; else continue;

          tmp32=str.toUInt(&ok);
          if(!ok){
              msgBox.setText("Ошибка при компиляции 100мсек таймеров! Вход таймера имеет неверное значение! Таймер №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          if(tmp32>4095){
              msgBox.setText("Ошибка при компиляции 100мсек таймеров! Вход таймера >4095! Таймер №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          in=static_cast<uint16_t>(tmp32);


          str=columns[1][i];
          tmp32=str.toUInt(&ok);
          if(!ok){
              msgBox.setText("Ошибка при компиляции 100мсек таймеров! Выход таймера имеет неверное значение! Таймер №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          if(tmp32>4095){
              msgBox.setText("Ошибка при компиляции 100мсек таймеров! Выход таймера >4095! Таймер №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          out=static_cast<uint16_t>(tmp32);

          str=columns[2][i];
          tmp32=str.toUInt(&ok);
          if(!ok){
              msgBox.setText("Ошибка при компиляции 100мсек таймеров! Уставка таймера имеет неверное значение! Таймер №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          if(tmp32>0xffff){
              msgBox.setText("Ошибка при компиляции 100мсек таймеров! Уставка таймера >65535! Таймер №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          if(tmp32==0){
              msgBox.setText("Ошибка при компиляции 100мсек таймеров! Уставка 100мсек таймера не может быть нулевой! Таймер №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          value=static_cast<uint16_t>(tmp32);

          //записываю очередной 100мсек таймер
          *local_ptr=in; local_ptr++;
          *local_ptr=out; local_ptr++;
          *local_ptr=value; local_ptr++;

          upak_compile::USED_REGS+=in;
          upak_compile::USED_REGS+=out;
      }

    return count;
}


int MainWindow::compile_counters(uint8_t *ptr){
    QMessageBox msgBox;
    QStringList columns[4];
    QTableWidgetItem *table_item;
    int items_in_row;
    QString str;
    uint16_t in, reset, out, value;
    uint tmp32;
    int count;
    bool ok;
    uint16_t *local_ptr = reinterpret_cast<uint16_t*>(ptr);

    msgBox.setIcon(QMessageBox::Critical);


      for(int i=0; i<4; i++) columns[i].clear();

      //собираю в список все строчки счетчиков, где заполнены все поля , либо не заполнены вообще
      for(int i=0; i<20; i++){
          items_in_row=0;
          for(int j=0; j<4; j++){
            table_item = ui->tableCounters->item(i,j);
            if(table_item){
                columns[j].append(table_item->text());
                if(table_item->text().length()) items_in_row++;
            }
            else columns[j].append("");
          }
          if(items_in_row!=0 && items_in_row!=4){
              msgBox.setText("Ошибка при компиляции счетчиков! Заданы не все поля счетчика. Счетчик №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
      }

      for(int i=0; i<3; i++){
          upak_compile::ConvertNumAndChars(columns[i], ToDigits);
      }

      count=0;

      for(int i=0; i<20; i++){
          str=columns[0][i];
          if(str.length()) count++; else continue;

          tmp32=str.toUInt(&ok);
          if(!ok){
              msgBox.setText("Ошибка при компиляции счетчиков! Вход счетчика имеет неверное значение! Счетчик №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          if(tmp32>4095){
              msgBox.setText("Ошибка при компиляции счетчиков! Вход счетчика >4095! Счетчик №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          in=static_cast<uint16_t>(tmp32);

          str=columns[1][i];
          tmp32=str.toUInt(&ok);
          if(!ok){
              msgBox.setText("Ошибка при компиляции счетчиков! Вход сброса счетчика имеет неверное значение! Счетчик №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          if(tmp32>4095){
              msgBox.setText("Ошибка при компиляции счетчиков! Вход сброса счетчика >4095! Счетчик №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          reset=static_cast<uint16_t>(tmp32);

          str=columns[2][i];
          tmp32=str.toUInt(&ok);
          if(!ok){
              msgBox.setText("Ошибка при компиляции счетчиков! Выход счетчика имеет неверное значение! Счетчик №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          if(tmp32>4095){
              msgBox.setText("Ошибка при компиляции счетчиков! Выход счетчика >4095! Счетчик №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          out=static_cast<uint16_t>(tmp32);

          str=columns[3][i];
          tmp32=str.toUInt(&ok);
          if(!ok){
              msgBox.setText("Ошибка при компиляции счетчиков! Уставка счетчика имеет неверное значение! Счетчик №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          if(tmp32>0xffff){
              msgBox.setText("Ошибка при компиляции счетчиков! Уставка счетчика >65535! Счетчик №" + QString::number(i+1));
              msgBox.exec();
              return -1;
          }
          value=static_cast<uint16_t>(tmp32);

          //записываю очередной счетчик
          *local_ptr=in; local_ptr++;
          *local_ptr=reset; local_ptr++;
          *local_ptr=out; local_ptr++;
          *local_ptr=value; local_ptr++;

          upak_compile::USED_REGS+=in;
          upak_compile::USED_REGS+=reset;
          upak_compile::USED_REGS+=out;
      }

    return count;
}


int MainWindow::compile_modules(uint8_t *ptr){
 QMessageBox msgBox;
 poll_table_t *poll_item_ptr;
 QSet<uint8_t> set_adr;
 QSet<uint8_t> set_regs;
 QTreeWidgetItem *top;
 SpinBoxItem *SpinBox;
 ComboBoxItem *combo;
 uint8_t adr;
 uint16_t reg;
 int val;

   //записываю количество записей в таблице опроса
   *reinterpret_cast<uint16_t*>(ptr)=static_cast<uint16_t>(ui->spinBox_et01num->value()+ui->spinBox_et05num->value()+ui->spinBox_et07num->value());

   poll_item_ptr = reinterpret_cast<poll_table_t*>(ptr+2);

   //заполняю саму таблицу опроса
   if(*reinterpret_cast<uint16_t*>(ptr)){

       //заполняю множество адресов set_adr адресами 04х
       top = ui->treeDozator->topLevelItem(0);
       combo = reinterpret_cast<ComboBoxItem*>(ui->treeDozator->itemWidget(top, 1));
       for(int i=combo->currentIndex()-1; i>=0; i--){
          SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeDozator->itemWidget(top->child(i)->child(2), 1));
          val = SpinBox->value();
          adr = static_cast<uint8_t>(val);
          if(!set_adr.contains(adr)) set_adr<<adr;
       }

       //записываю модули ЕТ-01
       top = ui->treeModules->topLevelItem(0);
       for(int i=0; i<ui->spinBox_et01num->value(); i++){
           poll_item_ptr->type=MODULE_TYPE_01;

           SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(top->child(i), 1));
           if(!SpinBox) return -1;
           val = SpinBox->value();
           adr=static_cast<uint8_t>(val);
           if(set_adr.contains(adr)){
               msgBox.setText("Ошибка таблицы модулей! Модуль ЕТ-01 с номером в списке " + QString::number(i+1) + " имеет не уникальный адрес MODBUS !");
               msgBox.setIcon(QMessageBox::Critical);
               msgBox.exec();
               return -1;
           }
           set_adr<<adr;
           poll_item_ptr->adr=adr;

           SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(top->child(i), 2));
           if(!SpinBox) return -1;
           val = SpinBox->value();
           reg = static_cast<uint16_t>(val)>>3;
           if(set_regs.contains(static_cast<uint8_t>(reg)) || set_regs.contains(static_cast<uint8_t>(reg+1))){
               msgBox.setText("Ошибка таблицы модулей! Модуль ЕТ-01 с номером в списке " + QString::number(i+1) + " имеет диапазон регистров-входов, пересекающийся с диапазоном другого модуля!");
               msgBox.setIcon(QMessageBox::Critical);
               msgBox.exec();
               return -1;
           }
           set_regs<<static_cast<uint8_t>(reg);
           set_regs<<static_cast<uint8_t>(reg+1);
           poll_item_ptr->regs_in=reg;

           reg<<=3;
           for(int r=reg; r<reg+16; r++) upak_compile::USED_REGS+=static_cast<uint16_t>(r);


           SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(top->child(i), 3));
           if(!SpinBox) return -1;
           val = SpinBox->value();
           reg = static_cast<uint16_t>(val)>>3;
           if(set_regs.contains(static_cast<uint8_t>(reg))){
               msgBox.setText("Ошибка таблицы модулей! Модуль ЕТ-01 с номером в списке " + QString::number(i+1) + " имеет диапазон регистров-выходов, пересекающийся с диапазоном другого модуля!");
               msgBox.setIcon(QMessageBox::Critical);
               msgBox.exec();
               return -1;
           }
           set_regs<<static_cast<uint8_t>(reg);
           poll_item_ptr->regs_out=reg;
           poll_item_ptr++;

           reg<<=3;
           for(int r=reg; r<reg+8; r++) upak_compile::USED_REGS+=static_cast<uint16_t>(r);
       }

       //записываю модули ЕТ-05
       top = ui->treeModules->topLevelItem(1);
       for(int i=0; i<ui->spinBox_et05num->value(); i++){
           poll_item_ptr->type=MODULE_TYPE_05;

           SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(top->child(i), 1));
           if(!SpinBox) return -1;
           val = SpinBox->value();
           adr = static_cast<uint8_t>(val);
           if(set_adr.contains(adr)){
               msgBox.setText("Ошибка таблицы модулей! Модуль ЕТ-05 с номером в списке " + QString::number(i+1) + " имеет не уникальный адрес MODBUS !");
               msgBox.setIcon(QMessageBox::Critical);
               msgBox.exec();
               return -1;
           }
           set_adr<<adr;
           poll_item_ptr->adr=adr;

           SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(top->child(i), 3));
           if(!SpinBox) return -1;
           val = SpinBox->value();
           reg = static_cast<uint16_t>(val)>>3;
           if(set_regs.contains(static_cast<uint8_t>(reg)) || set_regs.contains(static_cast<uint8_t>(reg+1)) || set_regs.contains(static_cast<uint8_t>(reg+2))){
               msgBox.setText("Ошибка таблицы модулей! Модуль ЕТ-05 с номером в списке " + QString::number(i+1) + " имеет диапазон регистров, пересекающийся с диапазоном другого модуля!");
               msgBox.setIcon(QMessageBox::Critical);
               msgBox.exec();
               return -1;
           }
           set_regs<<static_cast<uint8_t>(reg);
           set_regs<<static_cast<uint8_t>(reg+1);
           set_regs<<static_cast<uint8_t>(reg+2);
           poll_item_ptr->regs_out=reg;
           poll_item_ptr++;

           reg<<=3;
           for(int r=reg; r<reg+24; r++) upak_compile::USED_REGS+=static_cast<uint16_t>(r);
       }


       //записываю модули ЕТ-07
       top = ui->treeModules->topLevelItem(2);
       for(int i=0; i<ui->spinBox_et07num->value(); i++){
           poll_item_ptr->type=MODULE_TYPE_07;

           SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(top->child(i), 1));
           if(!SpinBox) return -1;
           val = SpinBox->value();
           adr=static_cast<uint8_t>(val);
           if(set_adr.contains(adr)){
               msgBox.setText("Ошибка таблицы модулей! Модуль ЕТ-07 с номером в списке " + QString::number(i+1) + " имеет не уникальный адрес MODBUS !");
               msgBox.setIcon(QMessageBox::Critical);
               msgBox.exec();
               return -1;
           }
           set_adr<<adr;
           poll_item_ptr->adr=adr;

           SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(top->child(i), 2));
           if(!SpinBox) return -1;
           val = SpinBox->value();
           reg = static_cast<uint16_t>(val)>>3;
           if(set_regs.contains(static_cast<uint8_t>(reg)) || set_regs.contains(static_cast<uint8_t>(reg+1)) || set_regs.contains(static_cast<uint8_t>(reg+2))){
               msgBox.setText("Ошибка таблицы модулей! Модуль ЕТ-07 с номером в списке " + QString::number(i+1) + " имеет диапазон регистров, пересекающийся с диапазоном другого модуля!");
               msgBox.setIcon(QMessageBox::Critical);
               msgBox.exec();
               return -1;
           }
           set_regs<<static_cast<uint8_t>(reg);
           set_regs<<static_cast<uint8_t>(reg+1);
           set_regs<<static_cast<uint8_t>(reg+2);
           poll_item_ptr->regs_in=reg;
           poll_item_ptr++;

           reg<<=3;
           for(int r=reg; r<reg+24; r++) upak_compile::USED_REGS+=static_cast<uint16_t>(r);
       }

     }

   return (*reinterpret_cast<uint16_t*>(ptr)) * sizeof(poll_table_t) + 2;
}


void MainWindow::ConvertNumChars(ConvertDirection dir){
    QMessageBox msgBox;
    QTableWidgetItem *table_item;
    bool res;
    QStringList columns[3];
    int cnt;

    ui->btn_ConvertToChars->setEnabled(false);
    ui->btn_ConvertToDigits->setEnabled(false);

    upak_compile::ClearRules();

    QStringList list = ui->plainTextEdit_ProgramText->toPlainText().split("\n", QString::KeepEmptyParts);

    res = upak_compile::UpdateRules(list);
    if(res){
      upak_compile::ConvertNumAndChars(list, dir);
    }

    if(!res){
        msgBox.setText(upak_compile::GetLastErr());
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }else{
       ui->plainTextEdit_ProgramText->setPlainText(list.join("\n"));

       //ковертирую текст в таблице таймеров
       for(int j=0; j<2; j++) columns[j].clear();
       for(int i=0; i<40; i++){
           for(int j=0; j<2; j++){
             table_item = ui->tableTimers->item(i,j);
             if(table_item) columns[j].append(table_item->text());
             else columns[j].append("");
           }
       }
       for(int j=0; j<2; j++){
           upak_compile::ConvertNumAndChars(columns[j], dir);
       }
       for(int i=0; i<40; i++)
           for(int j=0; j<2; j++){
             table_item = ui->tableTimers->item(i,j);
             if(table_item) table_item->setText(columns[j][i]);
           }

       //ковертирую текст в таблице 100мсек таймеров
       for(int j=0; j<2; j++) columns[j].clear();
       for(int i=0; i<10; i++){
           for(int j=0; j<2; j++){
             table_item = ui->table100msecTimers->item(i,j);
             if(table_item) columns[j].append(table_item->text());
             else columns[j].append("");
           }
       }
       for(int j=0; j<2; j++){
           upak_compile::ConvertNumAndChars(columns[j], dir);
       }
       for(int i=0; i<10; i++)
           for(int j=0; j<2; j++){
             table_item = ui->table100msecTimers->item(i,j);
             if(table_item) table_item->setText(columns[j][i]);
           }

       //ковертирую текст в таблице счетчиков
       for(int j=0; j<3; j++) columns[j].clear();
       for(int i=0; i<20; i++){
           for(int j=0; j<3; j++){
             table_item = ui->tableCounters->item(i,j);
             if(table_item) columns[j].append(table_item->text());
             else columns[j].append("");
           }
       }
       for(int j=0; j<3; j++){
           upak_compile::ConvertNumAndChars(columns[j], dir);
       }
       for(int i=0; i<20; i++)
           for(int j=0; j<3; j++){
             table_item = ui->tableCounters->item(i,j);
             if(table_item) table_item->setText(columns[j][i]);
           }


       //конвертация в дереве параметров дозаторов
       columns[0].clear();

       for(int i=0; i<10; i++){
           columns[0].append(upak_compile::dozator_data[i].flags.error_load);
           columns[0].append(upak_compile::dozator_data[i].flags.error_zero);
           columns[0].append(upak_compile::dozator_data[i].flags.wait_weighter);
           columns[0].append(upak_compile::dozator_data[i].flags.substr_flag);
           columns[0].append(upak_compile::dozator_data[i].flags.DozAllow);
           columns[0].append(upak_compile::dozator_data[i].flags.DozOpen);
           columns[0].append(upak_compile::dozator_data[i].flags.DozEnd);
           columns[0].append(upak_compile::dozator_data[i].flags.DozEmpty);

           for(int j=0; j<16; j++){
               columns[0].append(upak_compile::dozator_data[i].flags.speed_12[j][0]);
               columns[0].append(upak_compile::dozator_data[i].flags.speed_12[j][1]);
           }

           for(int j=0; j<4; j++)
              columns[0].append(upak_compile::dozator_data[i].wp[j].reg);
       }

       upak_compile::ConvertNumAndChars(columns[0], dir);

       cnt=0;
       for(int i=0; i<10; i++){
           upak_compile::dozator_data[i].flags.error_load=columns[0][cnt++];
           upak_compile::dozator_data[i].flags.error_zero=columns[0][cnt++];
           upak_compile::dozator_data[i].flags.wait_weighter=columns[0][cnt++];
           upak_compile::dozator_data[i].flags.substr_flag=columns[0][cnt++];
           upak_compile::dozator_data[i].flags.DozAllow=columns[0][cnt++];
           upak_compile::dozator_data[i].flags.DozOpen=columns[0][cnt++];
           upak_compile::dozator_data[i].flags.DozEnd=columns[0][cnt++];
           upak_compile::dozator_data[i].flags.DozEmpty=columns[0][cnt++];

           for(int j=0; j<16; j++){
               upak_compile::dozator_data[i].flags.speed_12[j][0]=columns[0][cnt++];
               upak_compile::dozator_data[i].flags.speed_12[j][1]=columns[0][cnt++];
           }

           for(int j=0; j<4; j++)
              upak_compile::dozator_data[i].wp[j].reg=columns[0][cnt++];
       }

       UpdateDozatorTreeRegs();

       this->WriteStatus("Успешно!");
    }


    ui->btn_ConvertToChars->setEnabled(true);
    ui->btn_ConvertToDigits->setEnabled(true);
}


void MainWindow::on_btn_ConvertToChars_released()
{
    ConvertNumChars(ToChars);
}

void MainWindow::on_btn_ConvertToDigits_released()
{
    ConvertNumChars(ToDigits);
}



void MainWindow::CreateDozatorTree(){
   QTreeWidgetItem *root, *item, *doz, *wp, *sub_item, *flags;
   ComboBoxItem *combo;
   LineEditItem *line;
   DoubleSpinBoxItem *DoubleSpinBox;
   SpinBoxItem *SpinBox;
   int doz_num;

   DozatorTreeCreatedFlag=false;

   upak_compile::ResetDozators();

   ui->treeDozator->clear();

   //добавляем корневой элемент с количеством дозаторов
   item = new QTreeWidgetItem();
   item->setText(0,"Количество дозаторов");
   ui->treeDozator->addTopLevelItem(item);
   combo = new ComboBoxItem(item, 1);
   for(int i=0; i<=10; i++) combo->addItem(QString::number(i));
   combo->setCurrentIndex(0);
   ui->treeDozator->setItemWidget(item, 1, combo);
   item->setExpanded(true);

   root=item;

   for(doz_num=0; doz_num<10; doz_num++){
       item = new QTreeWidgetItem();
       item->setText(0,"Дозатор "+QString::number(doz_num+1));
       root->addChild(item);
       item->setExpanded(false);
       item->setHidden(true);
       item->setFlags(Qt::ItemIsEnabled);

       doz=item;

       //добавление пунктов в ветку дозатора

       item = new QTreeWidgetItem();
       item->setText(0,"Тип дозатора");
       doz->addChild(item);
       //item->setHidden(true);
       item->setFlags(Qt::ItemIsEnabled);
       combo = new ComboBoxItem(item, 1);
       combo->addItem("Весовой");
       combo->addItem("Весовой ПВ");
       combo->addItem("Вычитающий");
       combo->addItem("Импульсный");
       ui->treeDozator->setItemWidget(item, 1, combo);

       item = new QTreeWidgetItem();
       item->setText(0,"Кол-во компонентов");
       doz->addChild(item);
       //item->setHidden(true);
       item->setFlags(Qt::ItemIsEnabled);
       combo = new ComboBoxItem(item, 1);
       for(int i=1; i<=16; i++) combo->addItem(QString::number(i));
       ui->treeDozator->setItemWidget(item, 1, combo);


       item = new QTreeWidgetItem();
       item->setText(0,"Адрес ЕТ-04");
       doz->addChild(item);
       //item->setHidden(true);
       item->setFlags(Qt::ItemIsEnabled);
       SpinBox = new SpinBoxItem(item, 1);
       SpinBox->setSingleStep(1);
       SpinBox->setMaximum(254);
       SpinBox->setMinimum(1);
       ui->treeDozator->setItemWidget(item, 1, SpinBox);



       item = new QTreeWidgetItem();
       item->setText(0,"Канал ЕТ-04");
       doz->addChild(item);
       //item->setHidden(true);
       item->setFlags(Qt::ItemIsEnabled);
       combo = new ComboBoxItem(item, 1);
       for(int i=0; i<64; i++) combo->addItem(QString::number(i));
       ui->treeDozator->setItemWidget(item, 1, combo);




       //флаги

       item = new QTreeWidgetItem();
       item->setText(0,"Флаги");
       doz->addChild(item);
       item->setExpanded(false);
       //item->setHidden(true);
       item->setFlags(Qt::ItemIsEnabled);

       flags=item;

         //добавление основных флагов

         item = new QTreeWidgetItem();
         item->setText(0,"Ошибка дозирования");
         flags->addChild(item);
         //item->setHidden(true);
         item->setFlags(Qt::ItemIsEnabled);
         line = new LineEditItem(item, 1);
         ui->treeDozator->setItemWidget(item, 1, line);

         item = new QTreeWidgetItem();
         item->setText(0,"Ошибка нуля");
         flags->addChild(item);
         //item->setHidden(true);
         item->setFlags(Qt::ItemIsEnabled);
         line = new LineEditItem(item, 1);
         ui->treeDozator->setItemWidget(item, 1, line);

         item = new QTreeWidgetItem();
         item->setText(0,"Ожидание весовщика");
         flags->addChild(item);
         //item->setHidden(true);
         item->setFlags(Qt::ItemIsEnabled);
         line = new LineEditItem(item, 1);
         ui->treeDozator->setItemWidget(item, 1, line);

         item = new QTreeWidgetItem();
         item->setText(0,"Флаг Substr");
         flags->addChild(item);
         //item->setHidden(true);
         item->setFlags(Qt::ItemIsEnabled);
         line = new LineEditItem(item, 1);
         ui->treeDozator->setItemWidget(item, 1, line);

         item = new QTreeWidgetItem();
         item->setText(0,"DozAllow");
         flags->addChild(item);
         //item->setHidden(true);
         item->setFlags(Qt::ItemIsEnabled);
         line = new LineEditItem(item, 1);
         ui->treeDozator->setItemWidget(item, 1, line);

         item = new QTreeWidgetItem();
         item->setText(0,"DozOpen");
         flags->addChild(item);
         //item->setHidden(true);
         item->setFlags(Qt::ItemIsEnabled);
         line = new LineEditItem(item, 1);
         ui->treeDozator->setItemWidget(item, 1, line);

         item = new QTreeWidgetItem();
         item->setText(0,"DozEnd");
         flags->addChild(item);
         //item->setHidden(true);
         item->setFlags(Qt::ItemIsEnabled);
         line = new LineEditItem(item, 1);
         ui->treeDozator->setItemWidget(item, 1, line);

         item = new QTreeWidgetItem();
         item->setText(0,"DozEmpty");
         flags->addChild(item);
         //item->setHidden(true);
         item->setFlags(Qt::ItemIsEnabled);
         line = new LineEditItem(item, 1);
         ui->treeDozator->setItemWidget(item, 1, line);


         //добавление флагов скоростей

         item = new QTreeWidgetItem();
         item->setText(0,"Скорости");
         flags->addChild(item);
         item->setExpanded(false);
         //item->setHidden(true);
         item->setFlags(Qt::ItemIsEnabled);

         flags=item;

         for(int i=0; i<16; i++){

             item = new QTreeWidgetItem();
             item->setText(0,"Компонент " + QString::number(i+1));
             flags->addChild(item);
             item->setExpanded(false);
             item->setHidden(true);
             item->setFlags(Qt::ItemIsEnabled);

             sub_item = new QTreeWidgetItem();
             sub_item->setText(0,"1-ая скорость");
             item->addChild(sub_item);
             //item->setHidden(true);
             sub_item->setFlags(Qt::ItemIsEnabled);
             line = new LineEditItem(sub_item, 1);
             ui->treeDozator->setItemWidget(sub_item, 1, line);

             sub_item = new QTreeWidgetItem();
             sub_item->setText(0,"2-ая скорость");
             item->addChild(sub_item);
             //item->setHidden(true);
             sub_item->setFlags(Qt::ItemIsEnabled);
             line = new LineEditItem(sub_item, 1);
             ui->treeDozator->setItemWidget(sub_item, 1, line);

         }

       //----------



       item = new QTreeWidgetItem();
       item->setText(0,"Кол-во весовых точек");
       doz->addChild(item);
       //item->setHidden(true);
       item->setFlags(Qt::ItemIsEnabled);
       combo = new ComboBoxItem(item, 1);
       for(int i=0; i<=4; i++) combo->addItem(QString::number(i));
       ui->treeDozator->setItemWidget(item, 1, combo);

       wp=item;

       //весовые точки

       for(int i=0; i<4; i++){
           item = new QTreeWidgetItem();
           item->setText(0,"Точка "+QString::number(i+1));
           wp->addChild(item);
           item->setExpanded(false);
           item->setHidden(true);
           item->setFlags(Qt::ItemIsEnabled);

           //добавление параметров весовой точки

           sub_item = new QTreeWidgetItem();
           sub_item->setText(0,"Вес");
           item->addChild(sub_item);
           //sub_item->setHidden(true);
           sub_item->setFlags(Qt::ItemIsEnabled);
           DoubleSpinBox = new DoubleSpinBoxItem(sub_item, 1);
           DoubleSpinBox->setSingleStep(0.1);
           DoubleSpinBox->setMaximum(static_cast<double>(WP_MAX_WEIGHT));
           DoubleSpinBox->setMinimum(static_cast<double>(WP_MIN_WEIGHT));
           DoubleSpinBox->setDecimals(1);
           ui->treeDozator->setItemWidget(sub_item, 1, DoubleSpinBox);



           sub_item = new QTreeWidgetItem();
           sub_item->setText(0,"Гистерезис (%)");
           item->addChild(sub_item);
           //sub_item->setHidden(true);
           sub_item->setFlags(Qt::ItemIsEnabled);
           DoubleSpinBox = new DoubleSpinBoxItem(sub_item, 1);
           DoubleSpinBox->setSingleStep(0.1);
           DoubleSpinBox->setMaximum(static_cast<double>(WP_MAX_GIST));
           DoubleSpinBox->setMinimum(static_cast<double>(WP_MIN_GIST));
           DoubleSpinBox->setDecimals(1);
           ui->treeDozator->setItemWidget(sub_item, 1, DoubleSpinBox);



           sub_item = new QTreeWidgetItem();
           sub_item->setText(0,"Флаг");
           item->addChild(sub_item);
           //sub_item->setHidden(true);
           sub_item->setFlags(Qt::ItemIsEnabled);
           line = new LineEditItem(sub_item, 1);
           ui->treeDozator->setItemWidget(sub_item, 1, line);

       }

   }

   DozatorTreeCreatedFlag=true;


   UpdateDozatorTree();

}


void MainWindow::UpdateDozatorTree(){
    QTreeWidgetItem *top, *doz, *item;
    DoubleSpinBoxItem *DoubleSpinBox;
    SpinBoxItem *SpinBox;
    ComboBoxItem *combo;

    top=ui->treeDozator->topLevelItem(0);
    combo = reinterpret_cast<ComboBoxItem*>(ui->treeDozator->itemWidget(top, 1));
    combo->setCurrentIndex(upak_compile::NOfDoz);

    for(int i=0; i<10; i++){
        doz=top->child(i);

        item = doz->child(0);
        combo = reinterpret_cast<ComboBoxItem*>(ui->treeDozator->itemWidget(item, 1));
        combo->setCurrentIndex(upak_compile::dozator_data[i].dozator_type);

        item = doz->child(1);
        combo = reinterpret_cast<ComboBoxItem*>(ui->treeDozator->itemWidget(item, 1));
        combo->setCurrentIndex(upak_compile::dozator_data[i].NOfComp-1);
        UpdateBranch(doz->child(4)->child(8), upak_compile::dozator_data[i].NOfComp);

        item = doz->child(2);
        SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeDozator->itemWidget(item, 1));
        SpinBox->setValue(upak_compile::dozator_data[i].et04_adr);

        item = doz->child(3);
        combo = reinterpret_cast<ComboBoxItem*>(ui->treeDozator->itemWidget(item, 1));
        combo->setCurrentIndex(upak_compile::dozator_data[i].et04_channel);

        //весовые точки
        combo = reinterpret_cast<ComboBoxItem*>(ui->treeDozator->itemWidget(doz->child(5), 1));
        combo->setCurrentIndex(upak_compile::dozator_data[i].wp_num);

        for(int j=0; j<4; j++){
            item=doz->child(5)->child(j)->child(0);
            DoubleSpinBox = reinterpret_cast<DoubleSpinBoxItem*>(ui->treeDozator->itemWidget(item, 1));
            DoubleSpinBox->setValue(static_cast<double>(upak_compile::dozator_data[i].wp[j].weight));

            item=doz->child(5)->child(j)->child(1);
            DoubleSpinBox = reinterpret_cast<DoubleSpinBoxItem*>(ui->treeDozator->itemWidget(item, 1));
            DoubleSpinBox->setValue(static_cast<double>(upak_compile::dozator_data[i].wp[j].gist));
        }

    }

    UpdateDozatorTreeRegs();

    ui->treeDozator->header()->setMinimumSectionSize(100);
    ui->treeDozator->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->treeDozator->header()->resizeSection(1, 100);
    ui->treeDozator->header()->setStretchLastSection(false);
}


void MainWindow::UpdateDozatorTreeRegs(){
    QTreeWidgetItem *top, *doz, *flags, *item;
    LineEditItem *line;

    top=ui->treeDozator->topLevelItem(0);

    for(int i=0; i<10; i++){
        doz=top->child(i);
        flags=doz->child(4);

        item = flags->child(0);
        line = reinterpret_cast<LineEditItem*>(ui->treeDozator->itemWidget(item, 1));
        line->setText(upak_compile::dozator_data[i].flags.error_load);

        item = flags->child(1);
        line = reinterpret_cast<LineEditItem*>(ui->treeDozator->itemWidget(item, 1));
        line->setText(upak_compile::dozator_data[i].flags.error_zero);

        item = flags->child(2);
        line = reinterpret_cast<LineEditItem*>(ui->treeDozator->itemWidget(item, 1));
        line->setText(upak_compile::dozator_data[i].flags.wait_weighter);

        item = flags->child(3);
        line = reinterpret_cast<LineEditItem*>(ui->treeDozator->itemWidget(item, 1));
        line->setText(upak_compile::dozator_data[i].flags.substr_flag);

        item = flags->child(4);
        line = reinterpret_cast<LineEditItem*>(ui->treeDozator->itemWidget(item, 1));
        line->setText(upak_compile::dozator_data[i].flags.DozAllow);

        item = flags->child(5);
        line = reinterpret_cast<LineEditItem*>(ui->treeDozator->itemWidget(item, 1));
        line->setText(upak_compile::dozator_data[i].flags.DozOpen);

        item = flags->child(6);
        line = reinterpret_cast<LineEditItem*>(ui->treeDozator->itemWidget(item, 1));
        line->setText(upak_compile::dozator_data[i].flags.DozEnd);

        item = flags->child(7);
        line = reinterpret_cast<LineEditItem*>(ui->treeDozator->itemWidget(item, 1));
        line->setText(upak_compile::dozator_data[i].flags.DozEmpty);


        flags = flags->child(8);
        for(int j=0; j<16; j++){
           item = flags->child(j)->child(0);
           line = reinterpret_cast<LineEditItem*>(ui->treeDozator->itemWidget(item, 1));
           line->setText(upak_compile::dozator_data[i].flags.speed_12[j][0]);

           item = flags->child(j)->child(1);
           line = reinterpret_cast<LineEditItem*>(ui->treeDozator->itemWidget(item, 1));
           line->setText(upak_compile::dozator_data[i].flags.speed_12[j][1]);
        }

        flags=doz->child(5); //весовые точки
        for(int j=0; j<4; j++){
           item = flags->child(j)->child(2);
           line = reinterpret_cast<LineEditItem*>(ui->treeDozator->itemWidget(item, 1));
           line->setText(upak_compile::dozator_data[i].wp[j].reg);
        }

    }

}



void MainWindow::UpdateBranch(QTreeWidgetItem *root, int item_num){
 for(int i=0; i<root->childCount(); i++){
     if(i<item_num) root->child(i)->setHidden(false);
               else root->child(i)->setHidden(true);
 }
}


void MainWindow::on_treeDozator_itemChanged(QTreeWidgetItem *item, int column)
{
    QTreeWidgetItem *item_ptr;
    int stage, stage_index[5];
    int i;

    if(!DozatorTreeCreatedFlag) return;
    if(column!=1) return;

    this->content_changed_flag=true;

    // определение позиции изменившегося пункта в дереве
    stage=0;
    item_ptr=item;
    while((item_ptr=item_ptr->parent())) stage++;

    item_ptr=item;
    for(i=0; i<stage; i++){
        stage_index[(stage-1)-i] = item_ptr->parent()->indexOfChild(item_ptr);
        item_ptr = item_ptr->parent();
    }


    //сохранение в структуре изменившегося элемента
    //+ попутная коррекция количества пунктов дерева, если изменилось кол-во дозаторов, компонентов или весовых точек

    switch(stage){
     case 0: upak_compile::NOfDoz=item->text(1).toInt();  //изменилось кол-во дозаторов
             UpdateBranch(item, upak_compile::NOfDoz);

             break;
     case 2:  //поменялось что-то в корне дозатора
             switch(stage_index[1]){
              case 0: upak_compile::dozator_data[stage_index[0]].dozator_type=static_cast<uint8_t>(item->text(1).toInt()); break;
              case 1: i=item->text(1).toInt();
                      i++;
                      upak_compile::dozator_data[stage_index[0]].NOfComp=static_cast<uint8_t>(i);
                      item_ptr = item->parent();
                      UpdateBranch(item_ptr->child(4)->child(8), i);
                      break;
              case 2: upak_compile::dozator_data[stage_index[0]].et04_adr = static_cast<uint8_t>(item->text(1).toInt()); break;
              case 3: upak_compile::dozator_data[stage_index[0]].et04_channel = static_cast<uint8_t>(item->text(1).toInt()); break;
              case 5: i = item->text(1).toInt();
                      upak_compile::dozator_data[stage_index[0]].wp_num=static_cast<uint8_t>(i);
                      UpdateBranch(item, i);
                      break;
             }

             break;
     case 3:  //поменялись основные флаги дозатора (не скорости)
              switch(stage_index[2]){
               case 0: upak_compile::dozator_data[stage_index[0]].flags.error_load=item->text(1); break;
               case 1: upak_compile::dozator_data[stage_index[0]].flags.error_zero=item->text(1); break;
               case 2: upak_compile::dozator_data[stage_index[0]].flags.wait_weighter=item->text(1); break;
               case 3: upak_compile::dozator_data[stage_index[0]].flags.substr_flag=item->text(1); break;
               case 4: upak_compile::dozator_data[stage_index[0]].flags.DozAllow=item->text(1); break;
               case 5: upak_compile::dozator_data[stage_index[0]].flags.DozOpen=item->text(1); break;
               case 6: upak_compile::dozator_data[stage_index[0]].flags.DozEnd=item->text(1); break;
               case 7: upak_compile::dozator_data[stage_index[0]].flags.DozEmpty=item->text(1); break;
              }

             break;
     case 4:  // поменялись весовые точки
               switch(stage_index[3]){
                 case 0: upak_compile::dozator_data[stage_index[0]].wp[stage_index[2]].weight = item->text(1).toFloat(); break;
                 case 1: upak_compile::dozator_data[stage_index[0]].wp[stage_index[2]].gist = item->text(1).toFloat(); break;
                 case 2: upak_compile::dozator_data[stage_index[0]].wp[stage_index[2]].reg = item->text(1); break;
               }

             break;
     case 5: //поменялись флаги 1-2 скорости
               switch(stage_index[4]){
                 case 0: upak_compile::dozator_data[stage_index[0]].flags.speed_12[stage_index[3]][0] = item->text(1); break;
                 case 1: upak_compile::dozator_data[stage_index[0]].flags.speed_12[stage_index[3]][1] = item->text(1); break;
               }
             break;
     default:
        break;
    }

}




void MainWindow::CreateModulesTree(){
 QTreeWidgetItem *root, *item;
 SpinBoxItem *SpinBox;
 int adr=1;
 int regs=1140;   //сразу после весовых точек

 for(int type=0; type<3; type++){
    root=ui->treeModules->topLevelItem(type);
    if(root){
        for(int i=1; i<=8; i++){
            item = new QTreeWidgetItem();
            item->setText(0,QString::number(i));
            root->addChild(item);
            item->setHidden(true);
            item->setFlags(Qt::ItemIsEnabled);

            SpinBox = new SpinBoxItem(item, 1);
            SpinBox->setSingleStep(1);
            SpinBox->setMaximum(254);
            SpinBox->setMinimum(1);
            SpinBox->setValue(adr++);
            ui->treeModules->setItemWidget(item, 1, SpinBox);

            if(type!=1){
             SpinBox = new SpinBoxItem(item, 2);
             SpinBox->setSingleStep(8);
             if(type==0) SpinBox->setMaximum(4080);
                    else SpinBox->setMaximum(4072);
             SpinBox->setMinimum(0);
             ui->treeModules->setItemWidget(item, 2, SpinBox);
             SpinBox->setValue(regs);
            }

            if(type!=2){
                SpinBox = new SpinBoxItem(item, 3);
                SpinBox->setSingleStep(8);
                if(type==0) SpinBox->setMaximum(4088);
                       else SpinBox->setMaximum(4072);
                SpinBox->setMinimum(0);
                ui->treeModules->setItemWidget(item, 3, SpinBox);
                if(type==0) SpinBox->setValue(regs+16);
                       else SpinBox->setValue(regs);
            }

            regs+=24;
        }
    }
 }

 ui->treeModules->header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);

}


void MainWindow::on_treeModules_itemChanged(QTreeWidgetItem *item, int column)
{
    SpinBoxItem *SpinBox;
    int val, type;
    int in=0, out=0;

    this->content_changed_flag=true;

    if(column!=2 && column!=3) return;

    SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(item, column));
    if(!SpinBox) return;
    val = SpinBox->value();
    if(val%8){
       val-=val%8;
       SpinBox->setValue(val);
    }

    //определение типа модуля
    type=ui->treeModules->indexOfTopLevelItem(item->parent());

    switch(type){
      case 0:  //et-01
        if(column==2) {in=val; column++;} else {out=val; column--;}
        SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(item, column));
        if(!SpinBox) return;
        val = SpinBox->value();
        if(column==2) in=val; else out=val;
        item->setText(4, "Входы: " + QString::number(in) + "-" + QString::number(in+15) + "   Выходы: " + QString::number(out) + "-" + QString::number(out+7));
               break;
      case 1:  //et-05
        item->setText(4, "Выходы: " + QString::number(val) + "-" + QString::number(val+23));
               break;
      case 2:  //et-07
        item->setText(4, "Входы: " + QString::number(val) + "-" + QString::number(val+23));
               break;
       default: break;
    }


}


void MainWindow::on_spinBox_et07num_valueChanged(int val)
{
    QTreeWidgetItem *item;

    if(ui->treeModules){
        item = ui->treeModules->topLevelItem(2);
        if(item) UpdateBranch(item, val);
    }
}

void MainWindow::on_spinBox_et05num_valueChanged(int val)
{
    QTreeWidgetItem *item;

    if(ui->treeModules){
        item = ui->treeModules->topLevelItem(1);
        if(item) UpdateBranch(item, val);
    }
}


void MainWindow::on_spinBox_et01num_valueChanged(int val)
{
    QTreeWidgetItem *item;

    if(ui->treeModules){
        item = ui->treeModules->topLevelItem(0);
        if(item) UpdateBranch(item, val);
    }
}



void MainWindow::on_btn_SaveFile_released()
{
    if(OpenedFileName.isEmpty()){
        //диалог выбора файла для сохранения
        OpenedFileName = QFileDialog::getSaveFileName(this, "Сохранить как", QApplication::applicationDirPath(), "xml-файлы упак (*.xml)", nullptr, nullptr);
        if(!OpenedFileName.length()) return;
    }

    SaveFile();
}


bool MainWindow::SaveFile(bool for_et){
  QMessageBox msgBox;
  QTableWidgetItem *table_item;
  QTreeWidgetItem *top, *doz, *item;
  int tmp;
  QXmlStreamWriter *stream;
  QFile file;

  if(!for_et){
      //открываю файл c сохраненным ранее именем
      file.setFileName(OpenedFileName);

      if(!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        msgBox.setText("Не получается открыть файл " + OpenedFileName + "  на запись!");
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
        return false;
      }

      stream = new QXmlStreamWriter(&file);
      stream->setAutoFormatting(true);

  }else{
    buffer.clear();
    stream = new QXmlStreamWriter(&buffer);
    stream->setAutoFormatting(false);
  }


    stream->writeStartDocument();

    stream->writeStartElement("UPAK");


    //сохраняю текст программы
    QStringList list = ui->plainTextEdit_ProgramText->toPlainText().split("\n", QString::KeepEmptyParts);

    stream->writeStartElement("program_text");
     tmp = list.count();
     stream->writeAttribute("lines", QString::number(tmp));
     for(int i=0; i<tmp; i++) stream->writeTextElement(for_et?"l":"line", list[i]);
    stream->writeEndElement(); // program_text


    //сохраняю таймеры, 100мсек таймеры, счетчики
    stream->writeStartElement("timers");
    stream->writeAttribute("count", QString::number(40));
    for(int i=0; i<40; i++){
        stream->writeStartElement(for_et?"t":"timer");

        table_item = ui->tableTimers->item(i,0);
        if(table_item) stream->writeTextElement(for_et?"i":"in", table_item->text());
                  else stream->writeTextElement(for_et?"i":"in", "");

        table_item = ui->tableTimers->item(i,1);
        if(table_item) stream->writeTextElement(for_et?"o":"out", table_item->text());
                  else stream->writeTextElement(for_et?"o":"out", "");

        table_item = ui->tableTimers->item(i,2);
        if(table_item) stream->writeTextElement(for_et?"v":"value", table_item->text());
                  else stream->writeTextElement(for_et?"v":"value", "");

        table_item = ui->tableTimers->item(i,3);
        if(table_item) stream->writeTextElement(for_et?"c":"comment", table_item->text());
                  else stream->writeTextElement(for_et?"c":"comment", "");

        stream->writeEndElement(); // timer
    }
    stream->writeEndElement(); // timers


    stream->writeStartElement("timers_100msec");
    stream->writeAttribute("count", QString::number(10));
    for(int i=0; i<10; i++){
        stream->writeStartElement(for_et?"t":"timer");

        table_item = ui->table100msecTimers->item(i,0);
        if(table_item) stream->writeTextElement(for_et?"i":"in", table_item->text());
                  else stream->writeTextElement(for_et?"i":"in", "");

        table_item = ui->table100msecTimers->item(i,1);
        if(table_item) stream->writeTextElement(for_et?"o":"out", table_item->text());
                  else stream->writeTextElement(for_et?"o":"out", "");

        table_item = ui->table100msecTimers->item(i,2);
        if(table_item) stream->writeTextElement(for_et?"v":"value", table_item->text());
                  else stream->writeTextElement(for_et?"v":"value", "");

        table_item = ui->table100msecTimers->item(i,3);
        if(table_item) stream->writeTextElement(for_et?"c":"comment", table_item->text());
                  else stream->writeTextElement(for_et?"c":"comment", "");

        stream->writeEndElement(); // timer
    }
    stream->writeEndElement(); // timers_100msec


    stream->writeStartElement("counters");
    stream->writeAttribute("count", QString::number(20));
    for(int i=0; i<20; i++){
        stream->writeStartElement(for_et?"c":"counter");

        table_item = ui->tableCounters->item(i,0);
        if(table_item) stream->writeTextElement(for_et?"c":"clock", table_item->text());
                  else stream->writeTextElement(for_et?"c":"clock", "");

        table_item = ui->tableCounters->item(i,1);
        if(table_item) stream->writeTextElement(for_et?"r":"reset", table_item->text());
                  else stream->writeTextElement(for_et?"r":"reset", "");

        table_item = ui->tableCounters->item(i,2);
        if(table_item) stream->writeTextElement(for_et?"o":"out", table_item->text());
                  else stream->writeTextElement(for_et?"o":"out", "");

        table_item = ui->tableCounters->item(i,3);
        if(table_item) stream->writeTextElement(for_et?"v":"value", table_item->text());
                  else stream->writeTextElement(for_et?"v":"value", "");

        table_item = ui->tableCounters->item(i,4);
        if(table_item) stream->writeTextElement(for_et?"c":"comment", table_item->text());
                  else stream->writeTextElement(for_et?"c":"comment", "");

        stream->writeEndElement(); // counter
    }
    stream->writeEndElement(); // counters



    //сохраняю дерево дозаторов

    stream->writeStartElement("dosers");

    top=ui->treeDozator->topLevelItem(0);
    stream->writeAttribute("count", top->text(1));

       for(int doz_num=0; doz_num<10; doz_num++){
           doz=top->child(doz_num);
           stream->writeStartElement(for_et?"d":"doser");

             item = doz->child(0);
             stream->writeTextElement(for_et?"d":"dozator_type",  item->text(1));

             item = doz->child(1);
             stream->writeTextElement(for_et?"N":"NOfComp",  item->text(1));

             item = doz->child(2);
             stream->writeTextElement(for_et?"a":"et04_adr",  item->text(1));

             item = doz->child(3);
             stream->writeTextElement(for_et?"c":"et04_channel",  item->text(1));

             stream->writeStartElement("flags");

                 item=doz->child(4)->child(0);
                 stream->writeTextElement(for_et?"l":"flag_error_load",  item->text(1));

                 item=doz->child(4)->child(1);
                 stream->writeTextElement(for_et?"z":"error_zero",  item->text(1));

                 item=doz->child(4)->child(2);
                 stream->writeTextElement(for_et?"w":"wait_weighter",  item->text(1));

                 item=doz->child(4)->child(3);
                 stream->writeTextElement(for_et?"s":"substr_flag",  item->text(1));

                 item=doz->child(4)->child(4);
                 stream->writeTextElement(for_et?"a":"DozAllow",  item->text(1));

                 item=doz->child(4)->child(5);
                 stream->writeTextElement(for_et?"o":"DozOpen",  item->text(1));

                 item=doz->child(4)->child(6);
                 stream->writeTextElement(for_et?"e":"DozEnd",  item->text(1));

                 item=doz->child(4)->child(7);
                 stream->writeTextElement(for_et?"e":"DozEmpty",  item->text(1));


                 stream->writeStartElement(for_et?"s":"speed_12");
                     for(int comp=0; comp<16; comp++){
                         stream->writeStartElement(for_et?"c":"component");
                              item=doz->child(4)->child(8)->child(comp)->child(0);
                              stream->writeTextElement(for_et?"f":"speed_1",  item->text(1));

                              item=doz->child(4)->child(8)->child(comp)->child(1);
                              stream->writeTextElement(for_et?"s":"speed_2",  item->text(1));
                         stream->writeEndElement(); // component
                     }
                 stream->writeEndElement(); // speed_12

             stream->writeEndElement(); // flags

             stream->writeStartElement("wps");
             item = doz->child(5);
             stream->writeAttribute("count",  item->text(1));

             for(int wp_num=0; wp_num<4; wp_num++){
                 stream->writeStartElement(for_et?"w":"wp");
                     item=doz->child(5)->child(wp_num)->child(0);
                     stream->writeTextElement(for_et?"w":"weight",  item->text(1));

                     item=doz->child(5)->child(wp_num)->child(1);
                     stream->writeTextElement(for_et?"g":"gist",  item->text(1));

                     item=doz->child(5)->child(wp_num)->child(2);
                     stream->writeTextElement(for_et?"r":"reg",  item->text(1));
                 stream->writeEndElement(); // wp
             }

             stream->writeEndElement(); // wps


           stream->writeEndElement(); // doser
       }

    stream->writeEndElement(); // dosers



    //сохраняю модули

    stream->writeStartElement("modules");
        stream->writeStartElement("modules_et01");
          stream->writeAttribute("count", QString::number(ui->spinBox_et01num->value()) );
          top=ui->treeModules->topLevelItem(0);
          for(int i=0; i<8; i++){
              stream->writeStartElement(for_et?"m":"module");
                 item=top->child(i);
                 stream->writeTextElement(for_et?"a":"modbus_adr",  item->text(1));
                 stream->writeTextElement(for_et?"i":"reg_inputs",  item->text(2));
                 stream->writeTextElement(for_et?"o":"reg_outputs",  item->text(3));
              stream->writeEndElement(); // module
          }
        stream->writeEndElement(); // modules_et01

        stream->writeStartElement("modules_et05");
          stream->writeAttribute("count", QString::number(ui->spinBox_et05num->value()) );
          top=ui->treeModules->topLevelItem(1);
          for(int i=0; i<8; i++){
              stream->writeStartElement(for_et?"m":"module");
                 item=top->child(i);
                 stream->writeTextElement(for_et?"a":"modbus_adr",  item->text(1));
                 stream->writeTextElement(for_et?"o":"reg_outputs",  item->text(3));
              stream->writeEndElement(); // module
          }
        stream->writeEndElement(); // modules_et05

        stream->writeStartElement("modules_et07");
          stream->writeAttribute("count", QString::number(ui->spinBox_et07num->value()) );
          top=ui->treeModules->topLevelItem(2);
          for(int i=0; i<8; i++){
              stream->writeStartElement(for_et?"m":"module");
                 item=top->child(i);
                 stream->writeTextElement(for_et?"a":"modbus_adr",  item->text(1));
                 stream->writeTextElement(for_et?"i":"reg_inputs",  item->text(2));
              stream->writeEndElement(); // module
          }
        stream->writeEndElement(); // modules_et07

    stream->writeEndElement(); // modules


    stream->writeEndElement(); // UPAK

    stream->writeEndDocument();


    if(!for_et) file.close();

    delete stream;

    return true;
}



void MainWindow::on_btn_OpenFile_released()
{
   //диалог выбора файла
  OpenedFileName = QFileDialog::getOpenFileName(this, "Открыть", QApplication::applicationDirPath(), "xml-файлы упак (*.xml)", nullptr, nullptr);
  if(!OpenedFileName.length()) return;

  if(!OpenFile()){
      OpenedFileName.clear();
      CleanAllWighets();
  }
  return;
}


bool MainWindow::OpenFile(bool from_et){
  QMessageBox msgBox;
  bool ok;
  QFile file;
  QXmlStreamReader *xml = nullptr;

  if(!from_et){

      //открываю файл c сохраненным ранее именем
      file.setFileName(OpenedFileName);

      if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
          msgBox.setText("Не получается открыть файл " + OpenedFileName + " !");
          msgBox.setIcon(QMessageBox::Critical);
          msgBox.exec();
          return false;
      }

      xml = new QXmlStreamReader(&file);
  }else{
      xml = new QXmlStreamReader(buffer);
      OpenedFileName="";
  }

  CleanAllWighets();

  ok=open_upak_xml(*xml, from_et);
  if(!ok){
      CleanAllWighets();
      msgBox.setText("Ошибка при разборе данных файла " + OpenedFileName + " !");
      msgBox.setIcon(QMessageBox::Critical);
      msgBox.exec();
  }

  if(!from_et) file.close();

  if(xml)
      delete xml;

  return ok;

}



void MainWindow::CleanAllWighets(){
    //заполнить все поля в формах значениями по-умолчанию
   ui->plainTextEdit_ProgramText->clear();

   upak_compile::ResetDozators();
   UpdateDozatorTree();


   ui->tableTimers->clearContents();
   ui->table100msecTimers->clearContents();
   ui->tableCounters->clearContents();


   ui->spinBox_et01num->setValue(0);
   ui->spinBox_et05num->setValue(0);
   ui->spinBox_et07num->setValue(0);

}


bool MainWindow::read_xml_text(QXmlStreamReader& xml, QString* str){
  xml.readNextStartElement();
  if(xml.hasError()) return false;

  *str = xml.readElementText(QXmlStreamReader::ErrorOnUnexpectedElement);
  if(xml.hasError()) return false;

  return true;
}


int MainWindow::get_xml_attribute(QXmlStreamReader& xml, QString attribute){
    QString str;
    int res =-1;
    bool ok;

    str = xml.attributes().value(attribute).toString();
    if(str.isEmpty()) return -1;
    res = str.toInt(&ok);
    if(!ok || res<0) return -1;
    return res;
}


bool MainWindow::find_start_element(QXmlStreamReader& xml){
    bool res=false;

    while(!xml.atEnd() && !res) res=xml.readNextStartElement();
    return res;
}


bool MainWindow::open_upak_xml(QXmlStreamReader& xml, bool from_et){
 QTableWidgetItem *table_item;
 SpinBoxItem *SpinBox;
 QString str;
 int count, value;
 float value_f;
 bool ok;

     if(!xml.readNextStartElement()) return false;
     if(xml.name() != "UPAK") return false;

         //чтение текста программы
     xml.readNextStartElement();
     if(xml.name()!="program_text") return false;
     count=get_xml_attribute(xml, "lines");
     if(count<0) return false;
     for(int i=0; i<count; i++){
        if(!read_xml_text(xml, &str)) return false;
        ui->plainTextEdit_ProgramText->appendPlainText(str);
     }

     //чтение таймеров

    if(!find_start_element(xml)) return false;
    if(xml.name()!="timers") return false;

    count = get_xml_attribute(xml, "count");
    if(count<0) return false;
    for(int i=0; i<count; i++){
        if(!find_start_element(xml)) return false;
        for(int j=0; j<4; j++){
            if(!read_xml_text(xml, &str)) return false;
            table_item = ui->tableTimers->item(i,j);
            if(table_item) table_item->setText(str);
            else{
                table_item = new QTableWidgetItem(str);
                ui->tableTimers->setItem(i,j, table_item);
            }
        }
    }


    //чтение 100мсек таймеров

   if(!find_start_element(xml)) return false;
   if(xml.name()!="timers_100msec") return false;

   count = get_xml_attribute(xml, "count");
   if(count<0) return false;
   for(int i=0; i<count; i++){
       if(!find_start_element(xml)) return false;
       for(int j=0; j<4; j++){
           if(!read_xml_text(xml, &str)) return false;
           table_item = ui->table100msecTimers->item(i,j);
           if(table_item) table_item->setText(str);
           else{
               table_item = new QTableWidgetItem(str);
               ui->table100msecTimers->setItem(i,j, table_item);
           }
       }
   }

   //чтение счетчиков

  if(!find_start_element(xml)) return false;
  if(xml.name()!="counters") return false;

  count = get_xml_attribute(xml, "count");
  if(count<0) return false;
  for(int i=0; i<count; i++){
      if(!find_start_element(xml)) return false;
      for(int j=0; j<5; j++){
          if(!read_xml_text(xml, &str)) return false;
          table_item = ui->tableCounters->item(i,j);
          if(table_item) table_item->setText(str);
          else{
              table_item = new QTableWidgetItem(str);
              ui->tableCounters->setItem(i,j, table_item);
          }
      }
  }


  //чтение дерева дозатора

  if(!find_start_element(xml)) return false;
  if(xml.name()!="dosers") return false;

  count = get_xml_attribute(xml, "count");
  if(count<0 || count>10) return false;
  upak_compile::NOfDoz=count;

  for(int i=0; i<10; i++){
      if(!find_start_element(xml)) return false;
      if(xml.name()!=(from_et?"d":"doser")) return false;

      if(!read_xml_text(xml, &str)) return false;
      if(!str.length()) return false;
      value=str.toInt(&ok);
      if(!ok) return false;
      if(value<0 || value>3) return false;
      upak_compile::dozator_data[i].dozator_type=static_cast<uint8_t>(value);

      if(!read_xml_text(xml, &str)) return false;
      if(!str.length()) return false;
      value=str.toInt(&ok);
      if(!ok) return false;
      if(value<0 || value>15) return false;
      upak_compile::dozator_data[i].NOfComp=static_cast<uint8_t>(value+1);

      if(!read_xml_text(xml, &str)) return false;
      if(!str.length()) return false;
      value=str.toInt(&ok);
      if(!ok) return false;
      if(value<1 || value>254) return false;
      upak_compile::dozator_data[i].et04_adr=static_cast<uint8_t>(value);

      if(!read_xml_text(xml, &str)) return false;
      if(!str.length()) return false;
      value=str.toInt(&ok);
      if(!ok) return false;
      if(value<0 || value>63) return false;
      upak_compile::dozator_data[i].et04_channel=static_cast<uint8_t>(value);

      //флаги
      if(!find_start_element(xml)) return false;
      if(xml.name()!="flags") return false;

      if(!read_xml_text(xml, &str)) return false;
      upak_compile::dozator_data[i].flags.error_load=str;

      if(!read_xml_text(xml, &str)) return false;
      upak_compile::dozator_data[i].flags.error_zero=str;

      if(!read_xml_text(xml, &str)) return false;
      upak_compile::dozator_data[i].flags.wait_weighter=str;

      if(!read_xml_text(xml, &str)) return false;
      upak_compile::dozator_data[i].flags.substr_flag=str;

      if(!read_xml_text(xml, &str)) return false;
      upak_compile::dozator_data[i].flags.DozAllow=str;

      if(!read_xml_text(xml, &str)) return false;
      upak_compile::dozator_data[i].flags.DozOpen=str;

      if(!read_xml_text(xml, &str)) return false;
      upak_compile::dozator_data[i].flags.DozEnd=str;

      if(!read_xml_text(xml, &str)) return false;
      upak_compile::dozator_data[i].flags.DozEmpty=str;

      if(!find_start_element(xml)) return false;
      if(xml.name()!=(from_et?"s":"speed_12")) return false;

      for(int j=0; j<16; j++){
          if(!find_start_element(xml)) return false;
          if(xml.name()!=(from_et?"c":"component")) return false;

          if(!read_xml_text(xml, &str)) return false;
          upak_compile::dozator_data[i].flags.speed_12[j][0]=str;

          if(!read_xml_text(xml, &str)) return false;
          upak_compile::dozator_data[i].flags.speed_12[j][1]=str;
      }


      //весовые точки
      if(!find_start_element(xml)) return false;
      if(xml.name()!="wps") return false;
      count = get_xml_attribute(xml, "count");
      if(count<0 || count>4) return false;
      upak_compile::dozator_data[i].wp_num=static_cast<uint8_t>(count);

      for(int j=0; j<4; j++){
          if(!find_start_element(xml)) return false;
          if(xml.name()!=(from_et?"w":"wp")) return false;

          if(!read_xml_text(xml, &str)) return false;
          if(!str.length()) return false;
          value_f=str.toFloat(&ok);
          if(!ok) return false;
          if(value_f<WP_MIN_WEIGHT || value>WP_MAX_WEIGHT) return false;
          upak_compile::dozator_data[i].wp[j].weight=value_f;

          if(!read_xml_text(xml, &str)) return false;
          if(!str.length()) return false;
          value_f=str.toFloat(&ok);
          if(!ok) return false;
          if(value_f<WP_MIN_GIST || value>WP_MAX_GIST) return false;
          upak_compile::dozator_data[i].wp[j].gist=value_f;

          if(!read_xml_text(xml, &str)) return false;
          upak_compile::dozator_data[i].wp[j].reg=str;
      }

  }

  UpdateDozatorTree();


  //чтение дерева модулей

  if(!find_start_element(xml)) return false;
  if(xml.name()!="modules") return false;


  if(!find_start_element(xml)) return false;
  if(xml.name()!="modules_et01") return false;

  count = get_xml_attribute(xml, "count");
  if(count<0 || count>8) return false;
  ui->spinBox_et01num->setValue(count);

  for(int i=0; i<8; i++){
      if(!find_start_element(xml)) return false;
      if(xml.name()!=(from_et?"m":"module")) return false;

      if(!read_xml_text(xml, &str)) return false;
      if(!str.length()) return false;
      value=str.toInt(&ok);
      if(!ok) return false;
      if(value<1 || value>254) return false;
      SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(0)->child(i), 1));
      SpinBox->setValue(value);

      if(!read_xml_text(xml, &str)) return false;
      if(!str.length()) return false;
      value=str.toInt(&ok);
      if(!ok) return false;
      if(value<0 || value>4080) return false;
      SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(0)->child(i), 2));
      SpinBox->setValue(value);

      if(!read_xml_text(xml, &str)) return false;
      if(!str.length()) return false;
      value=str.toInt(&ok);
      if(!ok) return false;
      if(value<0 || value>4088) return false;
      SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(0)->child(i), 3));
      SpinBox->setValue(value);
  }



  if(!find_start_element(xml)) return false;
  if(xml.name()!="modules_et05") return false;

  count = get_xml_attribute(xml, "count");
  if(count<0 || count>8) return false;
  ui->spinBox_et05num->setValue(count);

  for(int i=0; i<8; i++){
      if(!find_start_element(xml)) return false;
      if(xml.name()!=(from_et?"m":"module")) return false;

      if(!read_xml_text(xml, &str)) return false;
      if(!str.length()) return false;
      value=str.toInt(&ok);
      if(!ok) return false;
      if(value<1 || value>254) return false;
      SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(1)->child(i), 1));
      SpinBox->setValue(value);

      if(!read_xml_text(xml, &str)) return false;
      if(!str.length()) return false;
      value=str.toInt(&ok);
      if(!ok) return false;
      if(value<0 || value>4072) return false;
      SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(1)->child(i), 3));
      SpinBox->setValue(value);
  }



  if(!find_start_element(xml)) return false;
  if(xml.name()!="modules_et07") return false;

  count = get_xml_attribute(xml, "count");
  if(count<0 || count>8) return false;
  ui->spinBox_et07num->setValue(count);

  for(int i=0; i<8; i++){
      if(!find_start_element(xml)) return false;
      if(xml.name()!=(from_et?"m":"module")) return false;

      if(!read_xml_text(xml, &str)) return false;
      if(!str.length()) return false;
      value=str.toInt(&ok);
      if(!ok) return false;
      if(value<1 || value>254) return false;
      SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(2)->child(i), 1));
      SpinBox->setValue(value);

      if(!read_xml_text(xml, &str)) return false;
      if(!str.length()) return false;
      value=str.toInt(&ok);
      if(!ok) return false;
      if(value<0 || value>4072) return false;
      SpinBox = reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(2)->child(i), 2));
      SpinBox->setValue(value);
  }


  return true;
}






void MainWindow::action_11_activated()
{
    auto s = settings();
    DialogComSettings dialog(this, s);
    dialog.exec();
    if(dialog.IsOK()){
        //сохраняю новые настройки
        settings.set(s);
        GetComSettings(s);
        //переоткрыть порт с новыми настройками
        this->OpenCOM();
    }

}


void MainWindow::OpenCOM(){
    this->CloseCOM();
    if(port.Open(this->COM_num, this->baudrate))
         COM_status_label->setText("COM"+QString::number(this->COM_num)+" успешно открыт на скорости "+QString::number(this->baudrate));
    else COM_status_label->setText("Нет возможности открыть COM"+QString::number(this->COM_num)+" !!!");
}

void MainWindow::CloseCOM(){
    if(port.IsOpened()) port.Close();
    COM_status_label->setText("COM-порт закрыт");
}


void MainWindow::action_6_activated()
{
    this->OpenCOM();
}

void MainWindow::action_7_activated()
{
    this->CloseCOM();
}

void MainWindow::on_btn_ReleaseCOM_released()
{
    this->CloseCOM();
}

void MainWindow::on_btn_OpenCOM_clicked()
{
    this->OpenCOM();
}


void MainWindow::modbus_reply(message_data_t data, MSG_STATUS status){

    switch(data.label){
      case ET_CHECK:   //пинг
                 delete data.data_buf;
                 if(status==STATUS_OK) this->WriteStatus("Успешно!");
                                 else  this->WriteStatus("Ошибка!");
                 break;

      case PROGRAM_WRITE:    //запись программы упака
                 if(status==STATUS_OK) WriteUPAK(false, true);
                                  else WriteUPAK(false, false);
                 break;

      case GET_REGS:
                 poll_regs_received(status==STATUS_OK);

                 if(status==STATUS_OK){
                     memcpy(REGS, data.data_buf, sizeof(REGS));
                     RedrawRegs();

                     if(!poll_is_active())
                         this->WriteStatus("Успешно!");
                     else
                         highlighter->rehighlight();

                 }else{

                     if(!poll_is_active())
                         this->WriteStatus("Ошибка!");
                     else
                         highlighter->rehighlight();

                 }

                 if(data.RegCount>1) delete[] data.data_buf;
                                else delete data.data_buf;
                 break;

      case WRITE_PROGRAM_TEXT:
                  if(status==STATUS_OK) WriteUPAKtext(false, true);
                                  else  WriteUPAKtext(false, false);
                  break;

      case READ_PROGRAM_TEXT:
                if(status==STATUS_OK) LoadUPAKtext(false, true);
                                else  LoadUPAKtext(false, false);
                break;

    case PROGRAM_ERASE:

                break;
      default:
                 break;
    }


}


void MainWindow::on_btn_Check_released()
{
    ushort *data;
    message_data_t msg;
    bool res;

    if(!port.IsOpened()){
        QMessageBox msgBox;
        msgBox.setText("COM-порт закрыт !!!");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        return;
    }

    data = new ushort;

    msg.label = ET_CHECK;
    msg.adr=static_cast<int8_t>(this->NetAdr);
    msg.data_buf=data;
    msg.msg_type = MSG_TYPE_READ;
    msg.StartReg = MODBUS_REG_PING;
    msg.RegCount = 1;
    msg.msg_priority = PRIORITY_NORMAL;

    res=port.message(msg);
    if(!res){
        QMessageBox msgBox;
        msgBox.setText("Очередь обмена через COM-порт полностью заполнена! Попробуйте послать сообщение через несколько секунд!");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        return;
    }
}


void MainWindow::on_btn_WriteProgram_released()
{

    if(content_changed_flag){
        QMessageBox msgBox;
        msgBox.setText("Программа УПАК была изменена! Перед записью в прибор необходима компиляция!");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        return;
    }

    WriteUPAK(true, false);
}


void MainWindow::WriteUPAK(bool start, bool status){
  message_data_t msg;
  static uint16_t modbus_reg;
  bool res;
  static bool size_msg_flag;

    if(start){  //первый вызов - посылаем размер упака
       ui->btn_Compile->setEnabled(false);
       StartProgressBar();

       modbus_reg=static_cast<uint16_t>(this->upak_bytes);

       msg.label=PROGRAM_WRITE;
       msg.adr = static_cast<int8_t>(this->NetAdr);
       msg.msg_type=MSG_TYPE_WRITE;
       msg.StartReg=MODBUS_REG_UPAK_SIZE;
       msg.RegCount=1;
       msg.data_buf = &modbus_reg;
       msg.msg_priority=PRIORITY_HIGH;

       size_msg_flag=true;

       res=port.message(msg);
       if(!res){
          QMessageBox msgBox;
          msgBox.setText("Очередь обмена через COM-порт полностью заполнена! Попробуйте повторить запись программы через несколько секунд!");
          msgBox.setIcon(QMessageBox::Warning);
          msgBox.exec();
         ui->btn_Compile->setEnabled(true);
         StopProgressBar();
         return;
       }

    }else{  //последующие вызовы - посылаем саму программу
        if(!status){
           QMessageBox msgBox;
           msgBox.setText("Ошибка при записи упака!");
           msgBox.setIcon(QMessageBox::Critical);
           msgBox.exec();
           ui->btn_Compile->setEnabled(true);
           StopProgressBar();
          return;
        }

        if(size_msg_flag){ //отправляю теперь саму программу
            size_msg_flag=false;

            msg.label=PROGRAM_WRITE;
            msg.adr = static_cast<int8_t>(this->NetAdr);
            msg.msg_type=MSG_TYPE_WRITE;
            msg.StartReg=MODBUS_REG_UPAK;
            msg.RegCount=static_cast<uint16_t>(this->upak_bytes/2);
            msg.data_buf = reinterpret_cast<uint16_t*>(this->upak_mas);
            msg.msg_priority=PRIORITY_HIGH;

            res=port.message(msg);
            if(!res){
               QMessageBox msgBox;
               msgBox.setText("Ошибка! Очередь обмена через COM-порт полностью заполнена! Попробуйте повторить запись программы через несколько секунд!");
               msgBox.setIcon(QMessageBox::Critical);
               msgBox.exec();
              ui->btn_Compile->setEnabled(true);
              StopProgressBar();
              return;
            }

        }else{ //программа уже успешно отправлена, сообщаем об этом в статусе
          StopProgressBar();
          this->WriteStatus("Программа успешно записана! Ожидание начала передачи текста программы...");
           //запускаю таймер, по срабатывании которого запустится запись текста порграммы в прибор
          QTimer::singleShot(3000, this, SLOT(WriteUPAKtext()));
        }

    }
}

void MainWindow::EraseUPAK()
{
    message_data_t msg;
    static uint16_t modbus_reg;

    modbus_reg=0;

    msg.label=PROGRAM_ERASE;
    msg.adr = static_cast<int8_t>(this->NetAdr);
    msg.msg_type=MSG_TYPE_WRITE;
    msg.StartReg=MODBUS_REG_UPAK_SIZE;
    msg.RegCount=1;
    msg.data_buf = &modbus_reg;
    msg.msg_priority=PRIORITY_HIGH;

    if(!port.message(msg)){
        QMessageBox msgBox;
        msgBox.setText("Очередь обмена через COM-порт полностью заполнена! Попробуйте повторить запись программы через несколько секунд!");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
    }
}


void MainWindow::WriteUPAKtext(bool start, bool status){
 message_data_t msg;
 static uint16_t modbus_reg, check_number, *mas=nullptr;
 bool res;
 static int state=0;
 int count;

 if(start) state=0;
 else{
     if(status) state++;
     else{
         QMessageBox msgBox;
         msgBox.setText("Ошибка при записи текста упака!");
         msgBox.setIcon(QMessageBox::Critical);
         msgBox.exec();
         ui->btn_Compile->setEnabled(true);
         StopProgressBar();
         state=0;
         if(mas) delete[] mas;
         mas = nullptr;
        return;
     }
 }


 switch(state){
 case 0:
     //посылаем регистр-признак начала отправки файла
     ui->btn_Compile->setEnabled(false);

     modbus_reg=0;

     msg.label=WRITE_PROGRAM_TEXT;
     msg.adr = static_cast<int8_t>(this->NetAdr);
     msg.msg_type=MSG_TYPE_WRITE;
     msg.StartReg=FILE_START_REG;
     msg.RegCount=1;
     msg.data_buf = &modbus_reg;
     msg.msg_priority=PRIORITY_HIGH;
     break;

 case 1:
     //посылаем данные файла

     //сохраняю программу упак в буфер
     SaveFile(true);

     count=(buffer.size()+1)/2+2;
     mas = new uint16_t[static_cast<unsigned>(count)];
     if(!mas){
         QMessageBox msgBox;
         msgBox.setText("Внутренняя ошибка программы! Ошибка выделения памяти!");
         msgBox.setIcon(QMessageBox::Warning);
         msgBox.exec();
        ui->btn_Compile->setEnabled(true);
        StopProgressBar();
        state=0;
        return;
     }
     memcpy(&mas[1], buffer.constData(), static_cast<size_t>(buffer.size()));

     //добавляю длину данных и контрольную сумму
     mas[0]= static_cast<uint16_t>(buffer.size());
     ModbusThread::fast_crc16(reinterpret_cast<uint8_t*>(&mas[count-1]), reinterpret_cast<uint8_t*>(mas), static_cast<unsigned>((count-1)*2));

     //считаю проверочное число
     check_number=0;
     for(int i=0; i<count; i++) check_number+=mas[i]+0x0901;

     //отправляю сообщение
     msg.label=WRITE_PROGRAM_TEXT;
     msg.adr = static_cast<int8_t>(this->NetAdr);
     msg.msg_type=MSG_TYPE_WRITE;
     msg.StartReg=FILE_REG;
     msg.RegCount=static_cast<uint16_t>(count);
     msg.data_buf = mas;
     msg.msg_priority=PRIORITY_HIGH;

     StartProgressBar();
     break;

 case 2:
     //посылаем регистр-признак завершения передачи файла
     delete[] mas;
     mas = nullptr;

     msg.label=WRITE_PROGRAM_TEXT;
     msg.adr = static_cast<int8_t>(this->NetAdr);
     msg.msg_type=MSG_TYPE_WRITE;
     msg.StartReg=FILE_END_REG;
     msg.RegCount=1;
     msg.data_buf = &check_number;
     msg.msg_priority=PRIORITY_HIGH;

     break;

 case 3:
     //запрос скользящего проверочного числа
     msg.label=WRITE_PROGRAM_TEXT;
     msg.adr = static_cast<int8_t>(this->NetAdr);
     msg.msg_type=MSG_TYPE_READ;
     msg.StartReg=FILE_CHECK_REG;
     msg.RegCount=1;
     msg.data_buf = &modbus_reg;
     msg.msg_priority=PRIORITY_HIGH;
     break;

 case 4:
     //сравнение скользящего проверочного числа
     if(modbus_reg!=check_number){
         QMessageBox msgBox;
         msgBox.setText("Ошибка передачи текста программы! Несовпадение контрольной суммы переданных данных!");
         msgBox.setIcon(QMessageBox::Warning);
         msgBox.exec();
     }else{
         //файл передан
         state=0;
         this->WriteStatus("Текст программы успешно записан!");
     }

     ui->btn_Compile->setEnabled(true);
     StopProgressBar();
     return;
 }

 res=port.message(msg);
 if(!res){
    QMessageBox msgBox;
    msgBox.setText("Очередь обмена через COM-порт полностью заполнена! Попробуйте повторить запись программы через несколько секунд!");
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.exec();
   ui->btn_Compile->setEnabled(true);
   StopProgressBar();
   state=0;
   if(mas) delete[] mas;
   mas = nullptr;
 }

 return;

}



void MainWindow::LoadUPAKtext(bool start, bool status){
 static int state=0;
 message_data_t msg;
 static uint16_t modbus_reg, *mas=nullptr;
 bool res;
 int count;

 if(start) state=0;
 else{
     if(status) state++;
     else{
         QMessageBox msgBox;
         msgBox.setText("Ошибка при считывании текста упака!");
         msgBox.setIcon(QMessageBox::Critical);
         msgBox.exec();
         ui->btn_GetProgramText->setEnabled(true);
         StopProgressBar();
         state=0;
         if(mas) delete[] mas;
         mas = nullptr;
        return;
     }
 }


 switch(state){
 case 0:
     //считываем первые два байта с длиной хранящегося файла
     ui->btn_GetProgramText->setEnabled(false);

     msg.label=READ_PROGRAM_TEXT;
     msg.adr = static_cast<int8_t>(this->NetAdr);
     msg.msg_type=MSG_TYPE_READ;
     msg.StartReg=FILE_REG;
     msg.RegCount=1;
     msg.data_buf = &modbus_reg;
     msg.msg_priority=PRIORITY_HIGH;
     break;

 case 1:
     //принимаем все регистры файла + снова длину(нужна для расчета crc) и 1 регистр контрольной суммы
     //создаю буфер для принимаемых данных
     count = (modbus_reg+1)/2+2;    //(если в файле не четное кол-во байт, то выравниваем до 2х байт в большую сторону добавлением одного незначащего байта)
     mas = new uint16_t[static_cast<unsigned>(count)];
     if(!mas){
         QMessageBox msgBox;
         msgBox.setText("Внутрення ошибка программы! Ошибка выделения памяти!");
         msgBox.setIcon(QMessageBox::Critical);
         msgBox.exec();
        ui->btn_GetProgramText->setEnabled(true);
        StopProgressBar();
        state=0;
        return;
     }

     msg.label=READ_PROGRAM_TEXT;
     msg.adr = static_cast<int8_t>(this->NetAdr);
     msg.msg_type=MSG_TYPE_READ;
     msg.StartReg=FILE_REG;
     msg.RegCount=static_cast<uint16_t>(count);
     msg.data_buf = mas;
     msg.msg_priority=PRIORITY_HIGH;

     StartProgressBar();
     break;

 case 2:
     //проверяем целостность полученных данных, считываем программу
     StopProgressBar();

     count=mas[0];
     if(count&1) count++;

     if(!ModbusThread::check_crc16(reinterpret_cast<uint8_t*>(mas), static_cast<unsigned>(count+2), reinterpret_cast<uint8_t*>(&mas[(count+2)/2]))){
         QMessageBox msgBox;
         msgBox.setText("Ошибка контрольной суммы прочитанных данных!");
         msgBox.setIcon(QMessageBox::Critical);
         msgBox.exec();
        ui->btn_GetProgramText->setEnabled(true);
        if(mas) delete[] mas;
        mas = nullptr;
        state=0;
        return;
     }

     buffer.clear();
     buffer.resize(mas[0]);
     memcpy(buffer.data(), &mas[1], mas[0]);

     delete[] mas;
     mas = nullptr;

     OpenFile(true);

     buffer.clear();

     ui->btn_GetProgramText->setEnabled(true);
     state=0;

     this->WriteStatus("Текст программы успешно считан!");

     return;
 }

 res=port.message(msg);
 if(!res){
    QMessageBox msgBox;
    msgBox.setText("Очередь обмена через COM-порт полностью заполнена! Попробуйте повторить чтение программы через несколько секунд!");
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.exec();
   ui->btn_GetProgramText->setEnabled(true);
   StopProgressBar();
   state=0;
   if(mas) delete[] mas;
   mas = nullptr;
 }


  return;
}



void MainWindow::on_btn_GetRegs_released()
{
    request_regs();
}


void MainWindow::RedrawRegs(){
  QListWidgetItem *item;
  int row=0;

  if(upak_compile::USED_REGS.count()!=ui->listWidget_Regs->count()) return;

  /*обновить значения значков в списке*/

  deny_flags_update_flag=true;

  for(int i=0; i<4096; i++){
      if(upak_compile::USED_REGS.contains(static_cast<uint16_t>(i))){
          item = ui->listWidget_Regs->item(row);
          if(item){
              if(GetReg(static_cast<unsigned>(i))){item->setCheckState(Qt::Checked);}
               else {item->setCheckState(Qt::Unchecked);}
              row++;
          }
      }

  }

  deny_flags_update_flag=false;
}


void MainWindow::RebuildRegList(){

    deny_flags_update_flag=true;

    ui->listWidget_Regs->clear();


    for(int i=0; i<4096; i++){
        if(upak_compile::USED_REGS.contains(static_cast<uint16_t>(i))){
            QListWidgetItem *item = new QListWidgetItem();
            item->setText(QString::number(i));
            item->setCheckState(Qt::Unchecked);
            ui->listWidget_Regs->addItem(item);
        }
    }

   deny_flags_update_flag=false;
}


void MainWindow::on_checkBox_Poll_stateChanged(__attribute__((unused)) int arg1)
{
    if(ui->checkBox_Poll->isChecked()){
        poll_timer_start();
        this->highlighter->SetDebugMode(true);
    }else{
        poll_timer_stop();
        this->highlighter->SetDebugMode(false);
        this->highlighter->rehighlight();
    }

}


void MainWindow::flags_changed(__attribute__((unused)) QListWidgetItem* item){

    if(deny_flags_update_flag) return;


}

//возвращает значение флажка
bool MainWindow::GetReg(uint reg){
    if(reg>4095) return false;
    if(REGS[reg>>4]&(0x0001<<(reg&0x000F))) return true; else return false;
}


//возвращает значение флажка
int MainWindow::GetReg(QStringRef reg_name){
    int reg = upak_compile::GetRegNum(reg_name);
    if(reg>=0 && reg<4096) return GetReg(static_cast<unsigned>(reg));
    else return -1;
}


void MainWindow::action_activated()
{
  this->on_btn_OpenFile_released();
}

void MainWindow::action_2_activated()
{
   this->on_btn_SaveFile_released();
}


void MainWindow::action_3_activated()
{
   //диалог выбора файла для сохранения
   OpenedFileName = QFileDialog::getSaveFileName(this, "Сохранить как", QApplication::applicationDirPath(), "xml-файлы упак (*.xml)", nullptr, nullptr);
   if(!OpenedFileName.length()) return;

   SaveFile();
}


void MainWindow::action_4_activated()
{
    QPalette p;

   //меняю цвет выделения в окне программы на яркий
    p = ui->plainTextEdit_ProgramText->palette();
    //p.setColor(QPalette::Highlight, QColor(/*what ever you want*/);
    p.setColor(QPalette::Highlight, QColor("red"));
    ui->plainTextEdit_ProgramText->setPalette(p);

    find_dialog.show();
    find_dialog.activateWindow();
}

void MainWindow::find_text_hide(){
    QPalette p;

    //возвращаю цвет выделения в окне программы на обычный
    p = ui->plainTextEdit_ProgramText->palette();
    p.setColor(QPalette::Highlight, QApplication::palette().color(QPalette::Highlight));
    ui->plainTextEdit_ProgramText->setPalette(p);

    return;
}

void MainWindow::find_text(){
  QTextCursor cursor;

   if(!ui->plainTextEdit_ProgramText->find(find_dialog.getFindText(), find_dialog.GetSearchParams())){
       cursor=ui->plainTextEdit_ProgramText->textCursor();
       if(find_dialog.GetSearchParams()&QTextDocument::FindBackward)
            cursor.movePosition(QTextCursor::End);
       else cursor.movePosition(QTextCursor::Start);
       ui->plainTextEdit_ProgramText->setTextCursor(cursor);

       if(!ui->plainTextEdit_ProgramText->find(find_dialog.getFindText(), find_dialog.GetSearchParams()))
           find_dialog.fault_msg();
   }

}

void MainWindow::on_plainTextEdit_ProgramText_cursorPositionChanged()
{
    QTextCursor cursor;
    int line_num;

    cursor = ui->plainTextEdit_ProgramText->textCursor();
    line_num = cursor.blockNumber();

    line_number_label->setText("Строка: "+QString::number(line_num+1)+ "  ");
}



void MainWindow::StartProgressBar(){
    ProgressBar->setValue(0);
    ProgressBar->setVisible(true);
}

void MainWindow::StopProgressBar(){
    ProgressBar->setVisible(false);
}

void MainWindow::poll_init()
{
    poll_timer = new QTimer(this);
    poll_timer->setSingleShot(false);
    poll_timer->setInterval(500);
    poll_regs_received_flag = false;
    connect(poll_timer, &QTimer::timeout, this, &MainWindow::poll_timer_callback);
}

void MainWindow::poll_timer_start()
{
    if(poll_timer->isActive())
        return;

    request_regs(true);
    poll_regs_received_flag = false;
    poll_timer->start();
}

void MainWindow::poll_timer_stop()
{
    poll_timer->stop();
}

void MainWindow::poll_regs_received(__attribute__((unused)) bool status_ok){
    poll_regs_received_flag = true;
}

void MainWindow::poll_timer_callback()
{
    if(!poll_regs_received_flag)
        return;

    if(request_regs(true))  //сбросить флаг только если успешно добавлено в очередь на отправку
        poll_regs_received_flag = false;
}

bool MainWindow::read_file(const QString &filename, QByteArray &data)
{
    data.clear();

    QFile file(filename);

    if(!file.open(QIODevice::ReadOnly))
        return false;

    data = file.readAll();

    file.close();

    return true;
}

QString MainWindow::get_string_from_1251(const QByteArray &arr)
{
    QTextCodec* codec = QTextCodec::codecForName("Windows-1251");
    return codec?codec->toUnicode(arr):QString();
}

bool MainWindow::get_upak_data_from_lp(const QString& str, MainWindow::upak_data_t& data)
{

    typedef upak_data_t::opt<unsigned> opt_u;

    data.Clean();

    auto list = str.split(' ');

    for(auto& i : list)
        i = i.trimmed();

    auto total = list.count();
    if(!total)
        return true;

    bool version3_upak = (list[0]==QString(QChar(0xA9)));
    int line_cnt = version3_upak?1:0;

    bool ok;
    int tmp_i;

    //считывание таймеров
    if(total-line_cnt<1)
        return false;
    int timers_num = list[line_cnt++].toInt(&ok);
    if(!ok)
        return false;

    if(timers_num!=0){
        if(total-line_cnt<1)
            return false;

        tmp_i = list[line_cnt++].toInt(&ok);
        if(!ok || timers_num+1!=tmp_i)
            return false;

        while(timers_num--){
            upak_data_t::upak_timer_t t;

            if(total-line_cnt<(version3_upak?4:3))
                return false;

            t.input = list[line_cnt++].toUInt(&ok);
            if(!ok) return false;

            t.output = list[line_cnt++].toUInt(&ok);
            if(!ok) return false;

            t.setpoint = list[line_cnt++].toUInt(&ok);
            if(!ok) return false;

            if(version3_upak)
                t.description = import_comment(list[line_cnt++]);

            data.timers.push_back(t);
        }
    }

    //считывание счетчиков
    if(total-line_cnt<1)
        return false;
    int counters_num = list[line_cnt++].toInt(&ok);
    if(!ok)
        return false;

    if(counters_num!=0){
        if(total-line_cnt<1)
            return false;

        tmp_i = list[line_cnt++].toInt(&ok);
        if(!ok || counters_num+1!=tmp_i)
            return false;

        while(counters_num--){
            upak_data_t::upak_counter_t c;

            if(total-line_cnt<(version3_upak?5:4))
                return false;

            c.input_clock = list[line_cnt++].toUInt(&ok);
            if(!ok) return false;

            c.input_reset = list[line_cnt++].toUInt(&ok);
            if(!ok) return false;

            c.output = list[line_cnt++].toUInt(&ok);
            if(!ok) return false;

            c.setpoint = list[line_cnt++].toUInt(&ok);
            if(!ok) return false;

            if(version3_upak)
                c.description = import_comment(list[line_cnt++]);

            data.counters.push_back(c);
        }
    }

    //считывание дозаторов
    if(total-line_cnt<1)
        return false;
    int doz_num = list[line_cnt++].toInt(&ok);
    if(!ok)
        return false;

    if(doz_num!=0){
        if(total-line_cnt<1)
            return false;

        tmp_i = list[line_cnt++].toInt(&ok);
        if(!ok || tmp_i!=doz_num+1)
            return false;

        if(total-line_cnt<5*doz_num+doz_num*8)
            return false;

        std::vector<opt_u> tmp_allow, tmp_end, tmp_open, tmp_empty, tmp_comp;

        tmp_i=doz_num;
        while(tmp_i--){
            unsigned tmp = list[line_cnt++].toUInt(&ok);
            tmp_allow.push_back(ok?opt_u(tmp):opt_u());
        }
        tmp_i=doz_num;
        while(tmp_i--){
            unsigned tmp = list[line_cnt++].toUInt(&ok);
            tmp_end.push_back(ok?opt_u(tmp):opt_u());
        }
        tmp_i=doz_num;
        while(tmp_i--){
            unsigned tmp = list[line_cnt++].toUInt(&ok);
            tmp_open.push_back(ok?opt_u(tmp):opt_u());
        }
        tmp_i=doz_num;
        while(tmp_i--){
            unsigned tmp = list[line_cnt++].toUInt(&ok);
            tmp_empty.push_back(ok?opt_u(tmp):opt_u());
        }
        tmp_i=doz_num;
        while(tmp_i--){
            for(int i=0; i<8; i++){
                unsigned tmp = list[line_cnt++].toUInt(&ok);
                tmp_comp.emplace_back(ok?opt_u(tmp):opt_u());
            }
        }

        //заполнение структуры дозаторов
        for(unsigned i=0; i<static_cast<unsigned>(doz_num); i++){

            upak_data_t::doser_t d;

            d.type = upak_data_t::doser_t::DOSER_WEIGHT;
            d.et04_num=i+1;
            d.et04_ch=0;
            d.flags[upak_data_t::doser_t::doser_flags_t::DOZ_FL_ALLOW]=tmp_allow[i];
            d.flags[upak_data_t::doser_t::doser_flags_t::DOZ_FL_END]=tmp_end[i];
            d.flags[upak_data_t::doser_t::doser_flags_t::DOZ_FL_OPEN]=tmp_open[i];
            d.flags[upak_data_t::doser_t::doser_flags_t::DOZ_FL_EMPTY]=tmp_empty[i];

            for(unsigned j=0; j<8; j+=2)
                d.component_speed.emplace_back(std::make_pair(tmp_comp[i*8+j], tmp_comp[i*8+j+1]));

            //определение количества компонент для дозатора
            //предполагаю максимальные 4 и убираю последние в цикле, если регистры обеих скоростей не заданы
            d.component_num=4;
            unsigned k_num=3;
            while(k_num<4){
                if(d.component_speed[k_num].first || d.component_speed[k_num].second)
                    break;
                k_num--;
                d.component_num--;
            }

            data.dosers.emplace_back(std::move(d));
        }
    }


    //считывание непрерывных дозаторов
    if(total-line_cnt<1)
        return false;
    doz_num = list[line_cnt++].toInt(&ok);
    if(!ok)
        return false;

    if(doz_num!=0){

        if(total-line_cnt<3*4)
            return false;

        std::vector<opt_u> tmp_mas;
        for(unsigned i=0; i<3*4; i++){
            unsigned tmp = list[line_cnt++].toUInt(&ok);
            tmp_mas.push_back(ok?opt_u(tmp):opt_u());
        }

        for(unsigned i=0; i<static_cast<unsigned>(doz_num); i++){
            upak_data_t::doser_t d;

            d.type = upak_data_t::doser_t::DOSER_PW;
            d.et04_num=static_cast<unsigned>(data.dosers.size()+1);
            d.et04_ch=0;

            d.flags[upak_data_t::doser_t::doser_flags_t::DOZ_FL_ALLOW]=tmp_mas[i*3];
            d.component_speed.emplace_back(std::make_pair(tmp_mas[i*3+1], tmp_mas[i*3+2]));
            d.component_num=1;

            data.dosers.emplace_back(std::move(d));
        }
    }

    //считывание весовых точек
    if(total-line_cnt<1)
        return false;
    doz_num = list[line_cnt++].toInt(&ok);
    if(!ok)
        return false;

    for(unsigned wp_doz=0; wp_doz<static_cast<unsigned>(doz_num); wp_doz++){

        if(total-line_cnt<1)
            return false;

        int ch_num = list[line_cnt++].toInt(&ok);
        if(!ok)
            return false;
        ch_num++;

        if(total-line_cnt<ch_num*3)
            return false;

        while(ch_num--){
             upak_data_t::doser_t::wp_t w;

             unsigned u32 = list[line_cnt++].toUInt(&ok);
             if(ok)
                 w.flag = u32;

             float f32 = list[line_cnt++].toFloat(&ok);
             if(ok)
                 w.weight = f32;

             f32 = list[line_cnt++].toFloat(&ok);
             if(ok)
                 w.hysteresis = f32;

             if(wp_doz<data.dosers.size())
                 data.dosers[wp_doz].wp.emplace_back(std::move(w));
        }
    }

    //считывание кол-ва модулей

    if(!version3_upak)
        return true;

    if(total-line_cnt<4)
        return false;

    unsigned et04_n, et01_n, et05_n, et07_n;

    et04_n = list[line_cnt++].toUInt(&ok);
    if(!ok)
        return false;

    et01_n = list[line_cnt++].toUInt(&ok);
    if(!ok)
        return false;

    et05_n = list[line_cnt++].toUInt(&ok);
    if(!ok)
        return false;

    et07_n = list[line_cnt++].toUInt(&ok);
    if(!ok)
        return false;

    uint8_t modbus_adr = static_cast<uint8_t>(et04_n&0xff)+1;
    unsigned reg=152;

    if(et01_n){
        for(unsigned i=0; i<et01_n; i++)
            data.et01.push_back(upak_data_t::et_data());
        for(unsigned i=0; i<et01_n; i++)
            data.et01[i].modbus_adr = modbus_adr++;
        for(unsigned i=0; i<et01_n; i++)
            for(unsigned j=0; j<8; j++)
                data.et01[i].outputs.push_back(reg++);
        for(unsigned i=0; i<et01_n; i++)
            for(unsigned j=0; j<16; j++)
                data.et01[i].inputs.push_back(reg++);
    }

    if(et05_n){
        for(unsigned i=0; i<et05_n; i++)
            data.et05.push_back(upak_data_t::et_data());
        for(unsigned i=0; i<et05_n; i++)
            data.et05[i].modbus_adr = modbus_adr++;
        for(unsigned i=0; i<et05_n; i++)
            for(unsigned j=0; j<24; j++)
                data.et05[i].outputs.push_back(reg++);
    }

    if(et07_n){
        for(unsigned i=0; i<et07_n; i++)
            data.et07.push_back(upak_data_t::et_data());
        for(unsigned i=0; i<et07_n; i++)
            data.et07[i].modbus_adr = modbus_adr++;
        for(unsigned i=0; i<et07_n; i++)
            for(unsigned j=0; j<24; j++)
                data.et07[i].inputs.push_back(reg++);
    }

    return true;
}


void MainWindow::SetupWidgetsFromImport(const QString& text, const MainWindow::upak_data_t& data)
{
     CleanAllWighets();

     ui->plainTextEdit_ProgramText->appendPlainText(text);


     QTableWidgetItem *table_item;
     int num;

     //таймеры
     num=0;
     for(const auto& t : data.timers){
         if(num>=ui->tableTimers->rowCount())
             break;

         for(int i=0; i<4; i++){
             table_item = ui->tableTimers->item(num, i);
             if(!table_item)
                 ui->tableTimers->setItem(num, i, new QTableWidgetItem());
         }

         ui->tableTimers->item(num, 0)->setText(QString::number(t.input));
         ui->tableTimers->item(num, 1)->setText(QString::number(t.output));
         ui->tableTimers->item(num, 2)->setText(QString::number(t.setpoint));
         ui->tableTimers->item(num, 3)->setText(t.description);

         num++;
     }

     //счетчики
     num=0;
     for(const auto& c : data.counters){
         if(num>=ui->tableCounters->rowCount())
             break;

         for(int i=0; i<5; i++){
             table_item = ui->tableCounters->item(num, i);
             if(!table_item)
                 ui->tableCounters->setItem(num, i, new QTableWidgetItem());
         }

         ui->tableCounters->item(num, 0)->setText(QString::number(c.input_clock));
         ui->tableCounters->item(num, 1)->setText(QString::number(c.input_reset));
         ui->tableCounters->item(num, 2)->setText(QString::number(c.output));
         ui->tableCounters->item(num, 3)->setText(QString::number(c.setpoint));
         ui->tableCounters->item(num, 4)->setText(c.description);

         num++;
     }


     bool warning_float_out_of_range = false;


     //дозаторы
     num=0;
     for(const auto& d : data.dosers){

         if(num>10)
             break;

         upak_compile::NOfDoz = num+1;

         auto& doz = upak_compile::dozator_data[num];

         doz.dozator_type=static_cast<uint8_t>(d.type);
         doz.NOfComp=static_cast<uint8_t>(d.component_num);
         doz.et04_adr=static_cast<uint8_t>(d.et04_num);
         doz.et04_channel=static_cast<uint8_t>(d.et04_ch);

         doz.flags.error_load = d.get_flag_string(upak_data_t::doser_t::doser_flags_t::DOZ_FL_ERR);
         doz.flags.error_zero = d.get_flag_string(upak_data_t::doser_t::doser_flags_t::DOZ_FL_ERR_ZERO);
         doz.flags.wait_weighter = d.get_flag_string(upak_data_t::doser_t::doser_flags_t::DOZ_FL_WAIT_WEIGHTER);
         doz.flags.substr_flag = d.get_flag_string(upak_data_t::doser_t::doser_flags_t::DOZ_FL_SUBSTR);
         doz.flags.DozAllow = d.get_flag_string(upak_data_t::doser_t::doser_flags_t::DOZ_FL_ALLOW);
         doz.flags.DozOpen = d.get_flag_string(upak_data_t::doser_t::doser_flags_t::DOZ_FL_OPEN);
         doz.flags.DozEnd = d.get_flag_string(upak_data_t::doser_t::doser_flags_t::DOZ_FL_END);
         doz.flags.DozEmpty = d.get_flag_string(upak_data_t::doser_t::doser_flags_t::DOZ_FL_EMPTY);

         auto comps = d.component_num<d.component_speed.size()?d.component_num:d.component_speed.size();
         for(decltype(comps) i=0; i<comps; i++){
             doz.flags.speed_12[i][0] = upak_data_t::get_opt_string(d.component_speed[i].first);
             doz.flags.speed_12[i][1] = upak_data_t::get_opt_string(d.component_speed[i].second);
         }

         doz.wp_num = static_cast<unsigned char>(d.wp.size()<4?d.wp.size():4);

         for(decltype(doz.wp_num) i=0; i<doz.wp_num; i++){

             while(true){
                 if(d.wp[i].weight)
                     if(d.wp[i].weight.value() >= WP_MIN_WEIGHT
                        && d.wp[i].weight.value() <= WP_MAX_WEIGHT){
                         doz.wp[i].weight = d.wp[i].weight.value();
                         break;
                     }
                 warning_float_out_of_range = true;
                 doz.wp[i].weight = WP_MIN_WEIGHT;
             }

             while(true){
                 if(d.wp[i].hysteresis)
                     if(d.wp[i].hysteresis.value() >= WP_MIN_GIST
                        && d.wp[i].hysteresis.value() <= WP_MAX_GIST){
                         doz.wp[i].gist = d.wp[i].hysteresis.value();
                         break;
                     }
                 warning_float_out_of_range = true;
                 doz.wp[i].gist = WP_MIN_GIST;
             }

             doz.wp[i].reg = upak_data_t::get_opt_string(d.wp[i].flag);
         }


         num++;
     }
     UpdateDozatorTree();


     if(warning_float_out_of_range)
         show_msg_box("Внимание! Несовместимые значений весовых точек! (Заменены на минимальные допустимые)");



     //модули

     //et01
     num = static_cast<int>(data.et01.size()<=8?data.et01.size():8);
     ui->spinBox_et01num->setValue(num);
     for(int i=0; i<num; i++){
         reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(0)->child(i), 1))->setValue(data.et01[static_cast<unsigned>(i)].modbus_adr);
         reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(0)->child(i), 2))->setValue(static_cast<int>(data.et01[static_cast<unsigned>(i)].inputs[0]));
         reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(0)->child(i), 3))->setValue(static_cast<int>(data.et01[static_cast<unsigned>(i)].outputs[0]));
     }

     //et05
     num = static_cast<int>(data.et05.size()<=8?data.et05.size():8);
     ui->spinBox_et05num->setValue(num);
     for(int i=0; i<num; i++){
         reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(1)->child(i), 1))->setValue(data.et05[static_cast<unsigned>(i)].modbus_adr);
         reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(1)->child(i), 3))->setValue(static_cast<int>(data.et05[static_cast<unsigned>(i)].outputs[0]));
     }

     //et07
     num = static_cast<int>(data.et07.size()<=8?data.et07.size():8);
     ui->spinBox_et07num->setValue(num);
     for(int i=0; i<num; i++){
         reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(2)->child(i), 1))->setValue(data.et07[static_cast<unsigned>(i)].modbus_adr);
         reinterpret_cast<SpinBoxItem*>(ui->treeModules->itemWidget(ui->treeModules->topLevelItem(2)->child(i), 2))->setValue(static_cast<int>(data.et07[static_cast<unsigned>(i)].inputs[0]));
     }




}

QString MainWindow::import_comment(const QString& s)
{
    QString ret_s;

    while(true){

        if(s.size()<2)
            break;

        if(s[0]!=QChar(0xAE) || s[s.size()-1]!=QChar(0xAE))
            break;

        ret_s = s.mid(1, s.size()-2);

        if(ret_s=="nocomments")
            ret_s.clear();

        break;
    }

    return ret_s;
}

void MainWindow::import_ell_callback()
{
    //диалог выбора файла ell/lp
   QString filename = QFileDialog::getOpenFileName(this, "Открыть", QApplication::applicationDirPath(), "Упак3 (ELL и LP) (*.lp *.ell)",  nullptr, nullptr);
   if(!filename.length()) return;

   //получение имен файлов .ell и .lp
   QString filename_ell, filename_lp;
   if(filename.endsWith(".ell")){
       filename_ell = filename;
       filename.resize(filename.length()-4);
       filename_lp = filename+".lp";
   }else if(filename.endsWith(".lp")){
       filename_lp = filename;
       filename.resize(filename.length()-3);
       filename_ell = filename+".ell";
   }else
       return;

   QByteArray arr_ell, arr_lp;

   if(!read_file(filename_ell, arr_ell)){
       show_msg_box("Ошибка чтения файла "+filename+" !");
       return;
   }
   if(!read_file(filename_lp, arr_lp)){
       show_msg_box("Ошибка чтения файла "+filename+" !");
       return;
   }

   QString ell_text = get_string_from_1251(arr_ell);
   if(!ell_text.length()){
       show_msg_box("Ошибка преобразования файла текста программы (ELL) !");
       return;
   }

   QString lp_text = get_string_from_1251(arr_lp);
   if(!lp_text.length()){
       show_msg_box("Ошибка преобразования файла настроек Упак3 (LP) !");
       return;
   }

   upak_data_t lp_data;
   if(!get_upak_data_from_lp(lp_text, lp_data)){
       show_msg_box("Ошибка при разборе данных файла настроек Упак3 (LP) !");
       return;
   }

   OpenedFileName.clear();

   SetupWidgetsFromImport(ell_text, lp_data);
}

void MainWindow::UpdateProgressBar(int value){
    if(value<0) value=0;
    if(value>100) value=100;
    ProgressBar->setValue(value);
}



void MainWindow::on_btn_GetProgramText_released()
{
    LoadUPAKtext();
}

void MainWindow::GetComSettings(upak_settings_t& s)
{
    this->COM_num=s["COM"].toInt();
    this->baudrate=s["BAUDRATE"].toInt();
    this->NetAdr=s["MODBUS_ADR"].toInt();
}

bool MainWindow::request_regs(bool silent_mode)
{
    ushort *data;
    message_data_t msg;
    bool res;

    if(!port.IsOpened()){
        if(!silent_mode){
            QMessageBox msgBox;
            msgBox.setText("COM-порт закрыт !!!");
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
        }
        return false;
    }

    data = new ushort[256];

    msg.label = GET_REGS;
    msg.adr=static_cast<int8_t>(this->NetAdr);
    msg.data_buf=data;
    msg.msg_type = MSG_TYPE_READ;
    msg.StartReg = MODBUS_REG_REGS;
    msg.RegCount = 256;
    msg.msg_priority = PRIORITY_LOW;

    res=port.message(msg);
    if(!res){
        if(!silent_mode){
            QMessageBox msgBox;
            msgBox.setText("Очередь обмена через COM-порт полностью заполнена! Попробуйте послать сообщение через несколько секунд!");
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
        }
        return false;
    }

    return true;
}
