#include <QTime>
#include "serial.h"
#include "modbus.h"
#include <QDebug>


#define  N_OF_ATTEMPTS   3    //кол-во попыток повторной передачи сообщения
#define  MAX_REGS        32   //максимальное кол-во регистров modbus в одном сообщении

    //количество непрерывно обрабатываемых сообщений одного приоритета до выборки сообщения из очереди младшего приоритета
#define  SCHEDULE_STEPS     5
    //максимальное количество сообщение в очереди каждого приоритета
#define  QUEUE_MAX_LENGTH   50


const uchar ModbusThread::auchCRCHi[]={
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,
0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,
0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,
0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,
0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,
0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40};

const uchar ModbusThread::auchCRCLo[] = {
0x00,0xC0,0xC1,0x01,0xC3,0x03,0x02,0xC2,0xC6,0x06,0x07,0xC7,0x05,0xC5,0xC4,0x04,
0xCC,0x0C,0x0D,0xCD,0x0F,0xCF,0xCE,0x0E,0x0A,0xCA,0xCB,0x0B,0xC9,0x09,0x08,0xC8,
0xD8,0x18,0x19,0xD9,0x1B,0xDB,0xDA,0x1A,0x1E,0xDE,0xDF,0x1F,0xDD,0x1D,0x1C,0xDC,
0x14,0xD4,0xD5,0x15,0xD7,0x17,0x16,0xD6,0xD2,0x12,0x13,0xD3,0x11,0xD1,0xD0,0x10,
0xF0,0x30,0x31,0xF1,0x33,0xF3,0xF2,0x32,0x36,0xF6,0xF7,0x37,0xF5,0x35,0x34,0xF4,
0x3C,0xFC,0xFD,0x3D,0xFF,0x3F,0x3E,0xFE,0xFA,0x3A,0x3B,0xFB,0x39,0xF9,0xF8,0x38,
0x28,0xE8,0xE9,0x29,0xEB,0x2B,0x2A,0xEA,0xEE,0x2E,0x2F,0xEF,0x2D,0xED,0xEC,0x2C,
0xE4,0x24,0x25,0xE5,0x27,0xE7,0xE6,0x26,0x22,0xE2,0xE3,0x23,0xE1,0x21,0x20,0xE0,
0xA0,0x60,0x61,0xA1,0x63,0xA3,0xA2,0x62,0x66,0xA6,0xA7,0x67,0xA5,0x65,0x64,0xA4,
0x6C,0xAC,0xAD,0x6D,0xAF,0x6F,0x6E,0xAE,0xAA,0x6A,0x6B,0xAB,0x69,0xA9,0xA8,0x68,
0x78,0xB8,0xB9,0x79,0xBB,0x7B,0x7A,0xBA,0xBE,0x7E,0x7F,0xBF,0x7D,0xBD,0xBC,0x7C,
0xB4,0x74,0x75,0xB5,0x77,0xB7,0xB6,0x76,0x72,0xB2,0xB3,0x73,0xB1,0x71,0x70,0xB0,
0x50,0x90,0x91,0x51,0x93,0x53,0x52,0x92,0x96,0x56,0x57,0x97,0x55,0x95,0x94,0x54,
0x9C,0x5C,0x5D,0x9D,0x5F,0x9F,0x9E,0x5E,0x5A,0x9A,0x9B,0x5B,0x99,0x59,0x58,0x98,
0x88,0x48,0x49,0x89,0x4B,0x8B,0x8A,0x4A,0x4E,0x8E,0x8F,0x4F,0x8D,0x4D,0x4C,0x8C,
0x44,0x84,0x85,0x45,0x87,0x47,0x46,0x86,0x82,0x42,0x43,0x83,0x41,0x81,0x80,0x40};



void ModbusThread::fast_crc16(uchar* crc, uchar *buf, uint buf_size){
 uint uIndex;       /* will index into CRC lookup table */
 crc[0] = 0xFF;     /* high byte of CRC initialized     */
 crc[1] = 0xFF;     /* low byte of CRC initialized      */

  while(buf_size)   /* pass through message buffer */
   {
    uIndex = crc[0] ^ *buf++ ;    /* calculate the CRC  */
    crc[0] = crc[1] ^ auchCRCHi[uIndex];
    crc[1] = auchCRCLo[uIndex];
    buf_size--;
   }
}


int ModbusThread::check_crc16(uchar *buf, uint buf_size, uchar* crc){
 uchar crc_new[2];

  fast_crc16(crc_new, buf, buf_size);
  if(crc[0]==crc_new[0] && crc[1]==crc_new[1]) return 1;
 return 0;
}




ModbusThread::ModbusThread(){
 queue_counter_high=0;
 queue_counter_normal=0;

 queue_high.clear();
 queue_normal.clear();
 queue_low.clear();


 stop_thread_flag=false;

 percent_complete=0;
 port_state=COM_STATUS_CLOSED;
 this->start(QThread::HighPriority);
}


ModbusThread::~ModbusThread(){
 mutex.lock();
  stop_thread_flag=true;
 mutex.unlock();
 if(!this->wait(1000)){this->terminate();}
 //очистка очередей планировщика
 while(queue_high.size()) task_done(queue_high.dequeue(), STATUS_FAULT);
 while(queue_normal.size()) task_done(queue_normal.dequeue(), STATUS_FAULT);
 while(queue_low.size()) task_done(queue_low.dequeue(), STATUS_FAULT);
 Close();
}




bool ModbusThread::Open(int com_port, int baudrate){
 bool res;

  this->Close();
  QString strPort = "\\\\.\\COM" + QString::number(com_port);
  memset(com_str, 0, sizeof(com_str));
  memcpy(com_str, strPort.toLatin1().data(), static_cast<size_t>(strPort.length()));

  mutex.lock();
   this->baudrate=static_cast<uint32_t>(baudrate);
   res = com.Initialize(com_str, static_cast<unsigned long>(baudrate));
   if(res) port_state=COM_STATUS_OPENED;
  mutex.unlock();

  return res;
}


void ModbusThread::Close(){
 mutex.lock();
  if(port_state==COM_STATUS_OPENED) com.BreakDownCommPort();
  port_state=COM_STATUS_CLOSED;
 mutex.unlock();
}


bool ModbusThread::IsOpened(){
  bool res;

  mutex.lock();
    if(port_state==COM_STATUS_OPENED) res=true;
                                 else res=false;
  mutex.unlock();
  return res;
}



void ModbusThread::run(){
    bool exit, timeout_processed=false;
    int attempts, res;
    uchar local_port_state;


    while(true){
        mutex.lock();
         exit = stop_thread_flag;
         local_port_state=port_state;
         percent_complete=0;
        mutex.unlock();

        if(exit) break;


        res=SelectMessage(msg_data);
        if(!res) {this->msleep(2); timeout_processed=true; continue;}

        if(local_port_state!=COM_STATUS_OPENED){
          task_done(msg_data, STATUS_FAULT);
          this->msleep(2);
          timeout_processed=true;
          continue;
        }

        part_reg=0;

        if(msg_data.msg_type == MSG_TYPE_SET_REG) msg_data.RegCount=1;

        if(msg_data.RegCount>MAX_REGS) NOfRegs=MAX_REGS;
                                 else  NOfRegs=msg_data.RegCount;


        attempts=N_OF_ATTEMPTS;
        while(attempts && !exit && part_reg<msg_data.RegCount){
             attempts--;
             if(!timeout_processed) {this->msleep(1); timeout_processed=true;}
             res=PollTarget();
             if(res==0){
               timeout_processed=false;
               attempts=N_OF_ATTEMPTS;
               part_reg+=NOfRegs;
                mutex.lock();
                 percent_complete=static_cast<uint8_t>((100*part_reg)/msg_data.RegCount);
                mutex.unlock();
               emit progress(percent_complete);
               if(msg_data.RegCount-part_reg>MAX_REGS) NOfRegs=MAX_REGS;
                                                 else  NOfRegs=msg_data.RegCount-part_reg;
             }

             mutex.lock();
              exit = stop_thread_flag;
             mutex.unlock();
            }


          if(res==0 && !exit) emit task_done(msg_data, STATUS_OK);
                         else emit task_done(msg_data, STATUS_FAULT);

    }

}




int ModbusThread::PollTarget(){
    int res=(-1);

  switch(msg_data.msg_type){
   case MSG_TYPE_WRITE: res=SetReg(); break;
   case MSG_TYPE_READ:  res=GetReg(); break;
   case MSG_TYPE_SET_REG: res=ForceSingleCoil(); break;
  }

 return res;
}



int ModbusThread::SetReg(){
     uchar msg[MAX_REGS*2+6];
     uint i;
     ushort value, len, timeout;
     QTime time, start_time;
     ushort reg, num;

      reg = msg_data.StartReg+part_reg;
      num = NOfRegs;

      //проверка на максимальное кол-во данных в сообщении
      if(num>32) return -1;

      //составляю модбас сообщение
      msg[0]=static_cast<uint8_t>(msg_data.adr);
      msg[1]=0x10;
      msg[2]=static_cast<uint8_t>((reg>>8)&0x00ff);
      msg[3]=static_cast<uint8_t>(reg&0x00ff);
      msg[4]=0;
      msg[5]=static_cast<uint8_t>(num);
      msg[6]=static_cast<uint8_t>(num*2);

      for(i=0; i<num; i++){
       value=msg_data.data_buf[part_reg+i];
       msg[i*2+7]=static_cast<uint8_t>((value>>8)&0x00ff);
       msg[i*2+8]=static_cast<uint8_t>(value&0x00ff);
      }
      fast_crc16(&msg[7+num*2], msg, num*2+7);

      len=num*2+9;
      timeout = static_cast<ushort>((50+len)*(38400.0f/static_cast<float>(baudrate))); //вычисление таймаута задержки, нормированного на 100мс для скорости 38400

      //отправка сообщения
      com.ClearInCommBuffer();
      if(com.Out(msg, len) != len) return -2;

      start_time = QTime::currentTime();
      time = QTime::currentTime().addMSecs(timeout);

      //получение ответа
      len=8;    //длина ожидаемых байт
      i=0;

      do{
       i += com.In(&msg[i], static_cast<int>(len-i));
       if(i==len) break;
       if(i==5 && (msg[1]&0x80)) return -6;  //ответ с кодом ошибки модбас
       if(QTime::currentTime()<start_time) break; //защита от залипания в цикле при переводе часов( или при автоматической синхронизации времени в системе)
      }while(QTime::currentTime()<time);

      if(i!=len) return -3;

      //проверка полученного сообщения
      if(!check_crc16(msg, len-2, &msg[len-2])) return -4;

      if(msg[5]!=num) return -5;

    return 0;
}



int ModbusThread::GetReg(){
 uchar msg[MAX_REGS*2+6];
 ushort reg, num, len, timeout, value;
 QTime time, start_time;
 uint i;

   reg = msg_data.StartReg + part_reg;
   num = NOfRegs;

  //составляю сообщение
   msg[0]=static_cast<uint8_t>(msg_data.adr);
   msg[1]=0x03;
   msg[2]=static_cast<uint8_t>((reg>>8)&0x00ff);
   msg[3]=static_cast<uint8_t>(reg&0x00ff);
   msg[4]=0;
   msg[5]=static_cast<uint8_t>(num);
   fast_crc16(reinterpret_cast<uchar*>(&msg[6]), msg, 6);

   len=8;

  //отправка сообщения
   com.ClearInCommBuffer();
   if(com.Out(msg, len) != len) return -2;

  //получение ответа
   len=num*2+5;
   timeout = static_cast<ushort>((50+len)*(38400.f/static_cast<float>(baudrate))); //вычисление таймаута задержки, нормированного на 100мс для скорости 38400

   start_time = QTime::currentTime();
   time = start_time.addMSecs(timeout);

   i=0;

   do{
    i += com.In(&msg[i], static_cast<int>(len-i));
    if(i==len) break;
    if(i==5 && (msg[1]&0x80)) return -6;  //ответ с кодом ошибки модбас
    if(QTime::currentTime()<start_time) break; //защита от залипания в цикле при переводе часов( или при автоматической синхронизации времени в системе)
   }while(QTime::currentTime()<time);

   if(i!=len) return -3;

   //проверка полученного сообщения
   if(!check_crc16(msg, len-2, &msg[len-2])) return -4;

   if(msg[2]!=num*2) return -5;

   //обработка сообщения
   for(i=0; i<num; i++){
    value=( static_cast<ushort>(static_cast<ushort>(msg[i*2+3])<<8) | static_cast<ushort>(msg[i*2+4]) );
    msg_data.data_buf[part_reg+i]=value;
   }

 return 0;
}



int ModbusThread::ForceSingleCoil(){
    uchar msg[8], reply[8];
    uint i;
    ushort len, timeout;
    QTime time, start_time;
    ushort reg;

     reg = msg_data.StartReg;

     //составляю модбас сообщение
     msg[0]=static_cast<uint8_t>(msg_data.adr);
     msg[1]=0x05;
     msg[2]=static_cast<uint8_t>((reg>>8)&0x00ff);
     msg[3]=static_cast<uint8_t>(reg&0x00ff);
     if(*msg_data.data_buf) msg[4]=0xff;
                       else msg[4]=0x00;
     msg[5]=0x00;

     fast_crc16(&msg[6], msg, 6);

     len=8;
     timeout = static_cast<ushort>((50+len)*(38400.f/static_cast<float>(baudrate))); //вычисление таймаута задержки, нормированного на 100мс для скорости 38400

     //отправка сообщения
     com.ClearInCommBuffer();
     if(com.Out(msg, len) != len) return -2;

     start_time = QTime::currentTime();
     time = QTime::currentTime().addMSecs(timeout);

     //получение ответа
     len=8;    //длина ожидаемых байт
     i=0;

     do{
      i += com.In(&reply[i], static_cast<int>(len-i));
      if(i==len) break;
      if(i==5 && (msg[1]&0x80)) return -6;  //ответ с кодом ошибки модбас
      if(QTime::currentTime()<start_time) break; //защита от залипания в цикле при переводе часов( или при автоматической синхронизации времени в системе)
     }while(QTime::currentTime()<time);

     if(i!=len) return -3;

     //проверка полученного сообщения
     if(memcmp(msg, reply, 8)) return -4;

    return 0;
}





bool ModbusThread::message(const message_data_t& msg_data){
 bool res=false;

    SchedulerMutex.lock();

    switch(msg_data.msg_priority){
     case PRIORITY_HIGH:    if(queue_high.size()==QUEUE_MAX_LENGTH) break;
                            queue_high.enqueue(msg_data);
                            res=true;
                            break;
     case PRIORITY_NORMAL:  if(queue_normal.size()==QUEUE_MAX_LENGTH) break;
                            queue_normal.enqueue(msg_data);
                            res=true;
                            break;
     case PRIORITY_LOW:     if(queue_low.size()==QUEUE_MAX_LENGTH) break;
                            queue_low.enqueue(msg_data);
                            res=true;
                            break;
    }

    SchedulerMutex.unlock();
    return res;
}



bool ModbusThread::SelectMessage(message_data_t& msg_data){
 int total_msgs;

   SchedulerMutex.lock();

   total_msgs=queue_high.size()+queue_normal.size()+queue_low.size();

   while(total_msgs){

       if(queue_high.size() && queue_counter_high<SCHEDULE_STEPS){
           queue_counter_high++;
           msg_data=queue_high.dequeue();
           break;
       }
       queue_counter_high%=SCHEDULE_STEPS;

       if(queue_normal.size() && queue_counter_normal<SCHEDULE_STEPS){
           queue_counter_normal++;
           msg_data=queue_normal.dequeue();
           break;
       }
       queue_counter_normal%=SCHEDULE_STEPS;

       if(queue_low.size()){
           msg_data=queue_low.dequeue();
           break;
       }

   }

   SchedulerMutex.unlock();

 if(total_msgs) return true;
           else return false;
}
