#ifndef UPAK_SETTINGS_H
#define UPAK_SETTINGS_H


#include <map>
#include <utility>

#include <QDir>


typedef std::map<QString, QString> upak_settings_t;


static const upak_settings_t default_upak_settings = {
    {"COM", "1"},
    {"BAUDRATE", "38400"},
    {"MODBUS_ADR", "1"}};



class upak_settings
{
public:
    upak_settings(QString filename="upak_settings.txt");
    upak_settings_t get(){return settings;}
    bool set(const upak_settings_t& s){settings = s; return save();}
    upak_settings_t& operator()(){return settings;}
private:
    upak_settings_t settings;
    QString f_name;
    QDir root_directory;
    bool load();
    bool save();
    bool GetKeyValuePair(const QString& str, std::pair<QString, QString>& key_value);
};

#endif // UPAK_SETTINGS_H
