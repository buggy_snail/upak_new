#-------------------------------------------------
#
# Project created by QtCreator 2012-01-12T12:24:41
#
#-------------------------------------------------

QT       += core gui

TARGET = upak
TEMPLATE = app


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += main.cpp\
    common.cpp \
        mainwindow.cpp \
    upak_compile.cpp \
    compile_stack.cpp \
    comboboxitem.cpp \
    lineedititem.cpp \
    spinboxitem.cpp \
    doublespinboxitem.cpp \
    com_settings.cpp \
    modbus.cpp \
    serial.cpp \
    finddialog.cpp \
    syntax_highlighter.cpp \
    upak_settings.cpp

HEADERS  += mainwindow.h \
    common.h \
    upak_compile.h \
    compile_stack.h \
    comboboxitem.h \
    lineedititem.h \
    spinboxitem.h \
    doublespinboxitem.h \
    com_settings.h \
    modbus.h \
    serial.h \
    finddialog.h \
    syntax_highlighter.h \
    upak_settings.h

FORMS    += mainwindow.ui


