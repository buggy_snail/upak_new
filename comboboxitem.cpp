#include "comboboxitem.h"

ComboBoxItem::ComboBoxItem(QTreeWidgetItem *item, int column)
{
    this->item = item;
    this->column = column;
    this->setFocusPolicy(Qt::StrongFocus);
    connect(this, SIGNAL(currentIndexChanged(int)), SLOT(changeItem(int)));
}


void ComboBoxItem::changeItem(int index)
{
    if(index >=0)
        item->setText(this->column, QString::number(index));
}
