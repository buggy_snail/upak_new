#ifndef MODBUS_H
#define MODBUS_H

#include <QThread>
#include <QMutex>
#include <QObject>
#include <QQueue>
#include "serial.h"


enum MSG_TYPE{
 MSG_TYPE_WRITE,
 MSG_TYPE_READ,
 MSG_TYPE_SET_REG
};

enum MSG_PRIORITY{
  PRIORITY_HIGH,
  PRIORITY_NORMAL,
  PRIORITY_LOW
};

enum MSG_STATUS{
  STATUS_OK,
  STATUS_FAULT
};

typedef struct{
   MSG_TYPE msg_type;          //тип сообщения - чтение\запись\запись одного флага
   uint16_t* data_buf;         //буфер с данными\для данных\ либо значение флага(ненулевое значение - флаг в единице)
   int8_t adr;                 //адрес назначения сообщения
   uint16_t StartReg;          //начальный номер регистра для чтения записи \ либо номер флага для записи
   uint16_t RegCount;          //кол-во регистров для записи\чтения
   int8_t label;               //метка сообщения(его тип), для его отличия от других сообщений в последующей обработке
   MSG_PRIORITY msg_priority;  //приоритет обработки сообщения
} message_data_t;


enum{
 COM_STATUS_CLOSED,
 COM_STATUS_OPENED
};



class ModbusThread : public QThread{
 Q_OBJECT

 public:
    ModbusThread();
    ~ModbusThread();

    bool Open(int COM_port, int baudrate);
    void Close();
    bool IsOpened();

     //записывает новое сообщение в очередь обмена
     //возвращает false , если очередь слишком забита или порт не открыт
    bool message(const message_data_t& msg_data);

    static void fast_crc16(uchar* crc, uchar *buf, uint buf_size);
    static int check_crc16(uchar *buf, uint buf_size, uchar* crc);

 protected:
    void run();

 private:
    //данные для обрабатывающего потока под защитой мьютекса
     QMutex mutex;
     volatile bool stop_thread_flag;
     uchar port_state;
     uchar percent_complete;
     DHardwork com;
     char com_str[20];
     uint32_t baudrate;
    //--------------------------------------------------------

    // данные только для работы потока опроса
     message_data_t msg_data;
     uint16_t part_reg;
     uint16_t NOfRegs;
    //--------------------------------------------------------

    QQueue<message_data_t> queue_high, queue_normal, queue_low;
    int queue_counter_high, queue_counter_normal;
    QMutex SchedulerMutex;

    bool SelectMessage(message_data_t& msg_data);   //выбор потоком обработки очередного сообщения для отправки

    int PollTarget();
    int SetReg();
    int GetReg();
    int ForceSingleCoil();

    static const uchar auchCRCHi[256];
    static const uchar auchCRCLo[256];

 signals:
    //отправка результатов посылки сообщения
    //для обработки сообщения в принимающем потоке, надо делать connect обязательно с типом Qt::QueuedConnection
    void task_done(message_data_t data, MSG_STATUS status);
    //текущие проценты выполнения посылки\приема. сигнал посылается между приемом отправкой очередной части большого сообщения
    //для обработки в принимающем потоке, надо делать connect обязательно с типом Qt::QueuedConnection
    void progress(int percent_complete);
};


#endif // MODBUS_H
