#ifndef SYNTAX_HIGHLIGHTER_H
#define SYNTAX_HIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QString>

class Highlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    explicit Highlighter(QTextDocument  *parent, int(*func)(QStringRef)=0);

    void SetDebugMode(bool state) {debug_mode_flag=state;}

protected:
    void highlightBlock(const QString &text);

signals:

public slots:

private:
    struct HighlightingRule
         {
             QRegExp pattern;
             QTextCharFormat format;
         };
    QVector<HighlightingRule> highlightingRules;

    QTextCharFormat reg_num_format;
    QTextCharFormat reg_name_format;
    QTextCharFormat comment_format;
    QTextCharFormat condition_format;
    QTextCharFormat define_format;

    QVector<HighlightingRule> highlightingDebugRules;

    QTextCharFormat reg_num_name_debug_format;
    QTextCharFormat comment_debug_format;
    QTextCharFormat condition_debug_format;
    QTextCharFormat define_debug_format;

    HighlightingRule DebugRegsRule;

    void SetOrdinaryRules();
    void SetDebugModeRules();
    bool debug_mode_flag;

    int(*get_reg_func)(QStringRef);
};

#endif // SYNTAX_HIGHLIGHTER_H
