#include "doublespinboxitem.h"


DoubleSpinBoxItem::DoubleSpinBoxItem(QTreeWidgetItem *item, int column)
{
    this->item = item;
    this->column = column;
    this->setFocusPolicy(Qt::StrongFocus);
    connect(this, SIGNAL(valueChanged(double)), SLOT(changeItem(double)));
}


void DoubleSpinBoxItem::changeItem(double value)
{
   item->setText(this->column, QString::number(value));
}
