#ifndef COMMON_H
#define COMMON_H

#include <QString>
#include <QMessageBox>

void show_msg_box(const QString&, QMessageBox::Icon = QMessageBox::Warning);

#endif // COMMON_H
