#ifndef COMPILE_STACK_H
#define COMPILE_STACK_H
#include <QString>

class compile_stack
{
private:
    QChar st[100];
    int NumOfStrings;

public:
    compile_stack(){NumOfStrings=0;}
    void  Push(QChar);
    QChar Pop(void);
    void  Clear(){NumOfStrings=0;}
    bool  IsEmpty();
};


#endif // COMPILE_STACK_H
