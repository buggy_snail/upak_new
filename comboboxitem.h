#ifndef COMBOBOXITEM_H
#define COMBOBOXITEM_H

#include <QComboBox>
#include <QTreeWidgetItem>
#include <QWheelEvent>

class ComboBoxItem : public QComboBox
{
    Q_OBJECT

private:
    QTreeWidgetItem *item;
    int column;

public:
    ComboBoxItem(QTreeWidgetItem *item, int column);

public slots:
    void changeItem(int);

protected:

    virtual void wheelEvent(QWheelEvent *event) { event->ignore();}

};

#endif // COMBOBOXITEM_H
