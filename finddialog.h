#ifndef FINDDIALOG_H
#define FINDDIALOG_H

#include <QDialog>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QGridLayout>
#include <QMessageBox>
#include <QTextDocument>
#include <QCheckBox>
#include <QAction>


class FindDialog : public QDialog
{
    Q_OBJECT
public:
    explicit FindDialog(QWidget *parent = 0);

    QString getFindText();
    QTextDocument::FindFlags GetSearchParams();
    void fault_msg();
    
signals:
    void find_pressed();
    void find_hide();
    
public slots:
    void findClicked();

private:
    QPushButton *findButton;
    QLineEdit *lineEdit;
    QString findText;
    QTextDocument::FindFlags flags;
    QCheckBox *checkbox_Backward;
    QCheckBox *checkbox_CaseSensitively;
    QCheckBox *checkbox_FindWholeWords;

    void hideEvent(QHideEvent * event);
};

#endif // FINDDIALOG_H

