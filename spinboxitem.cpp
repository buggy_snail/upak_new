#include "spinboxitem.h"

SpinBoxItem::SpinBoxItem(QTreeWidgetItem *item, int column)
{
    this->item = item;
    this->column = column;
    this->setFocusPolicy(Qt::StrongFocus);
    connect(this, SIGNAL(valueChanged(int)), SLOT(changeItem(int)));
}


void SpinBoxItem::changeItem(int value)
{
   item->setText(this->column, QString::number(value));
}
