#include "upak_settings.h"

#include <QFile>
#include <QByteArray>
#include <QCoreApplication>


upak_settings::upak_settings(QString filename) : f_name(filename)
{
    root_directory.setPath(QCoreApplication::applicationDirPath());

    if(!load())
        settings = default_upak_settings;
}

bool upak_settings::load()
{
    QFile file(root_directory.filePath(f_name));

    QByteArray arr;

    if(file.open(QIODevice::ReadOnly)){
        arr = file.readAll();
        file.close();
    }

    if(!arr.size())
        return false;

    settings.clear();
    QString big_str(QString::fromUtf8(arr.data(), arr.size()));
    QStringList str_list = big_str.split('\n', QString::SkipEmptyParts);
    std::pair<QString, QString> kv;
    for(const auto& s : str_list){
        if(GetKeyValuePair(s, kv))
            settings[kv.first] = kv.second;
    }

    return  true;
}

bool upak_settings::save()
{
    QString big_str;
    for(const auto&p : settings)
        big_str += p.first + "=" + p.second + '\n';

    QByteArray arr = big_str.toUtf8();

    QFile file(root_directory.filePath(f_name));
    if(file.open(QIODevice::WriteOnly | QIODevice::Truncate)){
        file.write(arr.data(), arr.size());
        file.flush();
        file.close();
    }else
        return false;

    return true;
}

bool upak_settings::GetKeyValuePair(const QString &str, std::pair<QString, QString> &key_value)
{
    int pos = str.indexOf('=');
    if(pos<0)
        return false;

    key_value.first = str.left(pos);
    pos+=1;
    int n = str.size()-pos;
    key_value.second = n>0?str.right(n):"";

    return true;
}
