#include "upak_compile.h"


QStringList upak_compile::rules;
QHash<QString, uint16_t> upak_compile::hash_rules;

int upak_compile::jump_stack[50];
int upak_compile::jump_stack_ptr;

ushort upak_compile::mas[5*UPAK_MAX_SIZE/2];  //компилированный код упака  ; c 5кратным запасом
ushort upak_compile::mas_words;               //счетчик компилированных слов

QString upak_compile::ErrString;   //строка с текстом последней ошибки


upak_dozator_data_t upak_compile::dozator_data[10]; //данные дозаторов для записи в прибор
int upak_compile::NOfDoz;   //количество дозаторов

QSet<uint16_t> upak_compile::USED_REGS; //набор регистров, используемый в программе упак. заполняется во время компиляции




bool upak_compile::Compile(const QStringList &list)
{
 int line_num, i, res;
 QString poland_str, line, str1, str2;
 static QRegExp rx;
 bool ok;
 ushort reg, value;

 if(!rules.count()) ClearRules(); //если список правил еще не проинициализирован ни разу


 //очищаю стек условных переходов и массив байт-кода
 jump_stack_ptr=0;
 mas_words=0;

 for(line_num=0; line_num<list.count(); line_num++){
     poland_str.clear();
     line=list[line_num];
     line = line.trimmed();

     if(line.isEmpty()) continue;

     if(line.startsWith("#*")){

         //проверка наличия условного перехода
         if(line.length()<6) {
             ErrString = "Ошибка записи условного блока!";
             ErrString += " строка " + QString::number(line_num+1);
             return false;
         }

         line = line.toUpper();
         line = line.simplified();
         line.remove(0, 2);
         line = line.simplified();



         if(QString::compare(line.left(4), "STOP")==0){
             line.remove(0,4);
             line = line.simplified();
             if(line.length()){
                 ErrString = "Ошибка записи условного перехода! Строка после директивы 'STOP' не пустая!";
                 ErrString += " строка " + QString::number(line_num+1);
                 return false;
             }else{
                //---------  закрытие секции
                 if(jump_stack_ptr==0){
                     ErrString = "Количество открывающих и закрывающих секций условных переходов START-STOP не равно!";
                     return false;
                 }
                 jump_stack_ptr--;
                 mas[jump_stack[jump_stack_ptr]+1]=static_cast<ushort>(mas_words-(jump_stack[jump_stack_ptr]+2));
                //---------
             }
             continue;
         }


         if(QString::compare(line.left(5), "START")==0){
                 if(jump_stack_ptr>=50){
                     ErrString="Превышение максимальной вложенности условных секций!";
                     return false;
                 }

                 line.remove(0, 5);
                 line=line.simplified();

                 rx.setPattern("^(\\d\\d{0,3})\\s{0,10}=\\s{0,10}(0|1)$");
                 res = rx.indexIn(line);
                 if(res<0){
                     ErrString = "Ошибка записи условного перехода!";
                     ErrString += " строка " + QString::number(line_num+1);
                     return false;
                 }else{
                   str1=rx.cap(1);
                   str2=rx.cap(2);

                   reg=str1.toUShort(&ok);
                   if(!ok){
                       ErrString = "Ошибка в номере регистра при записи условного перехода!";
                       ErrString += " строка " + QString::number(line_num+1);
                       return false;
                   }
                   if(reg>4095){
                       ErrString = "Номер регистра при записи условного перехода >4095!";
                       ErrString += " строка " + QString::number(line_num+1);
                       return false;
                   }

                   value=str2.toUShort();

                   //----------- открытие секции
                    jump_stack[jump_stack_ptr++]=mas_words;
                    mas[mas_words]=(0x4000 | reg);
                    if(value) mas[mas_words]|=0x1000;
                    mas_words+=2;
                   //-----------
                    upak_compile::USED_REGS+=reg;
                 }
                 continue;
               }


         if(QString::compare(line.left(5), "BREAK")==0){
             line.remove(0, 5);
             line=line.simplified();

             rx.setPattern("^(\\d\\d{0,3})\\s{0,10}=\\s{0,10}(0|1)$");
             res = rx.indexIn(line);
             if(res<0){
                 ErrString = "Ошибка записи условия BREAK!";
                 ErrString += " строка " + QString::number(line_num+1);
                 return false;
             }else{
               str1=rx.cap(1);
               str2=rx.cap(2);

               reg=str1.toUShort(&ok);
               if(!ok){
                   ErrString = "Ошибка в номере регистра при записи условия BREAK!";
                   ErrString += " строка " + QString::number(line_num+1);
                   return false;
               }
               if(reg>4095){
                   ErrString = "Номер регистра при записи условия BREAK >4095!";
                   ErrString += " строка " + QString::number(line_num+1);
                   return false;
               }

               value=str2.toUShort();

               mas[mas_words]=(0x6000 | reg);
               if(value) mas[mas_words]|=0x1000;
               mas_words++;

               upak_compile::USED_REGS+=reg;
             }
             continue;
         }

        ErrString = "Ошибка записи условного перехода!";
        ErrString += " строка " + QString::number(line_num+1);
        return false;
     }

     if(line.startsWith("#?")) continue;

     if(!check_string(line)){
         ErrString += " строка " + QString::number(line_num+1);
         return false;
     }

     i=0;
     while(i<line.length()){
         if(line[i]=='#') break;  //если встретился коммент
         if(!make_poland(poland_str, line[i])){
             ErrString += " строка " + QString::number(line_num+1);
             return false;
         }
         i++;
     }
     make_poland(poland_str, 0);


     if(poland_str.length()){
         if(!check_poland_string(poland_str)){
             ErrString += " строка "  + QString::number(line_num+1);
             return false;
         }
         if(!add_bytecode(poland_str)){
             ErrString += " строка "  + QString::number(line_num+1);
             return false;
         }
         if(mas_words*2>UPAK_MAX_SIZE){
             ErrString = "Превышение максимальной длины программы УПАК!";
             return false;
         }
     }

 }

 if(jump_stack_ptr){
     ErrString = "Несовпадение количества секций START-STOP!";
     return false;
 }

 mas[mas_words]=0xffff;
 mas_words++;
 if(mas_words*2>UPAK_MAX_SIZE){
     ErrString = "Превышение максимальной длины программы УПАК!";
     return false;
 }

 ErrString = "Компиляция программы УПАК прошла успешно!";
 return true;
}



bool upak_compile::check_string(const QString& str)
{
  const uchar upak_check_rules[8] = {0x05, 0x85, 0x85, 0x85, 0x01, 0x85, 0x7A, 0x7B};
  int i, br;
  QString s;
  QChar c;
  int column, string;

  if(str.isEmpty()) return true;

  if(str[0]=='#') return true;

  //подготовка очищенной от комментов строки и с убранными избыточными  пробелами
     s=str;
     i=s.indexOf('#');
     if(i>-1) s.truncate(i);
     s=s.simplified();

  if(s.isEmpty()) return true;

  for(i=0; i<s.length(); i++){
      if(!upak_compile::is_operation(s[i]) && s[i]!='(' && s[i]!=')' && s[i]!=' ' && !s[i].isDigit()){
          ErrString = "Недопустимый символ '" + s[i] + "' в записи формулы!";
          return false;
      }
  }

  //различные проверки строки
    //проверка на совпадение кол-ва открывающих и закрывающих скобок
      br=0;
      for(i=0; i<s.length(); i++){
          if(s[i]=='(') br++;
          if(s[i]==')'){
              if(br) br--;
              else{
                  ErrString = "Некорректное использование скобок в формуле!";
                  return false;
              }
          }
      }
      if(br) {ErrString = "Несовпадение количества открывающих и закрывающих скобок!"; return false;}


    //проверка, что первым символом в строке может быть только цифра или операция инверсии '!' или '~' .  Для других операций всегда необходимы два операнда
      if(!s[0].isDigit() && !(s[0]=='!' || s[0]=='(' || s[0]=='~')){
          ErrString = "Недопустимый символ или операция в начале строки!";
          return false;
      }


    //проверка, что последним символом в формуле может быть только цифра
      if(!s[s.length()-1].isDigit()){
          ErrString = "Ошибка!  Формула должна заканчиваться номером регистра!";
          return false;
      }


    //проверка на однократное использование оператора '=' в формуле
      br=0;
      for(i=0; i<s.length(); i++) if(s[i]=='=') br++;
      if(br!=1){
          if(br>1) ErrString = "Ошибка!  В формуле не может быть более одного оператора присваивания '=' !";
             else  ErrString = "Ошибка!  В формуле отсутствует оператор присваивания '=' !";
          return false;
      }


    //проверка каждой операции на легальных соседей рядом

      /*=========================================================================*/
      /*
       таблица допустимости соседства символов в формуле упака
       по вертикали символ для сравнения, по горизонтали - следующий за ним в формуле
       единица - обозначает допустимость комбинации, ноль - соотвественно запрет
       так как операция инверсии допускает два написания символами: ! или ~ , то
       соотвественно при анализе они идут под одним кодом символа.
       анализ выполняется по таблице начиная с первого символа. допустимость самого первого
       символа на своей позиции проверяется вотдельной проверкой ранее.
       проверка на то, что операция присваивания = является последней в формуле выполняется
       отдельно, после преобразования формулы в бесскобочную запись


            7  6  5  4  3  2  1  0
            !  +  ^  *  =  (  )  A
          _________________________
      7 ! | 0  0  0  0  0  1  0  1
      6 + | 1  0  0  0  0  1  0  1
      5 ^ | 1  0  0  0  0  1  0  1
      4 * | 1  0  0  0  0  1  0  1
      3 = | 0  0  0  0  0  0  0  1
      2 ( | 1  0  0  0  0  1  0  1
      1 ) | 0  1  1  1  1  0  1  0
      0 A | 0  1  1  1  1  0  1  1


      */
      /*=========================================================================*/

      //убираю пробелы из строки
      s.remove(QRegExp("\\s"));

      for(i=1; i<s.length(); i++){
          c=s[i-1];
          switch(c.toLatin1()){
          case '~':
          case '!': string=0;
              break;
          case '+': string=1;
              break;
          case '^': string=2;
              break;
          case '*': string=3;
              break;
          case '=': string=4;
              break;
          case '(': string=5;
              break;
          case ')': string=6;
              break;
          default:  string=7;
              break;
          }
          c=s[i];
          switch(c.toLatin1()){
          case '~':
          case '!': column=0x80;
              break;
          case '+': column=0x40;
              break;
          case '^': column=0x20;
              break;
          case '*': column=0x10;
              break;
          case '=': column=0x08;
              break;
          case '(': column=0x04;
              break;
          case ')': column=0x02;
              break;
          default:  column=0x01;
              break;
          }

          if(!(upak_check_rules[string]&column)){
             ErrString="Ошибка в формуле! Недопустимое сочетание  '" + s[i-1] + "'  и  '" + s[i] + "'  !";
             return false;
          }
      }


    //остальные проверки писать тут далее

   return true;
}

bool upak_compile::check_poland_string(const QString& str)
{
    //проверки пролучившейся бесскобочной записи
      //проверка последней операции. по правилам упака должна всегда оказываться операция присваивания '='
    if(str.length())
        if(str[str.length()-1]!='=' || !str[str.length()-3].isDigit()){
            ErrString = "Ошибка в формуле! Операция присваивания ('=') не в конце выражения!";
            return false;
        }

      //остальные проверки писать тут далее

  return true;
}

//возвращает false, если встречает в номере регистра посторонний символ(не цифру)
bool upak_compile::make_poland(QString& str, QChar c)
{
  static QString word="";
  static compile_stack s;
  QChar op;


    if(c==' ') return true;

    if(word.length() && (is_operation(c) || c=='(' || c==')' || c=='\0')) {str+=word; str+=" "; word="";}

    if(is_operation(c)){
        while((op=s.Pop())!='\0'){

                    if(op=='(') {s.Push(op); break;}

                    if(get_priority(op)>=get_priority(c)) {str += op; str += " ";}
                    else {s.Push(op); break;}
            }
            s.Push(c);
            return true;
    }


    while(1){
        if(c=='('){s.Push(c); break;}
        if(c==')'){while( (op=s.Pop())!='(' ) {str+=op; str+=" ";} break;}
        if(c=='\0'){while ((op=s.Pop())!='\0') {str += op; if(op!='=') str+=" "; word="";} break;}
        if(c.isDigit()) word+=c;
          else{
            ErrString = "Недопустимый символ в номере регистра!";
            return false;
          }
        break;
    }

    return true;
}


bool upak_compile::is_operation(QChar ch){
        if(ch=='+' || ch=='*' || ch=='~' || ch=='=' || ch=='!' || ch=='^') return true;
        return false;
}


int upak_compile::get_priority(QChar op){
    while(1){
        if(op=='=') return 0;
        if(op=='(' || op==')') return 1;
        if(op=='+') return 2;
        if(op=='^') return 3;
        if(op=='*') return 4;
        if(op=='~') return 5;
        if(op=='!') return 5;
        return -1;
    }
}



bool upak_compile::add_bytecode(const QString& src)
{
  int i;
  QString word="";
  QString str;
  ushort reg=0xffff;
  ushort type;
  ushort prev_type=0, op_counter=0;
  bool ok;

    str=src;
    str+=' ';

    for(i=0; i<str.length(); i++){

       //-----------------------------------
            switch(str[i].toLatin1()){
              case '~': type=3; break;
              case '!': type=3; break;
              case '*': type=2; break;
              case '^': type=4; break;
              case '+': type=1; break;
              case '=': type=5; break;

              case ' ':
                   if(word.length())
                    {
                     reg = word.toUShort(&ok);
                     if(!ok){
                         ErrString = "Ошибка в номере регистра: " + word;
                         return false;
                     }
                     if(reg>4095){
                       ErrString = "Ошибка! Используется номер логического регистра вне разрешённого диапазона!";
                       return false;
                      }
                     word="";
                     type=100;
                     break;
                    }
                   continue;

              default:
                   if(word.length()==4){
                       word+=str[i];
                       ErrString = "Ошибка в номере регистра: " + word;
                       return false;
                      }
                   word+=str[i];
                   continue;
             }
       //--------------------------------------------


      if(type==100){
        if(op_counter){
         mas_words++;
         op_counter=0;
        }
        mas[mas_words]=(0x8000|reg);
        mas_words++;
        upak_compile::USED_REGS+=reg;
       }

      if(type>0 && type<=5){
       if(prev_type==100){mas[mas_words-1]|=((type<<12)&0x7000);}
        else{
          if(op_counter<4){
            mas[mas_words]=((mas[mas_words]<<3)&0x0fff);
            mas[mas_words]|=((op_counter<<12)&0x3000);
            mas[mas_words]|=(type&0x0007);
            op_counter++;
            if(op_counter==4){
             mas_words++;
             op_counter=0;
            }
          }
        }
      }

      prev_type=type;
     }

    if(op_counter) mas_words++;

    return true;
}






void upak_compile::ClearRules()
{
    rules.clear();
    for(int i=0; i<N_OF_REGS; i++) rules.append("");
    hash_rules.clear();
}



bool upak_compile::UpdateRules(const QStringList& list)
{
    QString str;
    int line_num;


    ClearRules();

    line_num=0;

    foreach(str, list){
        line_num++;
        str = str.trimmed();
        if(str.startsWith("#?")){
            str.remove(0, 2);
            str = str.simplified();
            if(str.length()){
                if(!AddRules(str)){
                    ErrString += " строка " + QString::number(line_num);
                    return false;
                }
            }
        }
    }

    ErrString = "Считывание правил завершилось успешно!";
    return true;
}




bool upak_compile::AddRules(QString& str)
{
    QStringList list;
    QString rule, reg_str, name;
    QRegExp rx;
    QChar ch;
    int res, reg;
    bool ok;


    //замена всех ',' на ';'
    str.replace(',', ';');

    list = str.split(';', QString::SkipEmptyParts);

    //для каждой записи вида регистр=имя , перечисленных в строке через ';'
    foreach(rule, list){
        rule = rule.simplified();

        rx.setPattern("^(\\d\\d{0,3})\\s{0,1}=\\s{0,1}(\\w+)$");

        res = rx.indexIn(rule);

        if(res<0){
            ErrString = "Ошибка в записи правила конвертации имен регистров!";
            return false;
        }else{
          reg_str=rx.cap(1);
          name=rx.cap(2);

          reg=reg_str.toInt(&ok);
          if(!ok){
              ErrString = "Ошибка в записи номера регистра в правиле его конвертации в буквы!";
              return false;
          }
          if(reg>4095){
              ErrString = "Номер регистра в правиле его конвертации в буквы >4095!";
              return false;
          }

          res=0;
          foreach(ch, name){
              if(is_special_character(ch)){
                  ErrString = "Имя регистра в правиле его конвертации содержит недопустимые символы!";
                  return false;
              }
          }

          rules[reg] = name;
          hash_rules.insert(name, static_cast<ushort>(reg));
        }
    }

   return true;
}




bool upak_compile::is_special_character(QChar c){
    if(is_operation(c)) return true;
    if(c=='_') return false;
    if(c.isSpace()) return true;
    if(c.isLetterOrNumber()) return false;
    if(c=='(' || c==')' || c=='#' || c=='?') return true;

    return false;
}



void upak_compile::ConvertNumAndChars(QStringList& list, ConvertDirection dir)
{
    QString in, out, word;
    int i, j;

    if(!rules.count()) ClearRules(); //если список правил еще не проинициализирован ни разу

    //для каждой строчки
    for(i=0; i<list.count(); i++){
        in = list[i];
        word="";
        out="";

        for(j=0; j<in.length(); j++){
            if(is_special_character(in[j])){
                if(word.length()){
                    if(dir==ToChars) out+=GetRegName(word);
                                else out+=GetRegNum(word);
                    word="";
                 }

                 if(in[j]=='#'){
                     out+=in.mid(j);
                     break;
                 }else out+=in[j];

            }else word+=in[j];

        }
        if(word.length()){
            if(dir==ToChars) out+=GetRegName(word);
                        else out+=GetRegNum(word);
        }

        list[i]=out;
    }
}

QString& upak_compile::ConvertNumAndChars(QString& str, ConvertDirection dir){
    QStringList list;

    list.append(str);
    ConvertNumAndChars(list, dir);
    str=list[0];
    return str;
}


QString upak_compile::GetRegName(QString word)
{
    bool ok;
    int reg_num;

    if(!rules.count()) ClearRules(); //если список правил еще не проинициализирован ни разу

    reg_num = word.toInt(&ok);
    if(!ok) return word;
    if(reg_num<0 || reg_num>4095) return word;
    if(rules[reg_num].length()==0) return word;
    return rules[reg_num];
}


QString upak_compile::GetRegNum(QString word)
{
    if(!rules.count()) ClearRules(); //если список правил еще не проинициализирован ни разу

    /*
    for(int i=0; i<4096; i++)
        if(rules[i]==word) return QString::number(i);
    return word;
    */
    uint16_t val = hash_rules.value(word, 0xffff);
    if(val!=0xffff) return QString::number(val);
    else return word;
}


int upak_compile::GetRegNum(QStringRef word)
{
    bool ok;
    QString str=word.toString();
    int res;

    if(!rules.count()) ClearRules(); //если список правил еще не проинициализирован ни разу

    uint16_t val = hash_rules.value(str, 0xffff);
    if(val!=0xffff) return val;
    else{
        res = str.toUShort(&ok);
        if(ok) return res;
        else return -1;
    }
}


void upak_compile::ReplaceText(QStringList& list, const QString before, const QString after){
    QString in, out, word;
    int i, j;

    //для каждой строчки
    for(i=0; i<list.count(); i++){

        in = list[i];
        word="";

        for(j=0; j<in.count(); i++){

            if(is_special_character(in[j])){

                if(word.length()){
                    if(word==before) out+=after;
                    word="";
                 }

                 out+=in[j];

            }else word+=in[j];

        }
        if(word.length())
            if(word==before) out+=after;

        list[i]=out;
    }
}



 void upak_compile::ResetDozators(){
  int common_flag_regs=700, speed_regs=780, wp_regs=1100;

  NOfDoz=0;

     for(int i=0; i<10; i++){
         dozator_data[i].dozator_type=VES;
         dozator_data[i].NOfComp=1;
         dozator_data[i].et04_adr=static_cast<uint8_t>(i+1);
         dozator_data[i].et04_channel=0;

         dozator_data[i].flags.error_load=QString::number(common_flag_regs++);
         dozator_data[i].flags.error_zero=QString::number(common_flag_regs++);
         dozator_data[i].flags.wait_weighter=QString::number(common_flag_regs++);
         dozator_data[i].flags.substr_flag=QString::number(common_flag_regs++);
         dozator_data[i].flags.DozAllow=QString::number(common_flag_regs++);
         dozator_data[i].flags.DozOpen=QString::number(common_flag_regs++);
         dozator_data[i].flags.DozEnd=QString::number(common_flag_regs++);
         dozator_data[i].flags.DozEmpty=QString::number(common_flag_regs++);
         for(int j=0; j<16; j++){
             dozator_data[i].flags.speed_12[j][0]=QString::number(speed_regs++);
             dozator_data[i].flags.speed_12[j][1]=QString::number(speed_regs++);
         }

         dozator_data[i].wp_num=0;
         for(int j=0; j<4; j++){
             dozator_data[i].wp[j].weight=100*(j+1);
             dozator_data[i].wp[j].gist=3;
             dozator_data[i].wp[j].reg=QString::number(wp_regs++);
         }

         dozator_data[i].reserv=0;
     }

 }


int upak_compile::compile_dosers(uint8_t *ptr){
    uint16_t tmp16;
    uint32_t tmp32;
    dozator_data_t *doz_ptr;
    QSet<uint16_t> set;
    bool ok;

    ErrString = "Ошибка при компилировании дозаторов! ";
    set.clear();

    tmp16 = static_cast<uint16_t>(NOfDoz);
    *reinterpret_cast<uint16_t*>(ptr)=tmp16;
    ptr+=2;

    doz_ptr = reinterpret_cast<dozator_data_t*>(ptr);

    for(int i=0; i<NOfDoz; i++){
        doz_ptr->dozator_type=dozator_data[i].dozator_type;
        doz_ptr->NOfComp=dozator_data[i].NOfComp;
        doz_ptr->et04_adr=dozator_data[i].et04_adr;
        doz_ptr->et04_channel=dozator_data[i].et04_channel;
        for(int j=0; j<16; j++){
            if(j<dozator_data[i].NOfComp){
                tmp32 = ConvertNumAndChars(dozator_data[i].flags.speed_12[j][0], ToDigits).toUInt(&ok);
                if(!ok){
                    ErrString += "Неверное значение 1-ой скорости! Дозатор " + QString::number(i+1) + " , компонент " + QString::number(j+1);
                    return -1;
                }
                if(tmp32>4095){
                    ErrString += "Флаг 1-ой скорости >4095! Дозатор " + QString::number(i+1) + " , компонент " + QString::number(j+1);
                    return -1;
                }
                tmp16 = static_cast<uint16_t>(tmp32);
                if(set.contains(tmp16)){
                    ErrString += "Значение флага 1-ой скорости уже встречается ранее! Дозатор " + QString::number(i+1) + " , компонент " + QString::number(j+1);
                    return -1;
                }
                set<<tmp16;
                doz_ptr->flags.speed_12[j][0]=tmp16;

                tmp32 = ConvertNumAndChars(dozator_data[i].flags.speed_12[j][1], ToDigits).toUInt(&ok);
                if(!ok){
                    ErrString += "Неверное значение 2-ой скорости! Дозатор " + QString::number(i+1) + " , компонент " + QString::number(j+1);
                    return -1;
                }
                if(tmp32>4095){
                    ErrString += "Флаг 2-ой скорости >4095! Дозатор " + QString::number(i+1) + " , компонент " + QString::number(j+1);
                    return -1;
                }
                tmp16 = static_cast<uint16_t>(tmp32);
                if(set.contains(tmp16)){
                    ErrString += "Значение флага 2-ой скорости уже встречается ранее! Дозатор " + QString::number(i+1) + " , компонент " + QString::number(j+1);
                    return -1;
                }
                set<<tmp16;
                doz_ptr->flags.speed_12[j][1]=tmp16;

            }else{
                doz_ptr->flags.speed_12[j][0]=0;
                doz_ptr->flags.speed_12[j][1]=0;
            }

        }


        tmp32 = ConvertNumAndChars(dozator_data[i].flags.error_load, ToDigits).toUInt(&ok);
        if(!ok){
            ErrString += "Неверное значение флага ошибки дозирования! Дозатор " + QString::number(i+1);
            return -1;
        }
        if(tmp32>4095){
            ErrString += "Значение флага ошибки дозирования >4095! Дозатор " + QString::number(i+1);
            return -1;
        }
        tmp16 = static_cast<uint16_t>(tmp32);
        if(set.contains(tmp16)){
            ErrString += "Значение флага ошибки дозирования уже встречается ранее! Дозатор " + QString::number(i+1);
            return -1;
        }
        set<<tmp16;
        doz_ptr->flags.error_load=tmp16;


        tmp32 = ConvertNumAndChars(dozator_data[i].flags.error_zero, ToDigits).toUInt(&ok);
        if(!ok){
            ErrString += "Неверное значение флага ошибки нуля! Дозатор " + QString::number(i+1);
            return -1;
        }
        if(tmp32>4095){
            ErrString += "Значение флага ошибки нуля >4095! Дозатор " + QString::number(i+1);
            return -1;
        }
        tmp16 = static_cast<uint16_t>(tmp32);
        if(set.contains(tmp16)){
            ErrString += "Значение флага ошибки нуля уже встречается ранее! Дозатор " + QString::number(i+1);
            return -1;
        }
        set<<tmp16;
        doz_ptr->flags.error_zero=tmp16;


        tmp32 = ConvertNumAndChars(dozator_data[i].flags.wait_weighter, ToDigits).toUInt(&ok);
        if(!ok){
            ErrString += "Неверное значение флага ожидания весовщика! Дозатор " + QString::number(i+1);
            return -1;
        }
        if(tmp32>4095){
            ErrString += "Значение флага ожидания весовщика >4095! Дозатор " + QString::number(i+1);
            return -1;
        }
        tmp16 = static_cast<uint16_t>(tmp32);
        if(set.contains(tmp16)){
            ErrString += "Значение флага ожидания весовщика уже встречается ранее! Дозатор " + QString::number(i+1);
            return -1;
        }
        set<<tmp16;
        doz_ptr->flags.wait_weighter=tmp16;


        tmp32 = ConvertNumAndChars(dozator_data[i].flags.substr_flag, ToDigits).toUInt(&ok);
        if(!ok){
            ErrString += "Неверное значение флага substr_flag! Дозатор " + QString::number(i+1);
            return -1;
        }
        if(tmp32>4095){
            ErrString += "Значение флага substr_flag >4095! Дозатор " + QString::number(i+1);
            return -1;
        }
        tmp16 = static_cast<uint16_t>(tmp32);
        if(set.contains(tmp16)){
            ErrString += "Значение флага substr_flag уже встречается ранее! Дозатор " + QString::number(i+1);
            return -1;
        }
        set<<tmp16;
        doz_ptr->flags.substr_flag=tmp16;


        tmp32 = ConvertNumAndChars(dozator_data[i].flags.DozAllow, ToDigits).toUInt(&ok);
        if(!ok){
            ErrString += "Неверное значение флага DozAllow! Дозатор " + QString::number(i+1);
            return -1;
        }
        if(tmp32>4095){
            ErrString += "Значение флага DozAllow >4095! Дозатор " + QString::number(i+1);
            return -1;
        }
        tmp16 = static_cast<uint16_t>(tmp32);
        if(set.contains(tmp16)){
            ErrString += "Значение флага DozAllow уже встречается ранее! Дозатор " + QString::number(i+1);
            return -1;
        }
        set<<tmp16;
        doz_ptr->flags.DozAllow=tmp16;


        tmp32 = ConvertNumAndChars(dozator_data[i].flags.DozOpen, ToDigits).toUInt(&ok);
        if(!ok){
            ErrString += "Неверное значение флага DozOpen! Дозатор " + QString::number(i+1);
            return -1;
        }
        if(tmp32>4095){
            ErrString += "Значение флага DozOpen >4095! Дозатор " + QString::number(i+1);
            return -1;
        }
        tmp16 = static_cast<uint16_t>(tmp32);
        if(set.contains(tmp16)){
            ErrString += "Значение флага DozOpen уже встречается ранее! Дозатор " + QString::number(i+1);
            return -1;
        }
        set<<tmp16;
        doz_ptr->flags.DozOpen=tmp16;


        tmp32 = ConvertNumAndChars(dozator_data[i].flags.DozEnd, ToDigits).toUInt(&ok);
        if(!ok){
            ErrString += "Неверное значение флага DozEnd! Дозатор " + QString::number(i+1);
            return -1;
        }
        if(tmp32>4095){
            ErrString += "Значение флага DozEnd >4095! Дозатор " + QString::number(i+1);
            return -1;
        }
        tmp16 = static_cast<uint16_t>(tmp32);
        if(set.contains(tmp16)){
            ErrString += "Значение флага DozEnd уже встречается ранее! Дозатор " + QString::number(i+1);
            return -1;
        }
        set<<tmp16;
        doz_ptr->flags.DozEnd=tmp16;


        tmp32 = ConvertNumAndChars(dozator_data[i].flags.DozEmpty, ToDigits).toUInt(&ok);
        if(!ok){
            ErrString += "Неверное значение флага DozEmpty! Дозатор " + QString::number(i+1);
            return -1;
        }
        if(tmp32>4095){
            ErrString += "Значение флага DozEmpty >4095! Дозатор " + QString::number(i+1);
            return -1;
        }
        tmp16 = static_cast<uint16_t>(tmp32);
        if(set.contains(tmp16)){
            ErrString += "Значение флага DozEmpty уже встречается ранее! Дозатор " + QString::number(i+1);
            return -1;
        }
        set<<tmp16;
        doz_ptr->flags.DozEmpty=tmp16;


        doz_ptr->wp_num=dozator_data[i].wp_num;


        for(int j=0; j<4; j++){
            doz_ptr->wp[j].weight=dozator_data[i].wp[j].weight;
            doz_ptr->wp[j].gist=dozator_data[i].wp[j].gist;

            tmp32 = ConvertNumAndChars(dozator_data[i].wp[j].reg, ToDigits).toUInt(&ok);
            if(!ok){
                ErrString += "Неверное значение флага весовой точки! Дозатор " + QString::number(i+1) + " , точка " + QString::number(j+1);
                return -1;
            }
            if(tmp32>4095){
                ErrString += "Значение флага весовой точки >4095! Дозатор " + QString::number(i+1) + " , точка " + QString::number(j+1);
                return -1;
            }
            tmp16 = static_cast<uint16_t>(tmp32);
            if(set.contains(tmp16)){
                ErrString += "Значение флага весовой точки уже встречается ранее! Дозатор " + QString::number(i+1) + " , точка " + QString::number(j+1);
                return -1;
            }
            set<<tmp16;
            doz_ptr->wp[j].reg=tmp16;
        }


           //добавляю в набор используемые регистры
            upak_compile::USED_REGS+=doz_ptr->flags.error_load;
            upak_compile::USED_REGS+=doz_ptr->flags.error_zero;
            upak_compile::USED_REGS+=doz_ptr->flags.wait_weighter;
            upak_compile::USED_REGS+=doz_ptr->flags.substr_flag;
            upak_compile::USED_REGS+=doz_ptr->flags.DozAllow;
            upak_compile::USED_REGS+=doz_ptr->flags.DozOpen;
            upak_compile::USED_REGS+=doz_ptr->flags.DozEnd;
            upak_compile::USED_REGS+=doz_ptr->flags.DozEmpty;
            for(int c=0; c<doz_ptr->NOfComp; c++){
                upak_compile::USED_REGS+=doz_ptr->flags.speed_12[c][0];
                upak_compile::USED_REGS+=doz_ptr->flags.speed_12[c][1];
            }
            for(int wp=0; wp<doz_ptr->wp_num; wp++)
                upak_compile::USED_REGS+=doz_ptr->wp[wp].reg;


        doz_ptr++;
    }

    ErrString = "Дозаторы успешно скомпилированы";
    return static_cast<int>(sizeof(dozator_data_t))*NOfDoz + 2;
}

