#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include "upak_compile.h"
#include <QTreeWidget>
#include <QXmlStreamReader>
#include <com_settings.h>
#include "modbus.h"
#include <QLabel>
#include <QSet>
#include <QListWidget>
#include <QTimer>
#include <QProgressBar>
#include <QByteArray>
#include <QString>

#include <vector>
#include <map>
#include <utility>
#include <exception>

#include "upak_settings.h"

#include "finddialog.h"
#include "syntax_highlighter.h"


#define  WP_MAX_WEIGHT   100000.0f
#define  WP_MIN_WEIGHT   0.0f
#define  WP_MAX_GIST     99.9f
#define  WP_MIN_GIST     0.0f

//далее выбранные для обмена номера регистров в приборах ЕТ
#define MODBUS_REG_PING          1000
#define MODBUS_REG_UPAK_SIZE     5004
#define MODBUS_REG_UPAK          10000
#define MODBUS_REG_REGS          1000

#define FILE_REG          20000
#define FILE_START_REG    5007
#define FILE_END_REG      5008
#define FILE_CHECK_REG    5009



namespace Ui {
    class MainWindow;
    class com_settings;
}


enum ET_MESSAGE_TYPES{
    ET_CHECK=0,
    PROGRAM_WRITE,
    GET_REGS,
    WRITE_PROGRAM_TEXT,
    READ_PROGRAM_TEXT,
    PROGRAM_ERASE
};


#define  LABEL_MAIN_PROGRAM      1
#define  LABEL_TIMERS            2
#define  LABEL_MSEC_TIMERS       3
#define  LABEL_COUNTERS          4
#define  LABEL_MODULES           5
#define  LABEL_DOSERS            6


//типы модулей в таблице опроса
#define MODULE_TYPE_01     0x00
#define MODULE_TYPE_05     0x01
#define MODULE_TYPE_07     0x02




//общий вид упака
#pragma pack(push,1)
typedef struct{
  unsigned short signature;
  unsigned short size;
  unsigned char  crc16[2];
  unsigned char* data;
}upak_structure_t;
#pragma pack(pop)


//вид таблицы опроса модулей
typedef struct{
  uint32_t adr:8;         //сетевой адрес
  uint32_t type:2;        //тип модуля 01, 05, 07
  uint32_t regs_in:9;     // начальный регистр, куда кладем данные, полученные из модуля (деленный на 8, так как должен быть кратен байту)
  uint32_t regs_out:9;    // начальный регистр, откуда берем данные для отправки в модуль (деленный на 8, так как должен быть кратен байту)
  uint32_t reserv:4;
} poll_table_t;




class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow();

private slots:
    void on_btn_Compile_released();

    void on_btn_ConvertToChars_released();

    void on_btn_ConvertToDigits_released();

    void on_treeDozator_itemChanged(QTreeWidgetItem *item, int column);

    void on_treeModules_itemChanged(QTreeWidgetItem *item, int column);

    void on_btn_SaveFile_released();

    void on_spinBox_et07num_valueChanged(int val);

    void on_spinBox_et05num_valueChanged(int val);

    void on_spinBox_et01num_valueChanged(int val);

    void on_btn_OpenFile_released();

    void action_11_activated();

    void action_6_activated();

    void action_7_activated();

    void on_btn_ReleaseCOM_released();

    void on_btn_OpenCOM_clicked();

    void on_btn_Check_released();

    void modbus_reply(message_data_t data, MSG_STATUS status);

    void content_changed(void){content_changed_flag=true;}

    void on_btn_WriteProgram_released();

    void on_btn_GetRegs_released();

    void RedrawRegs();

    void RebuildRegList();

    void on_checkBox_Poll_stateChanged(int arg1);

    void flags_changed(QListWidgetItem*);

    void action_activated();

    void action_2_activated();

    void action_3_activated();

    void action_4_activated();

    void find_text();

    void find_text_hide();

    void on_plainTextEdit_ProgramText_cursorPositionChanged();

    void WriteUPAKtext(bool start=true, bool status=false);

    void LoadUPAKtext(bool start=true, bool status=false);

    void UpdateProgressBar(int value);

    void on_btn_GetProgramText_released();

private:
    Ui::MainWindow *ui;

    Highlighter *highlighter;

    //настройки программы
    upak_settings settings;

    //диалог поиска
    FindDialog find_dialog;

    bool DozatorTreeCreatedFlag;

    QString OpenedFileName;

    int COM_num;
    int baudrate;
    int NetAdr;
    void GetComSettings(upak_settings_t&);

    ModbusThread port;

    QLabel *COM_status_label;   //для отображения статуса ком-порта в статусной строке
    QLabel *line_number_label;  //для отображения номера строки
    QProgressBar *ProgressBar;   //для отображения хода процесса обмена с  прибором


    //хранение флажков, полученых из прибора
    static uint16_t REGS[256];
    bool request_regs(bool silent_mode = false);

    //буфер для хранения компилированного упака
    uint8_t upak_mas[0x3fff];
    uint upak_bytes;  //размер кол-ва данных в массиве упака

    //признак того, что программа поменялась и перед записью в прибор необходимо ее скомпилировать
    bool content_changed_flag;

    //запрет на запись в прибор изменяющихся флажков в списке
    bool deny_flags_update_flag;

    void WriteStatus(QString str);

    //как вариант дальнешие обновление количества элементов можно потом реализовать не вручную, а
    //связав сигнал изменения item-а количества параметров со слотом item-а, в котором
    //будет меняться количество дочерних элементов
    //возможно для этого надо будет унаследовать QTreeWidgetItem
    void UpdateBranch(QTreeWidgetItem *root, int item_num);

    void UpdateDozatorTree();
    void UpdateDozatorTreeRegs();

    void ConvertNumChars(ConvertDirection dir);

    void CreateDozatorTree();

    void CreateModulesTree();

    void CleanAllWighets();

    //сохранение в XML
    bool SaveFile(bool for_et=false);
    bool OpenFile(bool from_et=false);

    //буфер для сохранения упака из прибора
    QByteArray buffer;


    bool open_upak_xml(QXmlStreamReader& xml, bool from_et);
    bool read_xml_text(QXmlStreamReader& xml, QString* str);
    int  get_xml_attribute(QXmlStreamReader& xml, QString attribute);
    bool find_start_element(QXmlStreamReader& xml);
    bool find_next_start_element(QXmlStreamReader& xml);

    void OpenCOM();
    void CloseCOM();


    int compile_timers(uint8_t *ptr);
    int compile_100msec_timers(uint8_t *ptr);
    int compile_counters(uint8_t *ptr);
    int compile_modules(uint8_t *ptr);

    void WriteUPAK(bool start, bool status);

    void EraseUPAK();

    //возвращает значение флажка из массива REGS
    static bool GetReg(uint);
    static int GetReg(QStringRef);

    void StartProgressBar();
    void StopProgressBar();

    //периодический опрос
    void poll_init();
    void poll_timer_start();
    void poll_timer_stop();
    void poll_regs_received(bool status);
    bool poll_is_active(){return poll_timer->isActive();}
    QTimer* poll_timer;
    void poll_timer_callback();
    bool poll_regs_received_flag;

    //чтение файла
    bool read_file(const QString& filename, QByteArray& data);

    //общая структура хранения данных упака
    struct upak_data_t{

        //замена std::optional (из-за libstdc v6)
        template<typename T>
        struct opt{
            bool en;
            T data;
            opt():en(false){}
            opt(const opt& other): en(other.en){if(other) data=other.data;}
            opt(opt&& other): en(other.en){if(other){data=std::move(other.data); other.en=false;}}   //other.en трогаю специально (в std::optional перемещаемый объект остается со значением)
            opt& operator=(const opt& other){en=false; if(other.en){data=other.data; en=true;} return *this;}
            opt& operator=(opt&& other){en=false; if(other.en){std::move(other.data); en=true; other.en=false;} return *this;} //other.en трогаю специально (в std::optional перемещаемый объект остается со значением)

            opt(const T& value):en(true){data=value;}
            operator bool() const {return en;}
            void operator=(const T& new_value){en=true; data = new_value;}
            T value() const {if(!en) throw std::out_of_range("opt: no value!"); return data;}
            void reset(){en=false;}
        };

        template<typename T>
        static QString get_opt_string(const opt<T>& val){return val?QString::number(val.value()):QString();}

        //дозаторы
        struct doser_t{
            enum{DOSER_WEIGHT, DOSER_PW, DOSER_SUB, DOSER_IMP} type;
            unsigned component_num;
            unsigned et04_num;
            unsigned et04_ch;
            enum doser_flags_t {
                  DOZ_FL_ERR=0,
                  DOZ_FL_ERR_ZERO,
                  DOZ_FL_WAIT_WEIGHTER,
                  DOZ_FL_SUBSTR,
                  DOZ_FL_ALLOW,
                  DOZ_FL_OPEN,
                  DOZ_FL_END,
                  DOZ_FL_EMPTY};
            std::map<doser_flags_t, opt<unsigned>> flags;
            bool get_flag(doser_flags_t flag, unsigned& value) const {
                bool ret=true;
                try {auto f = flags.at(flag); if(f) value = f.value();}
                catch (std::out_of_range&) {ret = false;}
                return ret;
            }
            QString get_flag_string(doser_flags_t flag) const {
                unsigned val=0;
                return get_flag(flag, val)?QString::number(val):QString();
            }
            std::vector<std::pair<opt<unsigned>,opt<unsigned>>> component_speed;
            struct wp_t{
                opt<float> weight;
                opt<float> hysteresis;
                opt<unsigned> flag;
            };
            std::vector<wp_t> wp;
        };
        std::vector<doser_t> dosers;

        //модули ввода-вывода
        struct et_data{
           uint8_t modbus_adr;
           std::vector<unsigned> inputs;
           std::vector<unsigned> outputs;
        };
        std::vector<et_data> et01, et05, et07;

        //таймеры и счетчики
        struct upak_timer_t{
            unsigned input;
            unsigned output;
            unsigned setpoint;
            QString description;
        };
        std::vector<upak_timer_t> timers;

        struct upak_counter_t{
            unsigned input_clock;
            unsigned input_reset;
            unsigned output;
            unsigned setpoint;
            QString description;
        };
        std::vector<upak_counter_t> counters;

        void Clean(){
            dosers.clear();
            et01.clear(); et05.clear(); et07.clear();
            timers.clear();
            counters.clear();
        }
    };

    //импорт ELL
    void import_ell_callback();
    QString get_string_from_1251(const QByteArray& arr);
    bool get_upak_data_from_lp(const QString& arr, upak_data_t&);
    void SetupWidgetsFromImport(const QString& ell, const upak_data_t&);
    QString import_comment(const QString&);

};

#endif // MAINWINDOW_H
