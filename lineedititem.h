#ifndef LINEEDITITEM_H
#define LINEEDITITEM_H

#include <QLineEdit>
#include <QTreeWidgetItem>


class LineEditItem : public QLineEdit
{
    Q_OBJECT

private:
    QTreeWidgetItem *item;
    int column;

public:
    LineEditItem(QTreeWidgetItem *item, int column);

public slots:
    void changeItem(const QString & text);

};


#endif // LINEEDITITEM_H
