#include "compile_stack.h"


void compile_stack::Push(QChar c)
{
 if(NumOfStrings<100) st[NumOfStrings++]=c;
}

QChar compile_stack::Pop()
{
 if(NumOfStrings) return st[--NumOfStrings];
 else return 0;
}

bool compile_stack::IsEmpty()
{
 if(NumOfStrings) return false; else return true;
}
