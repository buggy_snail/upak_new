#ifndef DOUBLESPINBOXITEM_H
#define DOUBLESPINBOXITEM_H

#include <QTreeWidgetItem>
#include <QDoubleSpinBox>
#include <QWheelEvent>

class DoubleSpinBoxItem : public QDoubleSpinBox
{
 Q_OBJECT

private:
    QTreeWidgetItem *item;
    int column;

public:
    DoubleSpinBoxItem(QTreeWidgetItem *item, int column);

public slots:
    void changeItem(double);

protected:

   virtual void wheelEvent(QWheelEvent *event) { event->ignore();}
};


#endif // DOUBLESPINBOXITEM_H
