
#include "common.h"


void show_msg_box(const QString &str, QMessageBox::Icon icon)
{
    QMessageBox msgBox;
    msgBox.setIcon(icon);
    msgBox.setText(str);
    msgBox.exec();
}
