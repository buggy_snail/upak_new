#ifndef SPINBOXITEM_H
#define SPINBOXITEM_H

#include <QTreeWidgetItem>
#include <QSpinBox>
#include <QWheelEvent>

class SpinBoxItem : public QSpinBox
{
    Q_OBJECT

private:
    QTreeWidgetItem *item;
    int column;

public:
    SpinBoxItem(QTreeWidgetItem *item, int column);

public slots:
    void changeItem(int);

protected:

   virtual void wheelEvent(QWheelEvent *event) { event->ignore();}

};



#endif // SPINBOXITEM_H
