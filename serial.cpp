#include "serial.h"

#define MAX_WRITE_BUFFER 1024
#define MAX_READ_BUFFER  2048
#define ASCII_XON        0x11
#define ASCII_XOFF       0x13
#define PURGE_FLAGS      PURGE_TXABORT|PURGE_TXCLEAR|PURGE_RXABORT|PURGE_RXCLEAR

DHardwork::DHardwork()
{
//**********************************
//* Инициализация параметров порта *
//**********************************
 sCommBaseParam.PortName   = (char*)"COM1";
 sCommBaseParam.ulBaudRate = CBR_9600;
 sCommBaseParam.byByteSize = 8;
 sCommBaseParam.byStopBits = 0;

//*************************************
//* Инициализация параметров таймаута *
//*************************************
 sCommTimeouts.ReadIntervalTimeout         = 50;  // 50
 sCommTimeouts.ReadTotalTimeoutMultiplier  = 50;  // 4
 sCommTimeouts.ReadTotalTimeoutConstant    = 50;  // 10
 sCommTimeouts.WriteTotalTimeoutMultiplier = 2;   // 4
 sCommTimeouts.WriteTotalTimeoutConstant   = 50;   // 10

 is_initialized=0;
}

DHardwork::~DHardwork()
{
 if(is_initialized) BreakDownCommPort();
}

bool DHardwork::Initialize(char * pcPortName, unsigned long ulBaudRate, BYTE byStopBits)
{
  bool blResult;
  is_initialized=1;
  sCommBaseParam.PortName     = pcPortName;
  sCommBaseParam.ulBaudRate   = ulBaudRate;
  sCommBaseParam.byStopBits   = byStopBits;
  blResult = SetupCommDriver();
  ClearInCommBuffer();
  return blResult;
}

bool DHardwork::SetCommParam
  (                         //**************************************************
  char*   pcPortName,       //* Имя коммуникационного порта.                   *
  unsigned long ulBaudRate, //* Скорость обмена (бод).                         *
  BYTE    byByteSize,       //* Число бит данных в слове обмена.               *
  BYTE    byStopBits        //* Число стопбитов: 0/1/2 - 1/1.5/2 стопбита.     *
                            //* По умолчанию: COM1,9600,8,0.                   *
  )                         //**************************************************
 {
  sCommBaseParam.PortName     = pcPortName;
  sCommBaseParam.ulBaudRate   = ulBaudRate;
  sCommBaseParam.byByteSize   = byByteSize;
  sCommBaseParam.byStopBits   = byStopBits;
  return true;
 }
bool DHardwork::SetTimeoutParam
  (                         //**************************************************
  COMMTIMEOUTS              //* Значения  устанавливаемых  параметров  таймаута*
    sCommTimeouts_New       //* (по умолчанию устанавливаются - {4,2,10,2,10}).*
  )                         //**************************************************
 {
  sCommTimeouts = sCommTimeouts_New;
  return true;
 }

bool DHardwork::SetupCommDriver
  (                     //***************************************************
  HWND  hWnd            //* Дескриптор окна  которому  посылается нотифика- *
                        //* ционное  сообщение WM_COMMNOTIFY при завершении *
                        //* параллельной операции ввода с коммуникационного *
                        //* порта (если равно NULL,  то параллельная опера- *
                        //* ция ввода не должна использоваться).            *
  )                     //***************************************************
 {
  bool blResult;
  DWORD err;
  WCHAR wchar_str[40];

  MultiByteToWideChar(
    CP_ACP,
    MB_COMPOSITE,
    sCommBaseParam.PortName,
    -1,
    wchar_str,
    sizeof(wchar_str)/sizeof(WCHAR)
  );

  hCom = CreateFile(wchar_str,
                    GENERIC_READ | GENERIC_WRITE,
                    0,NULL,OPEN_EXISTING,
                    FILE_ATTRIBUTE_NORMAL,NULL);
  if (hCom==INVALID_HANDLE_VALUE)
  {
   err = GetLastError();
   return false;
  }
  GetCommTimeouts(                      //***********************************
         hCom,                          //* Сохраняем первоначальные значе- *
         &sCommTimeouts_Old);           //* ния задержек для CommPort.      *
                                        //***********************************
                                        //***********************************
  SetCommTimeouts(                      //* Устанавливаем   новые  значения *
         hCom,                          //* для CommPort.                   *
         &sCommTimeouts);               //***********************************
  SetCommMask(hCom,                     //* Маска отслеживаемых событий для *
              0);                       //* коммуникационного порта  - пока *
                                        //* ничего не отслеживаем.          *
  SetupComm(hCom,                       //***********************************
            MAX_READ_BUFFER,            //* Устанавливаем  размеры  буферов *
            MAX_WRITE_BUFFER);          //* для ввода и вывода.             *
                                        //***********************************
  blResult=UpdateConnection();          //* Конфигурируем CommPort.         *
                                        //***********************************
  if( blResult )
  {
   EscapeCommFunction(                  //***********************************
          hCom,                         //* Устанавливаем DTR.              *
          SETDTR);                      //***********************************

   blResult = EscapeCommFunction(       //***********************************
          hCom,                         //* Сбрасываем RTS.                 *
          CLRRTS);                      //***********************************
   if ( !blResult )
   {
    return (unsigned long)NULL;
   }
  return blResult;
  }
 return blResult;
}

 bool DHardwork::UpdateConnection()
  {
  BOOL blResult;
  DCB  sDcb;
  DWORD err;
                                        //***********************************
  GetCommState(hCom,                    //* Загружаем текущие установки DCB *
               &sDcb);                  //* структуры.                      *
                                        //***********************************
                                        //* Заполняем структуру DCB.        *
  err=GetLastError();
                                        //***********************************
  sDcb.DCBlength         = sizeof(DCB); //* Размер структуры.               *
                                        //***********************************
  sDcb.BaudRate          =              //*                                 *
       sCommBaseParam.ulBaudRate;       //* Скорость обмена (бод).          *
  sDcb.fBinary           = TRUE;        //* Двоичный режим.                 *
  sDcb.fParity           = FALSE;       //* Нет контроля по четности.       *
                                        //***********************************
  sDcb.fOutxCtsFlow      = FALSE;       //* Не требуется CTS при выводе.    *
  sDcb.fOutxDsrFlow      = FALSE;       //* Не требуется DTR при выводе.    *
  sDcb.fDsrSensitivity   = FALSE;       //* Нет чувствительности  к состоя- *
                                        //* нию линии DSR.                  *
  sDcb.fDtrControl = DTR_CONTROL_ENABLE;//* Разрешение DTR  после  открытия *
                                        //* устройства.                     *
  sDcb.fRtsControl = RTS_CONTROL_ENABLE;//* Разрешение RTS  после  открытия *
                                        //* устройства.                     *
                                        //***********************************
  sDcb.fTXContinueOnXoff = FALSE;       //* Прекращение передачи  по запол- *
                                        //* нению буфера до тех пор пока... *
  sDcb.fOutX             = FALSE;       //* Нет управления передачей симво- *
                                        //* лами XoffChar/XonChar.          *
  sDcb.fInX              = FALSE;       //* Нет управления  приемом  симво- *
                                        //* лами XoffChar/XonChar.          *
  sDcb.fErrorChar        = FALSE;       //* Нет замены ошибочного символа.  *
  sDcb.fNull             = FALSE;       //* Символ '\o' при приеме не игно- *
                                        //* рируется.                       *
  sDcb.fAbortOnError     = FALSE;       //* Нет  прекращения  передачи  при *
                                        //* получения ошибочного символа.   *
  sDcb.fDummy2           = 0;           //* Резерв. Не используется.        *
//  sDcb.wReserved         = 0;           //* Резерв. Должен быть равен 0.    *
  sDcb.XonLim            = 100;         //* Минимум  байт  в буфере  ввода, *
                                        //* прежде чем посылается XonChar.  *
  sDcb.XoffLim           = 100;         //* Максимум байт  в буфере вывода, *
                                        //* прежде чем посылается XoffChar. *
                                        //***********************************
  sDcb.ByteSize          =              //* Число  бит данных  в слове  об- *
       sCommBaseParam.byByteSize;       //* мена.                           *
  sDcb.Parity            = NOPARITY;    //* Схема контроля: нет контроля.   *
  sDcb.StopBits          =              //*                                 *
       sCommBaseParam.byStopBits;       //* Число стопбитов.                *
  sDcb.XonChar           = ASCII_XON;   //* Значение XonChar для Tx и Rx.   *
  sDcb.XoffChar          = ASCII_XOFF;  //* Значение XoffChar для Tx и Rx.  *
  sDcb.ErrorChar         = (char)0xFF;  //* Символ замены при ошибке обмена.*
  sDcb.EofChar           = (char)0xFE;  //* Символ конца передачи.          *
  sDcb.EvtChar           = '\0';        //* Символ сигнализирующий  о собы- *
                                        //* тии.                            *
  sDcb.wReserved1        = FALSE;       //* Резерв. Не используется.        *
                                        //***********************************
  blResult = SetCommState(              //* Конфигурируем  коммуникационный *
               hCom,                    //* порт асинхронной последователь- *
               &sDcb);                  //* ной передачи.                   *
                                        //***********************************

  err=GetLastError();


  if(blResult==TRUE) return true;
                else return false;
  }

bool DHardwork::BreakDownCommPort()
{
  EscapeCommFunction(                   //***********************************
  hCom,CLRDTR);                         //* Сбрасываем DTR.                 *
  SetCommTimeouts(                      //***********************************
      hCom,                             //* Восстанавливаем  первоначальные *
      &sCommTimeouts_Old);              //* значения задержек для CommPort. *
                                        //***********************************
  PurgeComm(                            //* Прекращаем, если они выполняют- *
      hCom,                             //* ся,  все операции  ввода/вывода *
      PURGE_FLAGS);                     //* и  очищаем   внутренние  буфера *
                                        //* ввода/вывода CommPort.          *
                                        //***********************************
  CloseHandle(                          //* Освобождаем дескриптор коммуни- *
      hCom);                            //* кационного порта.               *
  hCom = NULL;					//***********************************
  return true;
}

//***************************************************************************
//* Записать массив данных в коммуникационный порт.                         *
//***************************************************************************
//* Возвращает  число байтов данных  реально переданных  в коммуникационный *
//* порт.                                                                   *
//***************************************************************************
unsigned long DHardwork::Out
  (                     //***************************************************
  BYTE* pbyOutData,     //* Массив байтов  передаваемых  в коммуникационный *
                        //* порт.                                           *
  int   iQuantOutData   //* Число  байтов  передаваемых  в коммуникационный *
                        //* порт.                                           *
  )                     //***************************************************
  {
  bool   blResult;
  unsigned long  ulBytesWritten;
//  unsigned long  ulQuantityData;
  unsigned long  ulEventMask = EV_TXEMPTY;

#if 0
  blResult = EscapeCommFunction(        //***********************************
             hCom,                      //* Устанавливаем DTR.              *
             SETDTR);                   //***********************************
  if ( !blResult )
  {
   return (unsigned long)NULL;
  }
  blResult = EscapeCommFunction(        //***********************************
             hCom,                      //* Сбрасываем RTS.                 *
             CLRRTS);                   //***********************************
  if ( !blResult )
  {
   return (unsigned long)NULL;
  }
#endif
                                        //***********************************
  blResult = SetCommMask(               //* Устанавливаем маску  для комму- *
             hCom,                      //* никационного порта:             *
             /*EV_TXEMPTY*/ 0x01ff);    //* - ждать до полной передачи все- *
  if ( !blResult )                      //*   го буфера.                    *
  {                                     //***********************************
   return (unsigned long)NULL;
  }
    blResult=WriteFile(                 //***********************************
             hCom,                      //* Вывод в порт RS-232.            *
             pbyOutData,                //***********************************
             (unsigned long)iQuantOutData,&ulBytesWritten,NULL);

  if ( blResult )
  {                                     //***********************************
    blResult = WaitCommEvent(           //* Ждем завершения передачи  всего *
               hCom,                    //* буфера, т.к. маска настроена на *
               &ulEventMask,NULL);      //* единственное событие.           *
    if ( blResult )                     //***********************************
      {
       if(!(ulEventMask&EV_TXEMPTY)) ulBytesWritten=0;
      SetCommMask(                      //* Сброс (очистка) всех масок  для *
            hCom,                       //* коммуникационного порта:        *
            NULL);                      //***********************************
      }
    return ulBytesWritten;
    }
  else
    {
    return (unsigned long)NULL;
    }
  }
//***************************************************************************
//* Прочитать массив данных из коммуникационного порта.                     *
//***************************************************************************
//* Возвращает число байтов данных  реально полученных из коммуникационного *
//* порта.                                                                  *
//***************************************************************************

 unsigned long DHardwork::In
  (                     //***************************************************
  BYTE* pbyInData,      //* Массив для приема байтов получаемых из коммуни- *
                        //* кационного порта.                               *
  int   iQuantInData    //* Число  байтов  получаемых  из коммуникационного *
                        //* порта.                                          *
  )                     //***************************************************
  {
//  BOOL blResult;
  unsigned long ulBytesRealRead;

  bool bResult = ReadFile(hCom,pbyInData,
                      (unsigned long)iQuantInData,&ulBytesRealRead,NULL);
  if(!bResult) return -1;
  else return ulBytesRealRead;
  }
//***************************************************************************
//* Очистить системные буфера ввода и вывода коммуникационного порта.       *
//***************************************************************************
//* При успешном завершении возвращает TRUE.                                *
//***************************************************************************
//
bool DHardwork::ClearInCommBuffer()
{
  bool blResult;
                                                        //***********************************
  blResult = PurgeComm(                 //* Очищаем  внутренние буфера вво- *
           hCom,                        //* да/вывода CommPort.             *
           PURGE_TXCLEAR|PURGE_RXCLEAR);      //***********************************
  return blResult;
}
