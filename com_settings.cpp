#include "com_settings.h"

#include <set>
#include <exception>
#include <functional>

#include <QString>
#include <QLabel>
#include <QGridLayout>

#include "common.h"

DialogComSettings::DialogComSettings(QWidget *parent, upak_settings_t &upak_settings)  : QDialog(parent), settings(upak_settings)
{
    setModal(true);
    setWindowTitle("Настройки COM-порта");

    ok_status = false;


    button_ok = new QPushButton(this);
    button_ok->setText("Ok");
    button_cancel = new QPushButton(this);
    button_cancel->setText("Отмена");

    spin_com = new QSpinBox(this);
    spin_com->setMinimum(1);
    spin_com->setMaximum(63);
    QString s;
    try { s = settings.get().at("COM"); } catch (std::out_of_range&) {s = "1";}
    bool ok;
    int cur;
    cur = s.toInt(&ok);
    if(!ok || cur<spin_com->minimum() || cur>spin_com->maximum())
        cur=spin_com->minimum();
    spin_com->setValue(cur);


    spin_adr = new QSpinBox(this);
    spin_adr->setMinimum(1);
    spin_adr->setMaximum(254);
    try { s = settings.get().at("MODBUS_ADR"); } catch (std::out_of_range&) {s = "1";}
    cur = s.toInt(&ok);
    if(!ok || cur<spin_adr->minimum() || cur>spin_adr->maximum())
        cur=spin_adr->minimum();
    spin_adr->setValue(cur);


    combo_baudrate = new QComboBox(this);
    std::set<int> b_rates = {9600, 19200, 38400, 57600, 115200};
    s.clear();
    try {
        s = settings.get().at("BAUDRATE");
    } catch (std::out_of_range&) {}
    cur=0;
    if(s.length()){
       cur = s.toInt(&ok);
       if(ok){
           b_rates.insert(cur);
       }
    }
    for(auto& rate : b_rates)
        combo_baudrate->addItem(QString::number(rate));
    for(int i=0; i<combo_baudrate->count(); i++)
        if(s==combo_baudrate->itemText(i)){
            combo_baudrate->setCurrentIndex(i);
            break;
        }
    if(combo_baudrate->currentIndex()<0)
        combo_baudrate->setCurrentIndex(0);
    combo_baudrate->setEditable(false);


    QGridLayout* layout = new QGridLayout(this);

    layout->addWidget(new QLabel("Номер COM-порта:"), 0, 0);
    layout->addWidget(new QLabel("Скорость:"), 0, 1);
    layout->addWidget(new QLabel("Адрес:"), 0, 2);
    layout->addWidget(button_ok, 0, 3);
    layout->addWidget(button_cancel, 1, 3);
    layout->addWidget(spin_com, 1, 0);
    layout->addWidget(combo_baudrate, 1, 1);
    layout->addWidget(spin_adr, 1, 2);


    this->setLayout(layout);

    this->setFixedSize(layout->minimumSize());


    connect(button_ok, &QPushButton::clicked, [this](){
        QString baudrate = combo_baudrate->itemText(combo_baudrate->currentIndex());
        bool ok;
        baudrate.toUInt(&ok);
        if(!ok){
           show_msg_box("Скорость задана некорретно!");
           return;
        }
        settings.get()["COM"] = QString::number(spin_com->value());
        settings.get()["BAUDRATE"] = baudrate;
        settings.get()["MODBUS_ADR"] = QString::number(spin_adr->value());

        ok_status = true;
        this->close();
    });

    connect(button_cancel, &QPushButton::clicked, [this](){
        ok_status = false;
        this->close();
    });

}
