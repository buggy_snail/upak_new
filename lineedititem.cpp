#include "lineedititem.h"

LineEditItem::LineEditItem(QTreeWidgetItem *item, int column)
{
    this->item = item;
    this->column = column;
    connect(this, SIGNAL(textChanged(const QString&)), SLOT(changeItem(const QString&)) );
}



void LineEditItem::changeItem(const QString & text)
{
    item->setText(this->column, text);
}
